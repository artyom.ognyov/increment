# ∆ Increment

Development started: October 2019 in 100DaysOfCode challenge

## Roadmap:
- October 2020 - Alpha testing
- November 2020 - Beta testing
- December 2020 - Release (Win, Linux, MacOS)