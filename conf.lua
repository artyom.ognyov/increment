function love.conf(t)
	t.title = "INCREMENT"
    t.version = "11.3"
    t.console = false
    t.identity = "increment"
	t.gammacorrect = true
	t.window.highdpi = true
	t.window.icon = "icon_64.png"
end
