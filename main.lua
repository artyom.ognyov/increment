require 'source/engine'

function love.load()
	engineLoad()
end

function love.update(dt)
	startTime = os.clock()
	engineUpdate()
end

function love.keypressed(key)
	engineKeypressed(key)
end

function love.keyreleased(key)
	engineKeyreleased(key)
end

function love.mousepressed(x, y, button, istouch)
	engineMousepressed(x, y, button, istouch)
end

function love.draw()
	engineDraw()
	local delayTime = FPS_TIME - (os.clock() - startTime)
	if delayTime > 0 then
		love.timer.sleep(delayTime)
	end
end
