-- io.stdout:setvbuf'no' 

audio = require "source/libraries/wave/wave"

MusicEngine = {}
function MusicEngine:new()
	local public = {}
		public.state = "standby"
		public.musicData = loadMusicFiles()
		public.currentTrack = public.musicData.world1_1
		public.volume = 0
		public.volumeSpeed = 0.03
		public.volumeCoef = 0.75
		public.looping = true
		public.nextTrackLevel = 0
		public.deafLevel = 0
		public.deafRecoverySpeed = 0.05
		public.bpm = 120

	function public:returnTrackByLevel(level)
		if level < 10 then return public.musicData.world1_1
		elseif level < 11 then return public.musicData.world1_2
		elseif level < 20 then return public.musicData.world2_1
		elseif level < 21 then return public.musicData.world2_2
		elseif level < 30 then return public.musicData.world3_1
		elseif level < 31 then return public.musicData.world3_2
		elseif level < 40 then return public.musicData.world4_1
		elseif level < 41 then return public.musicData.world4_2
		elseif level < 50 then return public.musicData.world5_1
		elseif level < 51 then return public.musicData.world5_2
		elseif level < 60 then return public.musicData.world6_1
		elseif level < 61 then return public.musicData.world6_2
		end
	end

	function public:setCurrentTrack(level)
		if level < 61 then
			public.currentTrack = public:returnTrackByLevel(level)
			if public.currentTrack:isParsed() then
				public.currentTrack:detectBPM()
			end
		end
	end

	function public:updateTrack(level)
		if public.currentTrack == public:returnTrackByLevel(level) then
			public.volume = 1
		else
			if (level == game.levelsScreen.maxOpenedLevel)
			and ( level == 1
				or level == 11
				or level == 21
				or level == 31
				or level == 41
				or level == 51 ) then
					public.state = "stop"
					public.volume = 0 
			else
				public.state = "crossfade"
				public.nextTrackLevel = level
				public.volume = 0
			end
		end
	end

	function public:nextTrack(level, force)
		if not force and (level == game.levelsScreen.maxOpenedLevel)
			and ( level == 1
				or level == 11
				or level == 21
				or level == 31
				or level == 41
				or level == 51 ) then
					public.state = "stop"
					public.volume = 0
			else
				public.state = "crossfade"
				public.nextTrackLevel = level
				public.volume = 0
			end
	end

	function public:setVolume(level)
		public.volume = level
	end

	function public:update()
		if public.state == "standby" then

		elseif public.state == "play" then
			if public.currentTrack:isPlaying() then
				if public.currentTrack:isLooping() then
					public.currentTrack:setLooping(false)
				end
			else
				public.currentTrack:stop()
				public:setCurrentTrack(game.levelsScreen.maxOpenedLevel)
				public.currentTrack:play()
				public.currentTrack:setLooping(true)
				public.currentTrack:setVolume(public.volume)
				public.state = "looping"
			end

		elseif public.state == "looping" then

		elseif public.state == "stop" then
			if public.currentTrack:isLooping() then
				public.currentTrack:setLooping(false)
			elseif not public.currentTrack:isPlaying() then
				public.state = "standby"
			end

		elseif public.state == "crossfade" then
			if public.currentTrack:isPlaying() then
				if public.currentTrack:getVolume() == 0 then
					public.currentTrack:stop()
				end
			else
				public:setCurrentTrack(public.nextTrackLevel)
				public.currentTrack:play()
				public.currentTrack:setLooping(true)
				public.volume = 1
				public.currentTrack:setVolume(public.volume)
				public.state = "looping"
			end

		end

		local currentVolume = public.currentTrack:getVolume()

		if currentVolume > public.volume and currentVolume - public.volume > public.volumeSpeed then
			public.currentTrack:setVolume((currentVolume - public.volumeSpeed - public.deafLevel) * public.volumeCoef)
		elseif currentVolume < public.volume and public.volume - currentVolume > public.volumeSpeed then
			public.currentTrack:setVolume((currentVolume + public.volumeSpeed - public.deafLevel) * public.volumeCoef)
		else
			public.currentTrack:setVolume((public.volume - public.deafLevel) * public.volumeCoef)
		end

		if public.currentTrack:getPitch() < 1 then
			public.currentTrack:setPitch(public.currentTrack:getPitch() + 0.01)
		else
			public.currentTrack:setPitch(1)
		end

		if public.deafLevel > 0 then
			public.deafLevel = public.deafLevel - public.deafRecoverySpeed
		else
			public.deafLevel = 0
		end
		
	end

	setmetatable(public, self)
	self.__index = self; return public
end



SFXEngine = {}
function SFXEngine:new()
	local public = {}
		public.quitIN		= love.audio.newSource("source/sfx/bb_click_07.wav", "static")
		public.switchLevel	= love.audio.newSource("source/sfx/bb_click_01.wav", "static")
		public.cancel		= love.audio.newSource("source/sfx/bb_click_08.wav", "static")
		public.start		= love.audio.newSource("source/sfx/bb_click_04.wav", "static")
		public.finish		= love.audio.newSource("source/sfx/bb_click_04.wav", "static")
		public.spark = {
			love.audio.newSource("source/sfx/coin_01.wav", "static"),
			love.audio.newSource("source/sfx/coin_02.wav", "static"),
			love.audio.newSource("source/sfx/coin_03.wav", "static"),
			love.audio.newSource("source/sfx/coin_04.wav", "static")
		}
		public.text = love.audio.newSource("source/sfx/text.ogg", "static")
		public.damage = {
			love.audio.newSource("source/sfx/damage_1.wav", "static"),
			love.audio.newSource("source/sfx/damage_2.wav", "static"),
			love.audio.newSource("source/sfx/damage_3.wav", "static"),
			love.audio.newSource("source/sfx/damage_4.wav", "static"),
			love.audio.newSource("source/sfx/damage_5.wav", "static")
		}
		for i = 1, #public.damage do
			public.damage[i]:setVolume(0.3)
		end
		public.door = love.audio.newSource("source/sfx/metaldoor_open_1.ogg", "static")
		public.levelUnlock = love.audio.newSource("source/sfx/key_locking_1.ogg", "static")

		public.talking = {}
		public.talking["triangle"]	= love.audio.newSource("source/sfx/dig_click_06.wav", "static")
		public.talking["square"]	= love.audio.newSource("source/sfx/dig_click_11.wav", "static")
		public.talking["circle"]	= love.audio.newSource("source/sfx/dig_click_03.wav", "static")
		public.talking["heart"]		= love.audio.newSource("source/sfx/dig_click_08.wav", "static")
		public.talking["brilliant"] = love.audio.newSource("source/sfx/dig_click_13.wav", "static")
		public.talking["glitch"]	= {
			love.audio.newSource("source/sfx/dig_click_03.wav", "static"),
			love.audio.newSource("source/sfx/dig_click_06.wav", "static"),
			love.audio.newSource("source/sfx/dig_click_08.wav", "static"),
			love.audio.newSource("source/sfx/dig_click_11.wav", "static"),
			love.audio.newSource("source/sfx/dig_click_13.wav", "static")
		}

	function public:play(sound, speaker)
		if sound == "quitIN" then
			public.quitIN:stop()
			public.quitIN:play()
		elseif sound == "switchLevel" then
			public.switchLevel:stop()
			public.switchLevel:play()
		elseif sound == "cancel" then
			public.cancel:stop()
			public.cancel:play()
		elseif sound == "start" then
			public.start:stop()
			public.start:play()
		elseif sound == "finish" then
			public.finish:stop()
			public.finish:play()
		elseif sound == "spark" then
			for i = 1, #public.spark do
				public.spark[i]:stop()
			end
			local n = math.random(1, #public.spark)
			public.spark[n]:play()
		elseif sound == "text" then
			public.text:stop()
			public.text:play()
		elseif sound == "talking" then
			if not speaker or speaker == '' then
				local n = math.random(1, 5)
				public.talking['glitch'][n]:play()
			else
				public.talking[speaker]:stop()
				public.talking[speaker]:play()
			end
		elseif sound == "door" then
			public.door:stop()
			public.door:play()
		elseif sound == "levelUnlock" then
			public.levelUnlock:stop()
			public.levelUnlock:play()
		elseif sound == "damage" then
			for i = 1, #public.damage do
				public.damage[i]:stop()
			end
			local n = math.random(1, #public.damage)
			public.damage[n]:play()
		elseif sound == "kill" then
			for i = 1, #public.damage do
				public.damage[i]:stop()
			end
			local n = math.random(1, #public.damage)
			public.damage[n]:play()
		end
	end
	
	setmetatable(public, self)
	self.__index = self; return public
end
