Player = {}
function Player:new(newX, newY)
	local public = {}
		public.startX = newX
		public.startY = newY
		public.x = public.startX
		public.y = public.startY
		public.angle = 0
		public.reverse = true
		public.velX = math.cos(public.angle)
		public.velY = math.sin(public.angle)
		public.velocity = 0
		public.acceleration = 0.03
		public.deceleration = 0.01
		public.state = "stop"
		public.speed = 3
		public.rotationSpeed = 3
		public.blendColor = black50
		public.blendShift = 10

		public.size = 20
		public.collisionRadius = public.size
		public.smallCollisionRadius = public.size * 0.5
		public.extendedCollisionRadius = public.size * 1.5

		public.x1v = 0
		public.y1v = -public.size*1.5
		public.x2v = -public.size
		public.y2v = public.size
		public.x3v = public.size
		public.y3v = public.size
		public.x1 = public.x + public.x1v
		public.y1 = public.y + public.y1v
		public.x2 = public.x + public.x2v
		public.y2 = public.y + public.y2v
		public.x3 = public.x + public.x3v
		public.y3 = public.y + public.y3v

		public.tailsize = 90
		public.tail1 = {}
		for i = 1, public.tailsize, 2 do
			public.tail1[i] = public.x2
			public.tail1[i+1] = public.y2
		end
		public.tail2 = {}
		for i = 1, public.tailsize, 2 do
			public.tail2[i] = public.x3
			public.tail2[i+1] = public.y3
		end

		public.dotsR = 0
		public.maxDotsR = 40
		public.dotsSpeed = 4
		public.dotsColor = {}
		public.dotsColor[1] = white[1]
		public.dotsColor[2] = white[2]
		public.dotsColor[3] = white[3]
		public.dotsColor[4] = 0

	function public:setAngle(newAngle)
		public.angle = newAngle
		local cosAngle = math.cos(math.rad(public.angle))
		local sinAngle = math.sin(math.rad(public.angle))
		public.x1 = (public.x + (public.x1v * cosAngle - public.y1v * sinAngle))
		public.y1 = (public.y + (public.y1v * cosAngle + public.x1v * sinAngle))
		public.x2 = (public.x + (public.x2v * cosAngle - public.y2v * sinAngle))
		public.y2 = (public.y + (public.y2v * cosAngle + public.x2v * sinAngle))
		public.x3 = (public.x + (public.x3v * cosAngle - public.y3v * sinAngle))
		public.y3 = (public.y + (public.y3v * cosAngle + public.x3v * sinAngle))

		local xRand = (math.random(0, 2) - 1)
		local yRand = (math.random(0, 2) - 1)

		for i = public.tailsize, 2, -1 do
			public.tail1[i], public.tail1[i-1] = public.tail1[i-2], public.tail1[i-3]
			public.tail2[i], public.tail2[i-1] = public.tail2[i-2], public.tail2[i-3]
		end
		public.tail1[1], public.tail1[2] = public.x2 + xRand, public.y2 + yRand
		public.tail2[1], public.tail2[2] = public.x3 + xRand, public.y3 + yRand
	end

	function public:move()
		public.velocity = public.velocity + public.acceleration
		if public.velocity > 1 then
			public.velocity = 1
		end

		public.rotationVelocity = 0

		public.velX = math.sin(math.rad(public.angle)) * public.velocity
		public.velY = - math.cos(math.rad(public.angle)) * public.velocity
		
		public.x = public.x + public.velX * public.speed
		public.y = public.y + public.velY * public.speed

		local cosAngle = math.cos(math.rad(public.angle))
		local sinAngle = math.sin(math.rad(public.angle))
	
		public.x1 = (public.x + (public.x1v * cosAngle - public.y1v * sinAngle))
		public.y1 = (public.y + (public.y1v * cosAngle + public.x1v * sinAngle))
		public.x2 = (public.x + (public.x2v * cosAngle - public.y2v * sinAngle))
		public.y2 = (public.y + (public.y2v * cosAngle + public.x2v * sinAngle))
		public.x3 = (public.x + (public.x3v * cosAngle - public.y3v * sinAngle))
		public.y3 = (public.y + (public.y3v * cosAngle + public.x3v * sinAngle))

		local xRand = (math.random(0, 4) - 2)
		local yRand = (math.random(0, 4) - 2)

		for i = public.tailsize, 2, -1 do
			public.tail1[i], public.tail1[i-1] = public.tail1[i-2], public.tail1[i-3]
			public.tail2[i], public.tail2[i-1] = public.tail2[i-2], public.tail2[i-3]
		end
		public.tail1[1], public.tail1[2] = public.x2 + xRand, public.y2 + yRand
		public.tail2[1], public.tail2[2] = public.x3 + xRand, public.y3 + yRand
	end

	function public:rotate()
		public.velocity = public.velocity - public.deceleration
		if public.velocity < 0 then
			public.velocity = 0
		end

		public.velX = public.velX * public.velocity
		public.velY = public.velY * public.velocity
		
		public.x = public.x + public.velX * public.speed
		public.y = public.y + public.velY * public.speed

		if public.reverse == false then
			if public.angle < 360 then
				public.angle = public.angle + public.rotationSpeed
			else
				public.angle = 0
			end
		else
			if public.angle > 0 then
				public.angle = public.angle - public.rotationSpeed
			else
				public.angle = 360
			end
		end
		public:setAngle(public.angle)
	end

	function public.update()
		if public.state == "stop" then
			public:move()

			if public.dotsR > 0 then
				public.dotsR = public.dotsR - public.dotsSpeed
			else
				public.dotsR = 0
			end
			
			if game.hud.pressAnyKey.state ~= "hidden" then
				game.hud.pressAnyKey.state = "hidden"
				game.hud.pressAnyKey.timer = 0
			end
			
		elseif public.state == "pause" then
			public:rotate()

			if public.dotsR < public.maxDotsR then
				public.dotsR = public.dotsR + public.dotsSpeed
			else
				public.dotsR = public.maxDotsR
			end

			if game.hud.pressAnyKey.state == "hidden" then
				game.hud.pressAnyKey.state = "toshown"
			end
		end
	end

	function public:draw(layer)
		local randomColorsMode = game.levelsScreen.selectedLevel > 50
		if layer == -1 then
			public.dotsColor[4] = (public.dotsR/public.maxDotsR-0.5)*2
			for i = 1, 24 do
				local cosAngle = math.cos(math.rad((i - 1) * 15 + public.angle/3))
				local sinAngle = math.sin(math.rad((i - 1) * 15 + public.angle/3))

				local dotX1 = public.x + (public.dotsR)*cosAngle
				local dotY1 = public.y - (public.dotsR)*sinAngle
				local dotX2 = public.x + (public.dotsR - 3 - 5*(1-public.dotsR/public.maxDotsR))*cosAngle
				local dotY2 = public.y - (public.dotsR - 3 - 5*(1-public.dotsR/public.maxDotsR))*sinAngle
				if randomColorsMode then
					love.graphics.setColor(math.random(), math.random(), math.random(), public.dotsColor[4])
				else
					love.graphics.setColor(public.dotsColor)
				end
				love.graphics.line(dotX1, dotY1 - public.blendShift/3, dotX2, dotY2 - public.blendShift/3)
			end

		elseif layer == 0 then
			local cosAngle = math.cos(math.rad(public.angle))
			local sinAngle = math.sin(math.rad(public.angle))
			love.graphics.setColor(black10)
			for i = public.size - 8, public.size, 2 do
				local coef = i/public.size
				local x1 = (public.x + (public.x1v*coef * cosAngle - public.y1v*coef * sinAngle))
				local y1 = (public.y + (public.y1v*coef * cosAngle + public.x1v*coef * sinAngle))
				local x2 = (public.x + (public.x2v*coef * cosAngle - public.y2v*coef * sinAngle))
				local y2 = (public.y + (public.y2v*coef * cosAngle + public.x2v*coef * sinAngle))
				local x3 = (public.x + (public.x3v*coef * cosAngle - public.y3v*coef * sinAngle))
				local y3 = (public.y + (public.y3v*coef * cosAngle + public.x3v*coef * sinAngle))
				love.graphics.polygon("fill", x1, y1 + public.blendShift, x2, y2 + public.blendShift, x3, y3 + public.blendShift)
			end

			local tailColor = {}
			tailColor[1] = white[1]
			tailColor[2] = white[2]
			tailColor[3] = white[3]

			for i = 1, public.tailsize, 2 do
				tailColor[4] = 1 - i / public.tailsize
				
				love.graphics.setColor(public.blendColor[1], public.blendColor[2], public.blendColor[3], tailColor[4]/6)
				love.graphics.points(public.tail1[i], public.tail1[i+1] + public.blendShift)
				love.graphics.points(public.tail2[i], public.tail2[i+1] + public.blendShift)
				love.graphics.circle("fill", public.tail1[i], public.tail1[i+1] + public.blendShift, 1)
				love.graphics.circle("fill", public.tail2[i], public.tail2[i+1] + public.blendShift, 1)

				if i < public.tailsize - 1 then
					love.graphics.points((public.tail1[i]+public.tail1[i+2])/2, (public.tail1[i+1]+public.tail1[i+3])/2)
					love.graphics.points((public.tail2[i]+public.tail2[i+2])/2, (public.tail2[i+1]+public.tail2[i+3])/2)
				end

				if randomColorsMode then
					love.graphics.setColor(math.random(), math.random(), math.random(), tailColor[4])
				else
					love.graphics.setColor(tailColor)
				end
				love.graphics.points(public.tail1[i], public.tail1[i+1])
				love.graphics.points(public.tail2[i], public.tail2[i+1])
				if i < public.tailsize - 1 then
					love.graphics.points((public.tail1[i]+public.tail1[i+2])/2, (public.tail1[i+1]+public.tail1[i+3])/2)
					love.graphics.points((public.tail2[i]+public.tail2[i+2])/2, (public.tail2[i+1]+public.tail2[i+3])/2)
				end
			end

			love.graphics.setColor(white)
			love.graphics.polygon("fill", public.x1, public.y1, public.x2, public.y2, public.x3, public.y3)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Finish = {}
function Finish:new(newX, newY, superMode)
	local public = {}
		public.state = "waiting"
		public.x = newX
		public.y = newY
		public.constR = 40
		public.superMode = superMode
		if public.superMode then
			public.constR = public.constR * 2
		end
		public.r = public.constR
		public.animationState = "up"
		public.animationMin = 1
		public.animationRate = public.animationMin
		public.animationMax = 1.2
		public.distance = public.r*2
		public.color = white
		public.blendColor = black50
		public.blendShift = 10
		public.activationShift = public.blendShift
		public.activationSpeed = 1
		public.animationSpeed = 0.005
		public.quitingAnimationSpeed = 3

	function public:update()
		if public.state == "waiting" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < (public.r + game.gameScreen.player.collisionRadius) * 4 then
				public.state = "tostandby"
			end

		elseif public.state == "tostandby" then
			if public.activationShift > 0 then
				public.activationShift = public.activationShift - public.activationSpeed
			else
				public.activationShift = 0
				public.state = "standby"
			end

		elseif public.state == "standby" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < (public.r + game.gameScreen.player.collisionRadius) then
				if game.hud.webs.maskOpacity > 0 then
					if game.gameScreen.sparksCount > maximumSparksLevel[game.gameScreen.level] then
						maximumSparksLevel[game.gameScreen.level] = game.gameScreen.sparksCount
					end
					public.state = "quiting"
					game.sfxEngine:play("finish")
					game.hud.mask.state = "fadeout"
					game.hud.mask.color[4] = 0
				end
			end

			if public.animationState == "up" then
				if public.animationRate < public.animationMax then
					public.animationRate = public.animationRate + public.animationSpeed
				else
					public.animationRate = public.animationMax
					public.animationState = "down"
				end
			elseif public.animationState == "down" then
				if public.animationRate > public.animationMin then
					public.animationRate = public.animationRate - public.animationSpeed
				else
					public.animationRate = public.animationMin
					public.animationState = "up"
				end
			end

		elseif public.state == "quiting" then
			if public.r > 0 then
				public.r = public.r - public.quitingAnimationSpeed
			else
				public.r = 0
			end
			
			game.gameScreen.player.speed = game.gameScreen.player.speed - public.animationSpeed*10

			if game.hud.mask.state == "standby" then
				game.sfxEngine:play("cancel")
				game.state = "levels"
				game.hud.mask.state = "fromgame"
				game.hud.mask.color[4] = 1
				game.levelsScreen.screenOpacity = 1
				game.levelsScreen.state = "justfromgame"
				game.hud.webs:toDefault()
				game.hud.webs.state = "standby"
				game.gameScreen.collisionCount = 0
				if game.levelsScreen.selectedLevel ~= game.levelsScreen.maxOpenedLevel
				and math.fmod(game.levelsScreen.maxOpenedLevel, 10) <= (game.levelsScreen.maxOpenedLevel - game.levelsScreen.selectedLevel) then
					game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
				end
				game.levelsScreen.worldName = ""
				game.levelsScreen.nextWorldName = getWorldName(game.levelsScreen.maxOpenedLevel)
				game.levelsScreen.worldNameState = "out"
			end

		end

		
	end

	function public:draw(layer)
		if layer == -1 then
			love.graphics.setColor(game.gameScreen.mainColor)
			love.graphics.circle("fill", public.x - public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.4, 3)
			love.graphics.circle("fill", public.x + public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.4, 3)
			love.graphics.setColor(game.background.color)
			love.graphics.circle("fill", public.x - public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.2, 3)
			love.graphics.circle("fill", public.x + public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.2, 3)
			love.graphics.setColor(game.gameScreen.gameShining.color)
			love.graphics.circle("fill", public.x - public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.2, 3)
			love.graphics.circle("fill", public.x + public.constR/2, public.y + public.constR - public.constR + public.blendShift, public.constR*1.2, 3)

			if public.state ~= "standby" then
				love.graphics.setColor(game.gameScreen.lightColor)
				love.graphics.circle("fill", public.x - public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift, public.r, 3)
				love.graphics.circle("fill", public.x + public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift, public.r, 3)				
			end

		elseif layer == 1 then
			if public.state == "standby" or public.state == "quiting" or public.state == "tostandby" then

				if public.r > 0 then
					love.graphics.setColor(black10)
					for i = public.r - 6, public.r, 2 do
						love.graphics.circle("fill", public.x - public.r/2, public.y + public.blendShift, i/public.animationRate, 3)
						love.graphics.circle("fill", public.x + public.r/2, public.y + public.blendShift, i/public.animationRate, 3)
					end
				end
				
				for i = 1, 5 do
					love.graphics.setColor(game.gameScreen.mainColor[1] * (1 + i*0.05 - 0.25), game.gameScreen.mainColor[2] * (1 + i*0.05  - 0.25), game.gameScreen.mainColor[3] * (1 + i*0.05 - 0.25), game.gameScreen.mainColor[4])
					love.graphics.circle("fill", public.x - public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift + 5 - i, public.r, 3)
					love.graphics.circle("fill", public.x + public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift + 5 - i, public.r, 3)
				end

				love.graphics.polygon("fill",
					public.x + public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift,
					public.x + public.r/2 + public.r, public.y + public.r - public.r*public.animationRate + public.activationShift,
					public.x + public.r/2 + public.r, public.y + public.r - public.r*public.animationRate + public.activationShift - 5,
					public.x + public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift - 5)
				love.graphics.setColor(game.gameScreen.lightColor)
				love.graphics.circle("fill", public.x - public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift - 5, public.r, 3)
				love.graphics.circle("fill", public.x + public.r/2, public.y + public.r - public.r*public.animationRate + public.activationShift - 5, public.r, 3)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Wall = {}
function Wall:new(newX, newY, newW, newH)
	local public = {}
		public.state = "standby"
		public.x = newX
		public.y = newY
		public.w = newW
		public.h = newH
		public.color = darkColdGrey
		public.blendShift = 20
		public.frameSize = 2
		public.distance = 0

	function public:update()

		-- collision with player
		local testX = game.gameScreen.player.x
		local testY = game.gameScreen.player.y
		
		if (game.gameScreen.player.x < public.x) then
			testX = public.x
		elseif (game.gameScreen.player.x > public.x + public.w) then
			testX = public.x + public.w
		end

		if (game.gameScreen.player.y < public.y) then
			testY = public.y
		elseif (game.gameScreen.player.y > public.y + public.h + 15) then
			testY = public.y + public.h + 15
		end

		local distance = math.sqrt( (game.gameScreen.player.x - testX)^2 + (game.gameScreen.player.y - testY)^2 )

		if (distance <= game.gameScreen.player.smallCollisionRadius) then
			if game.gameScreen.player.velocity > 0 then game.gameScreen.player.velocity = 0 end
			game.gameScreen.player.x = game.gameScreen.player.x - game.gameScreen.player.velX * game.gameScreen.player.speed
			game.gameScreen.player.y = game.gameScreen.player.y - game.gameScreen.player.velY * game.gameScreen.player.speed
			game.gameScreen.player.state = "pause"
			game.gameScreen.player.reverse = not game.gameScreen.player.reverse
		end

	end

	function public:draw(layer)
		if layer == -2 then
			love.graphics.setColor(black10)
			for i = 1, 5 do
				love.graphics.rectangle("fill",
					public.x - i/2,
					public.y + 10 + i*3,
					public.w + i,
					public.h)
			end

			if public.y + public.h < game.gameScreen.player.y then
				for i = 1, 15 do
					love.graphics.setColor(game.gameScreen.mainColor[1] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[2] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[3] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[4])
					love.graphics.rectangle("fill",
						public.x,
						public.y + 15 - i,
						public.w,
						public.h)
				end
			end

		elseif layer == -1 then
			if public.y + public.h < game.gameScreen.player.y then
				love.graphics.setColor(public.color)
				love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
				if game.hud.webs.flashColor[4] > 0 then
					love.graphics.setColor(game.hud.webs.flashColor)
					love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
				end
			end

		elseif layer == 1 then
			if public.y + public.h >= game.gameScreen.player.y then
				for i = 1, 15 do
					love.graphics.setColor(game.gameScreen.mainColor[1] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[2] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[3] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[4])
					love.graphics.rectangle("fill",
						public.x,
						public.y + 15 - i,
						public.w,
						public.h)
				end
			end
		elseif layer == 2 then
			if public.y + public.h >= game.gameScreen.player.y then
				love.graphics.setColor(public.color)
				love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
				if game.hud.webs.flashColor[4] > 0 then
					love.graphics.setColor(game.hud.webs.flashColor)
					love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
				end
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



DissappearingWall = {}
function DissappearingWall:new(newX, newY, newW, newH)
	local public = {}
		public.state = "standby"
		public.x = newX
		public.y = newY
		public.w = newW
		public.h = newH
		public.color = {}
		public.color[1] = darkColdGrey[1]
		public.color[2] = darkColdGrey[2]
		public.color[3] = darkColdGrey[3]
		public.color[4] = 1
		public.animationSpeed = 0.05
		public.distance = 0

	function public:update()
		local testX = game.gameScreen.player.x
		local testY = game.gameScreen.player.y
		
		if (game.gameScreen.player.x < public.x) then
			testX = public.x
		elseif (game.gameScreen.player.x > public.x + public.w) then
			testX = public.x + public.w
		end

		if (game.gameScreen.player.y < public.y) then
			testY = public.y
		elseif (game.gameScreen.player.y > public.y + public.h) then
			testY = public.y + public.h
		end

		local distance = math.sqrt( (game.gameScreen.player.x - testX)^2 + (game.gameScreen.player.y - testY)^2 )

		if (distance <= game.gameScreen.player.collisionRadius) then
			public.state = "collide"
		else
			public.state = "standby"
		end

		if public.state == "collide" then
			if public.color[4] > 0.5 then
				public.color[4] = public.color[4] - public.animationSpeed
			else
				public.color[4] = 0.5
			end
		elseif public.state == "standby" then
			if public.color[4] < 1 then
				public.color[4] = public.color[4] + public.animationSpeed
			else
				public.color[4] = 1
			end
		end

	end

	function public:draw(layer)
		if layer == 2 then
			love.graphics.setColor(public.color)
			love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			if game.hud.webs.flashColor[4] > 0 then
				love.graphics.setColor(game.hud.webs.flashColor)
				love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			end
		end

	end

	setmetatable(public, self)
	self.__index = self; return public
end



Door = {}
function Door:new(newX, newY, newW, newH, maxNumber)
	local public = {}
		public.state = "locked"
		public.x = newX
		public.y = newY
		public.w = newW
		public.h = newH
		public.maxNumber = maxNumber
		public.number = 0
		public.color = {}
		public.color[1] = coldGrey[1]
		public.color[2] = coldGrey[2]
		public.color[3] = coldGrey[3]
		public.color[4] = 1
		public.animationSpeed = 0.1
		public.distance = 0
		public.shift = 15
		public.unlockingSpeed = 1

	function public:update()

		if public.state == "locked" or public.state == "unlocking" then

			local testX = game.gameScreen.player.x
			local testY = game.gameScreen.player.y
			
			if (game.gameScreen.player.x < public.x) then
				testX = public.x
			elseif (game.gameScreen.player.x > public.x + public.w) then
				testX = public.x + public.w
			end

			if (game.gameScreen.player.y < public.y) then
				testY = public.y
			elseif (game.gameScreen.player.y > public.y + public.h + public.shift) then
				testY = public.y + public.h + public.shift
			end

			local distance = math.sqrt( (game.gameScreen.player.x - testX)^2 + (game.gameScreen.player.y - testY)^2 )

			if (distance <= game.gameScreen.player.smallCollisionRadius) then
				if game.gameScreen.player.velocity > 0 then game.gameScreen.player.velocity = 0 end
				game.gameScreen.player.x = game.gameScreen.player.x - game.gameScreen.player.velX * game.gameScreen.player.speed
				game.gameScreen.player.y = game.gameScreen.player.y - game.gameScreen.player.velY * game.gameScreen.player.speed
				game.gameScreen.player.state = "pause"
				game.gameScreen.player.reverse = not game.gameScreen.player.reverse
			end

			if game.gameScreen.sparksCount > public.number then
				public.number = public.number + public.animationSpeed
			end

			if public.number >= public.maxNumber then
				public.state = "unlocking"
				game.sfxEngine:play("door")
			end

		end
		
		if public.state == "locked" then
			-- no code

		elseif public.state == "unlocking" then
			if public.shift > 0 then
				public.shift = public.shift - public.unlockingSpeed
				public.y = public.y + public.unlockingSpeed
			else
				public.state = "unlocked"
			end

		elseif public.state == "unlocked" then
			
		end

	end

	function public:draw(layer)
		if layer == -2 then
			if public.state == "locked" then
				love.graphics.setColor(black10)
				for i = 1, 5 do
					love.graphics.rectangle("fill",
						public.x - i/2,
						public.y + 10 + i*3,
						public.w + i,
						public.h)
				end
			end
	
			for i = 1, public.shift do
				love.graphics.setColor(game.gameScreen.mainColor[1] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[2] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[3] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[4])
				love.graphics.rectangle("fill",
					public.x,
					public.y + public.shift - i,
					public.w,
					public.h)
			end

		elseif (public.state == "unlocked" and layer == -1) or (public.state ~= "unlocked" and layer == 1) then
			love.graphics.setColor(game.gameScreen.mainColor)
			love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			love.graphics.setColor(game.gameScreen.darkColor)
			love.graphics.rectangle("fill", public.x + 5, public.y + 5, public.w - 10, public.h - 10)
			love.graphics.setColor(game.gameScreen.mainColor)
			love.graphics.rectangle("fill", public.x + 10, public.y + 10, public.w - 20, public.h - 20)
			if game.levelsScreen.selectedLevel > 50 then
				love.graphics.setColor(yellow[1], yellow[2], yellow[3], public.shift/15)
			else
				love.graphics.setColor(lightYellow[1], lightYellow[2], lightYellow[3], public.shift/15)
			end
			local doorProgress = public.number/public.maxNumber
			if doorProgress > 1 then doorProgress = 1 end
			love.graphics.circle("line", public.x + public.w/2, public.y + public.h/2, 20, 4)
			love.graphics.circle("fill", public.x + public.w/2, public.y + public.h/2, 20 * doorProgress, 4)
			love.graphics.setColor(game.gameScreen.darkColor[1], game.gameScreen.darkColor[2], game.gameScreen.darkColor[3], 1 - public.shift/15)
			love.graphics.circle("fill", public.x + public.w/2, public.y + public.h/2, 20, 4)
			if game.hud.webs.flashColor[4] > 0 then
				love.graphics.setColor(game.hud.webs.flashColor)
				love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			end

		end

	end

	setmetatable(public, self)
	self.__index = self; return public
end



Spark = {}
function Spark:new(newX, newY)
	local public = {}
		public.state = "standby"
		public.minSize = 15
		public.maxSize = 20
		public.x = newX
		public.y = newY
		public.r = public.minSize
		public.distance = public.r*2
		public.ringR = 0
		public.color = {}
		public.color[1] = lightYellow[1]
		public.color[2] = lightYellow[2]
		public.color[3] = lightYellow[3]
		public.color[4] = 1
		public.ringColor = {}
		public.ringColor[1] = white[1]
		public.ringColor[2] = white[2]
		public.ringColor[3] = white[3]
		public.ringColor[4] = 0.75
		public.blendColor = black50
		public.blendShift = 10
		public.animationSpeed = 0.5
		public.sizeAnimationSpeed = 0.3
		public.breathingDirection = "up"

	function public:update()
		if public.state == "standby" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.extendedCollisionRadius then
				public.r = public.minSize
				public.state = "disappearing"
				public.r = public.r * 1.2
				game.gameScreen.sparksCount = game.gameScreen.sparksCount + 1
				if game.hud.sparks.state == "standby" then
					game.hud.sparks.state = "popup"
				end
				game.sfxEngine:play("spark")
			end

			if public.breathingDirection == "up" then
				if public.r < public.maxSize then
					public.r = public.r + public.sizeAnimationSpeed
				else
					public.r = public.maxSize
					public.breathingDirection = "down"
				end

			elseif public.breathingDirection == "down" then
				if public.r > public.minSize then
					public.r = public.r - public.sizeAnimationSpeed
				else
					public.r = public.minSize
					public.breathingDirection = "up"
				end

			end

		elseif public.state == "disappearing" then
			if public.ringColor[4] > 0 then
				public.r = public.r - public.animationSpeed
				public.ringR = public.ringR + public.animationSpeed * 8
				public.ringColor[4] = public.r/public.minSize - 0.5
				public.color[4] = public.r/public.minSize - 0.5
			else
				public.state = "hidden"
			end

		elseif public.state == "hidden" then
			-- no code

		end
	end

	function public:draw(layer)
		if public.state == "standby" then
			if layer == -1 then
				love.graphics.setColor(black10)
				for i = public.r - 6, public.r, 2 do
					love.graphics.circle("fill", public.x, public.y + public.blendShift, i, 4)
				end
			elseif layer == 1 then
				love.graphics.setColor(yellow)
				love.graphics.circle("fill", public.x, public.y, public.r, 4)
				love.graphics.polygon("fill",
					public.x - public.r, public.y,
					public.x + public.r, public.y,
					public.x + public.r, public.y - 5,
					public.x - public.r, public.y - 5)
				love.graphics.setColor(public.color)
				love.graphics.circle("fill", public.x, public.y - 5, public.r, 4)
			end
		elseif public.state == "disappearing" then
			if layer == -1 then
				love.graphics.setColor(0, 0, 0, 0.1 * public.r/public.minSize)
				for i = public.r/3, public.r do
					love.graphics.circle("fill", public.x, public.y + public.blendShift, i, 4)
				end
				public.ringColor[1] = game.gameScreen.lightColor[1]
				public.ringColor[2] = game.gameScreen.lightColor[2]
				public.ringColor[3] = game.gameScreen.lightColor[3]
				love.graphics.setColor(public.ringColor)
				love.graphics.circle("line", public.x, public.y, public.ringR, 64)
			elseif layer == 1 then
				love.graphics.setColor(public.color)
				love.graphics.circle("fill", public.x, public.y - public.ringR*1.5, public.r*public.r/15, 4)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Circle = {}
function Circle:new(newX, newY, newRadius)
	local public = {}
		public.state = "standby"
		public.x = newX
		public.y = newY
		public.constX = public.x
		public.constY = public.y
		public.r = newRadius
		public.distance = public.r*2
		public.blendColor = black50
		public.blendShift = 10
		public.collisionExpand = 5
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.02
		public.dieScale = 1
		public.dieAnimationSpeed = 0.05

	function public:update()
		if game.levelsScreen.selectedLevel == 60 then
			public.x = public.constX + math.random(0, 200) - 100
			public.y = public.constY + math.random(0, 200) - 100
		end

		local mortal = false
		if game.levelsScreen.selectedLevel > 50 then
			mortal = true
		end

		if public.state == "dead" then
			-- no code
		else
			if public.state == "standby" then
				if game.gameScreen.state == "waiting" then
					if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
						if mortal then
							public.state = "die"
							game.sfxEngine:play("kill")
						else
							public.state = "collision"
							game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
							game.hud.webs:createNext(game.gameScreen.collisionCount)
							public.ringColor[4] = 1
						end
					end
				end

				if public.collisionExpand < 5 then
					public.collisionExpand = public.collisionExpand + public.expandSpeed/4
				else
					public.collisionExpand = 5
				end

				if public.ringColor[4] > 0 then
					public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
				else
					public.ringColor[4] = 0
				end

			elseif public.state == "collision" then

				if public.collisionExpand > 2 then
					public.collisionExpand = public.collisionExpand - public.expandSpeed
				else
					public.collisionExpand = 2
				end

			elseif public.state == "die" then
				if public.dieScale > 0 then
					public.dieScale = public.dieScale - public.dieAnimationSpeed
				else
					public.state = "dead"
				end

			end

			if public.state == "dead" then
				-- no code
			else
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					if mortal then
						public.state = "die"
					else
						public.state = "collision"
						game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
					end
				else
					if public.state == "die" then
						-- no code
					else
						public.state = "standby"
					end
				end
			end
		end
	end

	function public:draw(layer)
		if public.state == "dead" then
			-- no code
		else
			if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
				if layer == 2 then
					local circle = {}
					local verticlesNumber = 96*public.dieScale
					if verticlesNumber < 3 then verticlesNumber = 3 end
					for i = 1, verticlesNumber do
						local cosAngle = math.cos(math.rad((i - 1) * 3.75))
						local sinAngle = math.sin(math.rad((i - 1) * 3.75))
						local move = math.random()/public.collisionExpand*3 + 0.6
						local dotX = public.x + (public.r*move*public.dieScale)*cosAngle
						local dotY = public.y - (public.r*move*public.dieScale)*sinAngle
						table.insert(circle, dotX)
						table.insert(circle, dotY)
					end

					circles = {}
					if (math.random() > 0.99) then
						love.graphics.setColor(math.random(), math.random(), math.random(), 1)
					else
						love.graphics.setColor(darkColdGrey)
					end
					circles[1] = shuffleArray(circle)
					love.graphics.polygon('line', circles[1])

					love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
					love.graphics.polygon('line', circles[1])
				end
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



SizeCircle = {}
function SizeCircle:new(newX, newY, newMinR, newMaxR, newRadius)
	local public = {}
		public.state = "standby"
		public.sizeState = "up"
		public.sizeAnimationSpeed = 1
		public.x = newX
		public.y = newY
		public.minR = newMinR
		public.maxR = newMaxR
		public.r = newRadius
		public.distance = public.r*2
		public.blendColor = black50
		public.blendShift = 10
		public.collisionColor = {}
		public.collisionColor[1] = white[1]
		public.collisionColor[2] = white[2]
		public.collisionColor[3] = white[3]
		public.collisionColor[4] = 0
		public.collisionColorLimit = 0.3
		public.collisionColorAnimationSpeed = 0.05
		public.collisionExpand = 10
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.03
		public.glitchColor = {0, 0, 0, 0}
		public.glitchColorAnimtionSpeed = 0.2
		public.glitchState = false
		public.dieScale = 1
		public.dieAnimationSpeed = 0.05

	function public:update()
		local mortal = false
		if game.levelsScreen.selectedLevel > 50 then
			mortal = true
		end
		
		public.distance = public.r*2
		if public.sizeState == "up" then
			if public.r < public.maxR then
				public.r = public.r + public.sizeAnimationSpeed
			else
				public.r = public.maxR
				public.sizeState = "down"
			end
		elseif public.sizeState == "down" then
			if public.r > public.minR then
				public.r = public.r - public.sizeAnimationSpeed
			else
				public.r = public.minR
				public.sizeState = "up"
			end
		end

		if public.state == "dead" then
			-- no code
		else
			if public.state == "standby" then
				if game.gameScreen.state == "waiting" then
					if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
						if mortal then
							public.state = "die"
							game.sfxEngine:play("kill")
						else
							public.state = "collision"
							game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
							game.hud.webs:createNext(game.gameScreen.collisionCount)
							public.ringColor[4] = 1
						end
					end
				end

				if public.collisionExpand < 15 then
					public.collisionExpand = public.collisionExpand + public.expandSpeed
				else
					public.collisionExpand = 15
				end

				if public.ringColor[4] > 0 then
					public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
				else
					public.ringColor[4] = 0
				end

			elseif public.state == "collision" then
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					-- no code
				else
					public.state = "standby"
				end

				if public.collisionExpand > 3 then
					public.collisionExpand = public.collisionExpand - public.expandSpeed
				else
					public.collisionExpand = 3
				end

			elseif public.state == "die" then
				if public.dieScale > 0 then
					public.dieScale = public.dieScale - public.dieAnimationSpeed
				else
					public.state = "dead"
				end

			end
		end

		if public.state == "dead" then
			-- no code
		else
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
				if mortal then
					public.state = "die"
				else
					public.state = "collision"
					game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
				end
			else
				if public.state == "die" then
					-- no code
				else
					public.state = "standby"
				end
			end
		end

		if public.glitchState == true then
			if public.glitchColor[4] < 1 then
				public.glitchColor[4] = public.glitchColor[4] + public.glitchColorAnimtionSpeed
			else
				public.glitchColor[4] = 1
				public.glitchState = false
			end
		else
			if public.glitchColor[4] > 0 then
				public.glitchColor[4] = public.glitchColor[4] - public.glitchColorAnimtionSpeed
			else
				public.glitchColor[4] = 0
			end
		end

		if math.random() > 0.99 and public.glitchColor[4] == 0 then
			public.glitchColor = {math.random(), math.random(), math.random(), 0}
			public.glitchState = true
		end

	end

	function public:draw(layer)
		if public.state == "dead" then
			-- no code
		else
			if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
				if layer == 2 then
					local circle = {}
					local verticlesNumber = 96*public.dieScale
					if verticlesNumber < 3 then verticlesNumber = 3 end
					for i = 1, verticlesNumber do
						local cosAngle = math.cos(math.rad((i - 1) * 3.75))
						local sinAngle = math.sin(math.rad((i - 1) * 3.75))
						local move = math.random()/public.collisionExpand*3 + 0.6
						local dotX = public.x + (public.r*move*public.dieScale)*cosAngle
						local dotY = public.y - (public.r*move*public.dieScale)*sinAngle
						table.insert(circle, dotX)
						table.insert(circle, dotY)
					end

					circles = {}
					if (math.random() > 0.99) then
						love.graphics.setColor(math.random(), math.random(), math.random(), 1)
					else
						love.graphics.setColor(darkColdGrey)
					end
					circles[1] = shuffleArray(circle)
					love.graphics.polygon('line', circles[1])

					love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
					love.graphics.polygon('line', circles[1])
				end
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



MoveCircle = {}
function MoveCircle:new(newCoordinates, newRadius, newSpeed, newMovementType)
	local public = {}
		public.state = "standby"
		public.animationSpeed = newSpeed
		public.coordinates = {}
		for i = 1, #newCoordinates do
			public.coordinates[i] = {}
			public.coordinates[i].x = newCoordinates[i][1]
			public.coordinates[i].y = newCoordinates[i][2]
		end
		
		if newMovementType == "pong" then
			public.movementType = "pong"
			public.movementDirection = "forward"
		else
			public.movementType = "ring"
		end

		public.step = 1

		public.x = public.coordinates[1].x
		public.y = public.coordinates[1].y
		public.r = newRadius

		public.distance = public.r*2
		
		public.blendColor = black50
		public.blendShift = 10
		public.collisionColor = {}
		public.collisionColor[1] = white[1]
		public.collisionColor[2] = white[2]
		public.collisionColor[3] = white[3]
		public.collisionColor[4] = 0
		public.collisionColorLimit = 0.3
		public.collisionColorAnimationSpeed = 0.05
		public.collisionExpand = 10
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.03
		public.dieScale = 1
		public.dieAnimationSpeed = 0.05

	function public:update()
		local mortal = false
		if game.levelsScreen.selectedLevel > 50 then
			mortal = true
		end
		
		public.distance = public.r*2
		
		if public.movementType == "pong" then
			if public.movementDirection == "forward" then
				if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
					if public.step < #public.coordinates - 1 then
						public.step = public.step + 1
					else
						public.movementDirection = "backward"
					end
				else
					if public.x < public.coordinates[public.step + 1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step + 1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step + 1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step + 1].y then
						public.y = public.y - public.animationSpeed
					end
				end
			elseif public.movementDirection == "backward" then
				if (math.abs(public.x - public.coordinates[public.step].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step].y) < public.animationSpeed) then
					if public.step > 1 then
						public.step = public.step - 1
					else
						public.movementDirection = "forward"
					end
				else
					if public.x < public.coordinates[public.step].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step].y then
						public.y = public.y - public.animationSpeed
					end
				end
			end
		elseif public.movementType == "ring" then
			if public.step < #public.coordinates then
				if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
					public.step = public.step + 1
				else
					if public.x < public.coordinates[public.step + 1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step + 1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step + 1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step + 1].y then
						public.y = public.y - public.animationSpeed
					end
				end
			else
				if (math.abs(public.x - public.coordinates[1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[1].y) < public.animationSpeed) then
					public.step = 1
				else
					if public.x < public.coordinates[1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[1].y then
						public.y = public.y - public.animationSpeed
					end
				end
			end
		end

		if public.state == "dead" then
			-- no code
		else
			if public.state == "standby" then
				if game.gameScreen.state == "waiting" then
					if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
						if mortal then
							public.state = "die"
							game.sfxEngine:play("kill")
						else
							public.state = "collision"
							game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
							game.hud.webs:createNext(game.gameScreen.collisionCount)
							public.ringColor[4] = 1
						end
					end
				end

				if public.ringColor[4] > 0 then
					public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
				else
					public.ringColor[4] = 0
				end

				if public.collisionExpand < 15 then
					public.collisionExpand = public.collisionExpand + public.expandSpeed
				else
					public.collisionExpand = 15
				end

			elseif public.state == "collision" then
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					-- no code
				else
					public.state = "standby"
				end

				if public.collisionExpand > 3 then
					public.collisionExpand = public.collisionExpand - public.expandSpeed
				else
					public.collisionExpand = 3
				end

			elseif public.state == "die" then
				if public.dieScale > 0 then
					public.dieScale = public.dieScale - public.dieAnimationSpeed
				else
					public.state = "dead"
				end

			end

			if public.state == "dead" then
				-- no code
			else
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					if mortal then
						public.state = "die"
					else
						public.state = "collision"
						game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
					end
				else
					if public.state == "die" then
						-- no code
					else
						public.state = "standby"
					end
				end
			end
		end
	end

	function public:draw(layer)
		if public.state == "dead" then
			-- no code
		else
			if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
				if layer == 2 then
					local circle = {}
					local verticlesNumber = 96*public.dieScale
					if verticlesNumber < 3 then verticlesNumber = 3 end
					for i = 1, verticlesNumber do
						local cosAngle = math.cos(math.rad((i - 1) * 3.75))
						local sinAngle = math.sin(math.rad((i - 1) * 3.75))
						local move = math.random()/public.collisionExpand*3 + 0.6
						local dotX = public.x + (public.r*move*public.dieScale)*cosAngle
						local dotY = public.y - (public.r*move*public.dieScale)*sinAngle
						table.insert(circle, dotX)
						table.insert(circle, dotY)
					end

					circles = {}
					if (math.random() > 0.99) then
						love.graphics.setColor(math.random(), math.random(), math.random(), 1)
					else
						love.graphics.setColor(darkColdGrey)
					end
					circles[1] = shuffleArray(circle)
					love.graphics.polygon('line', circles[1])

					love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
					love.graphics.polygon('line', circles[1])
				end
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



MoveSizeCircle = {}
function MoveSizeCircle:new(newCoordinates, newSpeed, newMovementType)
	local public = {}
		public.state = "standby"
		public.animationSpeed = newSpeed
		public.coordinates = {}
		for i = 1, #newCoordinates do
			public.coordinates[i] = {}
			public.coordinates[i].x = newCoordinates[i][1]
			public.coordinates[i].y = newCoordinates[i][2]
			public.coordinates[i].r = newCoordinates[i][3]
		end
		
		if newMovementType == "pong" then
			public.movementType = "pong"
			public.movementDirection = "forward"
		else
			public.movementType = "ring"
		end

		public.step = 1

		public.x = public.coordinates[1].x
		public.y = public.coordinates[1].y
		public.r = public.coordinates[1].r

		public.distance = public.r*2
		
		public.blendColor = black50
		public.blendShift = 10
		public.collisionExpand = 10
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.03

	function public:update()
		
		public.distance = public.r*2

		if public.movementType == "pong" then
			if public.movementDirection == "forward" then
				if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
					if public.step < #public.coordinates - 1 then
						public.step = public.step + 1
					else
						public.movementDirection = "backward"
					end
				else
					if public.x < public.coordinates[public.step + 1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step + 1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step + 1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step + 1].y then
						public.y = public.y - public.animationSpeed
					end

					local xDiff = math.abs(public.coordinates[public.step].x - public.coordinates[public.step + 1].x)
					local yDiff = math.abs(public.coordinates[public.step].y - public.coordinates[public.step + 1].y)
					local rDiff = math.abs(public.coordinates[public.step].r - public.coordinates[public.step + 1].r)
					local xCurrentDiff = math.abs(public.x - public.coordinates[public.step].x)
					local yCurrentDiff = math.abs(public.y - public.coordinates[public.step].y)

					if rDiff > 0 then
						if xDiff > yDiff then
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (xCurrentDiff * rDiff) / xDiff + public.coordinates[public.step].r
							else
								public.r = -((xCurrentDiff * rDiff) / xDiff - public.coordinates[public.step].r)
							end
						else
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (yCurrentDiff * rDiff) / yDiff + public.coordinates[public.step].r
							else
								public.r = -((yCurrentDiff * rDiff) / yDiff  - public.coordinates[public.step].r)
							end
						end
					else
						public.r = public.coordinates[public.step + 1].r
					end
				end
			elseif public.movementDirection == "backward" then
				if (math.abs(public.x - public.coordinates[public.step].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step].y) < public.animationSpeed) then
					if public.step > 1 then
						public.step = public.step - 1
					else
						public.movementDirection = "forward"
					end
				else
					if public.x < public.coordinates[public.step].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step].y then
						public.y = public.y - public.animationSpeed
					end

					local xDiff = math.abs(public.coordinates[public.step].x - public.coordinates[public.step + 1].x)
					local yDiff = math.abs(public.coordinates[public.step].y - public.coordinates[public.step + 1].y)
					local rDiff = math.abs(public.coordinates[public.step].r - public.coordinates[public.step + 1].r)
					local xCurrentDiff = math.abs(public.x - public.coordinates[public.step].x)
					local yCurrentDiff = math.abs(public.y - public.coordinates[public.step].y)

					if rDiff > 0 then
						if xDiff > yDiff then
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (xCurrentDiff * rDiff) / xDiff + public.coordinates[public.step].r
							else
								public.r = -((xCurrentDiff * rDiff) / xDiff - public.coordinates[public.step].r)
							end
						else
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (yCurrentDiff * rDiff) / yDiff + public.coordinates[public.step].r
							else
								public.r = -((yCurrentDiff * rDiff) / yDiff - public.coordinates[public.step].r)
							end
						end
					else
						public.r = public.coordinates[public.step + 1].r
					end
				end
			end
		elseif public.movementType == "ring" then
			if public.step < #public.coordinates then
				if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
					public.step = public.step + 1
				else
					if public.x < public.coordinates[public.step + 1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[public.step + 1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[public.step + 1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[public.step + 1].y then
						public.y = public.y - public.animationSpeed
					end

					local xDiff = math.abs(public.coordinates[public.step].x - public.coordinates[public.step + 1].x)
					local yDiff = math.abs(public.coordinates[public.step].y - public.coordinates[public.step + 1].y)
					local rDiff = math.abs(public.coordinates[public.step].r - public.coordinates[public.step + 1].r)
					local xCurrentDiff = math.abs(public.x - public.coordinates[public.step].x)
					local yCurrentDiff = math.abs(public.y - public.coordinates[public.step].y)

					if rDiff > 0 then
						if xDiff > yDiff then
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (xCurrentDiff * rDiff) / xDiff + public.coordinates[public.step].r
							else
								public.r = -((xCurrentDiff * rDiff) / xDiff - public.coordinates[public.step].r)
							end
						else
							if public.coordinates[public.step + 1].r > public.coordinates[public.step].r then
								public.r = (yCurrentDiff * rDiff) / yDiff + public.coordinates[public.step].r
							else
								public.r = -((yCurrentDiff * rDiff) / yDiff - public.coordinates[public.step].r)
							end
						end
					else
						public.r = public.coordinates[public.step + 1].r
					end
				end
			else
				if (math.abs(public.x - public.coordinates[1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[1].y) < public.animationSpeed) then
					public.step = 1
				else
					if public.x < public.coordinates[1].x then
						public.x = public.x + public.animationSpeed
					elseif public.x > public.coordinates[1].x then
						public.x = public.x - public.animationSpeed
					end

					if public.y < public.coordinates[1].y then
						public.y = public.y + public.animationSpeed
					elseif public.y > public.coordinates[1].y then
						public.y = public.y - public.animationSpeed
					end

					local xDiff = math.abs(public.coordinates[public.step].x - public.coordinates[1].x)
					local yDiff = math.abs(public.coordinates[public.step].y - public.coordinates[1].y)
					local rDiff = math.abs(public.coordinates[public.step].r - public.coordinates[1].r)
					local xCurrentDiff = math.abs(public.x - public.coordinates[public.step].x)
					local yCurrentDiff = math.abs(public.y - public.coordinates[public.step].y)

					if rDiff > 0 then
						if xDiff > yDiff then
							if public.coordinates[1].r > public.coordinates[public.step].r then
								public.r = (xCurrentDiff * rDiff) / xDiff + public.coordinates[public.step].r
							else
								public.r = - ((xCurrentDiff * rDiff) / xDiff - public.coordinates[public.step].r)
							end
						else
							if public.coordinates[1].r > public.coordinates[public.step].r then
								public.r = (yCurrentDiff * rDiff) / yDiff + public.coordinates[public.step].r
							else
								public.r = - ((yCurrentDiff * rDiff) / yDiff - public.coordinates[public.step].r)
							end
						end
					else
						public.r = public.coordinates[1].r
					end
				end
			end
		end

		if public.state == "standby" then
			if game.gameScreen.state == "waiting" then
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					public.state = "collision"
					game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
					game.hud.webs:createNext(game.gameScreen.collisionCount)
					public.ringColor[4] = 1
				end
			end

			if public.ringColor[4] > 0 then
				public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
			else
				public.ringColor[4] = 0
			end

			if public.collisionExpand < 15 then
				public.collisionExpand = public.collisionExpand + public.expandSpeed
			else
				public.collisionExpand = 15
			end

		elseif public.state == "collision" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
				game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
			else
				public.state = "standby"
			end

			if public.collisionExpand > 3 then
				public.collisionExpand = public.collisionExpand - public.expandSpeed
			else
				public.collisionExpand = 3
			end

		end

	end

	function public:draw(layer)
		if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
			if layer == 2 then
				local circle = {}
				for i = 1, 96 do
					local cosAngle = math.cos(math.rad((i - 1) * 3.75))
					local sinAngle = math.sin(math.rad((i - 1) * 3.75))
					local move = math.random()/public.collisionExpand*3 + 0.6
					local dotX = public.x + (public.r*move)*cosAngle
					local dotY = public.y - (public.r*move)*sinAngle
					table.insert(circle, dotX)
					table.insert(circle, dotY)
				end

				circles = {}
				if (math.random() > 0.99) then
					love.graphics.setColor(math.random(), math.random(), math.random(), 1)
				else
					love.graphics.setColor(darkColdGrey)
				end
				circles[1] = shuffleArray(circle)
				love.graphics.polygon('line', circles[1])

				love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
				love.graphics.polygon('line', circles[1])
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



SizeCircleDependent = {}
function SizeCircleDependent:new(newX, newY, newMinR, newMaxR, newRadius, newMode)
	local public = {}
		public.state = "standby"
		public.mode = newMode
		public.sizeState = "up"
		public.sizeAnimationSpeed = 1
		public.x = newX
		public.y = newY
		public.minR = newMinR
		public.maxR = newMaxR
		public.r = newRadius
		public.distance = public.r*2
		public.blendColor = black50
		public.blendShift = 10
		public.collisionExpand = 10
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.03

	function public:update()

		if (public.mode == "stop" and game.gameScreen.player.state == "stop")
		or (public.mode == "pause" and game.gameScreen.player.state == "pause") then
		
			public.distance = public.r*2
			if public.sizeState == "up" then
				if public.r < public.maxR then
					public.r = public.r + public.sizeAnimationSpeed
				else
					public.r = public.maxR
					public.sizeState = "down"
				end
			elseif public.sizeState == "down" then
				if public.r > public.minR then
					public.r = public.r - public.sizeAnimationSpeed
				else
					public.r = public.minR
					public.sizeState = "up"
				end
			end

		end

		if public.state == "standby" then
			if game.gameScreen.state == "waiting" then
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					public.state = "collision"
					game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
					game.hud.webs:createNext(game.gameScreen.collisionCount)
					public.ringColor[4] = 1
				end
			end

			if public.ringColor[4] > 0 then
				public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
			else
				public.ringColor[4] = 0
			end

			if public.collisionExpand < 15 then
				public.collisionExpand = public.collisionExpand + public.expandSpeed
			else
				public.collisionExpand = 15
			end

		elseif public.state == "collision" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
				-- no code
			else
				public.state = "standby"
			end

			if public.collisionExpand > 3 then
				public.collisionExpand = public.collisionExpand - public.expandSpeed
			else
				public.collisionExpand = 3
			end

		end

		if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
			public.state = "collision"
			game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
		else
			public.state = "standby"
		end
	end

	function public:draw(layer)
		if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
			if layer == 2 then
				local circle = {}
				for i = 1, 96 do
					local cosAngle = math.cos(math.rad((i - 1) * 3.75))
					local sinAngle = math.sin(math.rad((i - 1) * 3.75))
					local move = math.random()/public.collisionExpand*3 + 0.6
					local dotX = public.x + (public.r*move)*cosAngle
					local dotY = public.y - (public.r*move)*sinAngle
					table.insert(circle, dotX)
					table.insert(circle, dotY)
				end

				circles = {}
				if (math.random() > 0.99) then
					love.graphics.setColor(math.random(), math.random(), math.random(), 1)
				else
					love.graphics.setColor(darkColdGrey)
				end
				circles[1] = shuffleArray(circle)
				love.graphics.polygon('line', circles[1])

				love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
				love.graphics.polygon('line', circles[1])
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



MoveCircleDependent = {}
function MoveCircleDependent:new(newCoordinates, newRadius, newSpeed, newMode, newMovementType)
	local public = {}
		public.state = "standby"
		public.mode = newMode
		public.animationSpeed = newSpeed
		public.coordinates = {}
		for i = 1, #newCoordinates do
			public.coordinates[i] = {}
			public.coordinates[i].x = newCoordinates[i][1]
			public.coordinates[i].y = newCoordinates[i][2]
		end
		
		if newMovementType == "pong" then
			public.movementType = "pong"
			public.movementDirection = "forward"
		else
			public.movementType = "ring"
		end

		public.step = 1

		public.x = public.coordinates[1].x
		public.y = public.coordinates[1].y
		public.r = newRadius

		public.distance = public.r*2
		
		public.blendColor = black50
		public.blendShift = 10
		public.collisionExpand = 10
		public.expandSpeed = 0.5
		public.ringColor = {}
		public.ringColor[1] = lightRed[1]
		public.ringColor[2] = lightRed[2]
		public.ringColor[3] = lightRed[3]
		public.ringColor[4] = 0
		public.ringColorAnimationSpeed = 0.03

	function public:update()

		if (public.mode == "stop" and game.gameScreen.player.state == "stop")
		or (public.mode == "pause" and game.gameScreen.player.state == "pause") then
			public.distance = public.r*2
			if public.movementType == "pong" then
				if public.movementDirection == "forward" then
					if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
						if public.step < #public.coordinates - 1 then
							public.step = public.step + 1
						else
							public.movementDirection = "backward"
						end
					else
						if public.x < public.coordinates[public.step + 1].x then
							public.x = public.x + public.animationSpeed
						elseif public.x > public.coordinates[public.step + 1].x then
							public.x = public.x - public.animationSpeed
						end

						if public.y < public.coordinates[public.step + 1].y then
							public.y = public.y + public.animationSpeed
						elseif public.y > public.coordinates[public.step + 1].y then
							public.y = public.y - public.animationSpeed
						end
					end
				elseif public.movementDirection == "backward" then
					if (math.abs(public.x - public.coordinates[public.step].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step].y) < public.animationSpeed) then
						if public.step > 1 then
							public.step = public.step - 1
						else
							public.movementDirection = "forward"
						end
					else
						if public.x < public.coordinates[public.step].x then
							public.x = public.x + public.animationSpeed
						elseif public.x > public.coordinates[public.step].x then
							public.x = public.x - public.animationSpeed
						end

						if public.y < public.coordinates[public.step].y then
							public.y = public.y + public.animationSpeed
						elseif public.y > public.coordinates[public.step].y then
							public.y = public.y - public.animationSpeed
						end
					end
				end
			elseif public.movementType == "ring" then
				if public.step < #public.coordinates then
					if (math.abs(public.x - public.coordinates[public.step + 1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[public.step + 1].y) < public.animationSpeed) then
						public.step = public.step + 1
					else
						if public.x < public.coordinates[public.step + 1].x then
							public.x = public.x + public.animationSpeed
						elseif public.x > public.coordinates[public.step + 1].x then
							public.x = public.x - public.animationSpeed
						end

						if public.y < public.coordinates[public.step + 1].y then
							public.y = public.y + public.animationSpeed
						elseif public.y > public.coordinates[public.step + 1].y then
							public.y = public.y - public.animationSpeed
						end
					end
				else
					if (math.abs(public.x - public.coordinates[1].x) < public.animationSpeed and math.abs(public.y - public.coordinates[1].y) < public.animationSpeed) then
						public.step = 1
					else
						if public.x < public.coordinates[1].x then
							public.x = public.x + public.animationSpeed
						elseif public.x > public.coordinates[1].x then
							public.x = public.x - public.animationSpeed
						end

						if public.y < public.coordinates[1].y then
							public.y = public.y + public.animationSpeed
						elseif public.y > public.coordinates[1].y then
							public.y = public.y - public.animationSpeed
						end
					end
				end
			end

		end

		if public.state == "standby" then
			if game.gameScreen.state == "waiting" then
				if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
					public.state = "collision"
					game.gameScreen.collisionCount = game.gameScreen.collisionCount + 1
					game.hud.webs:createNext(game.gameScreen.collisionCount)
					public.ringColor[4] = 1
				end
			end

			if public.ringColor[4] > 0 then
				public.ringColor[4] = public.ringColor[4] - public.ringColorAnimationSpeed
			else
				public.ringColor[4] = 0
			end

			if public.collisionExpand < 15 then
				public.collisionExpand = public.collisionExpand + public.expandSpeed
			else
				public.collisionExpand = 15
			end

		elseif public.state == "collision" then
			if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
				-- no code
			else
				public.state = "standby"
			end

			if public.collisionExpand > 3 then
				public.collisionExpand = public.collisionExpand - public.expandSpeed
			else
				public.collisionExpand = 3
			end

		end

		if math.sqrt( (public.x-game.gameScreen.player.x)^2 + (public.y-game.gameScreen.player.y)^2 ) < public.r+game.gameScreen.player.collisionRadius then
			public.state = "collision"
			game.gameScreen.camera:move(math.random()*10 - 5, math.random()*10 - 5)
		else
			public.state = "standby"
		end
	end

	function public:draw(layer)
		if math.abs(public.x - game.gameScreen.player.x) < game.ww and math.abs(public.y - game.gameScreen.player.y) < game.wh then
			if layer == 2 then
				local circle = {}
				for i = 1, 96 do
					local cosAngle = math.cos(math.rad((i - 1) * 3.75))
					local sinAngle = math.sin(math.rad((i - 1) * 3.75))
					local move = math.random()/public.collisionExpand*3 + 0.6
					local dotX = public.x + (public.r*move)*cosAngle
					local dotY = public.y - (public.r*move)*sinAngle
					table.insert(circle, dotX)
					table.insert(circle, dotY)
				end

				circles = {}
				if (math.random() > 0.99) then
					love.graphics.setColor(math.random(), math.random(), math.random(), 1)
				else
					love.graphics.setColor(darkColdGrey)
				end
				circles[1] = shuffleArray(circle)
				love.graphics.polygon('line', circles[1])

				love.graphics.setColor(math.random(), math.random(), math.random(), public.ringColor[4])
				love.graphics.polygon('line', circles[1])
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Text = {}
function Text:new(newText, triggerX, triggerY, newShape)
	local public = {}
		public.state = "hidden"
		public.shape = newShape

		public.color = {}
		public.color[1] = white[1]
		public.color[2] = white[2]
		public.color[3] = white[3]
		public.color[4] = 0

		public.toolTipColor = {}
		public.toolTipColorHighlight = {}
		if public.shape == "circle" then
			public.toolTipColor[1] = darkGreen[1]
			public.toolTipColor[2] = darkGreen[2]
			public.toolTipColor[3] = darkGreen[3]
			public.toolTipColorHighlight[1] = green[1]
			public.toolTipColorHighlight[2] = green[2]
			public.toolTipColorHighlight[3] = green[3]
		elseif public.shape == "triangle" then
			public.toolTipColor[1] = darkColdGrey[1]
			public.toolTipColor[2] = darkColdGrey[2]
			public.toolTipColor[3] = darkColdGrey[3]
			public.toolTipColorHighlight[1] = coldGrey[1]
			public.toolTipColorHighlight[2] = coldGrey[2]
			public.toolTipColorHighlight[3] = coldGrey[3]
		elseif public.shape == "square" then
			public.toolTipColor[1] = darkBlue[1]
			public.toolTipColor[2] = darkBlue[2]
			public.toolTipColor[3] = darkBlue[3]
			public.toolTipColorHighlight[1] = blue[1]
			public.toolTipColorHighlight[2] = blue[2]
			public.toolTipColorHighlight[3] = blue[3]
		elseif public.shape == "brilliant" then
			public.toolTipColor[1] = darkYellow[1]
			public.toolTipColor[2] = darkYellow[2]
			public.toolTipColor[3] = darkYellow[3]
			public.toolTipColorHighlight[1] = yellow[1]
			public.toolTipColorHighlight[2] = yellow[2]
			public.toolTipColorHighlight[3] = yellow[3]
		elseif public.shape == "heart" then
			public.toolTipColor[1] = darkRed[1]
			public.toolTipColor[2] = darkRed[2]
			public.toolTipColor[3] = darkRed[3]
			public.toolTipColorHighlight[1] = red[1]
			public.toolTipColorHighlight[2] = red[2]
			public.toolTipColorHighlight[3] = red[3]
		else
			public.glitch = true
		end
		public.toolTipColor[4] = 0

		if not public.glitch then
			public.text = "\n"..newText
		else
			public.text = newText
		end
		public.displayText = ''
		public.displayTextNumber = 0
		public.talkingPause = true
		public.font = gameFont
		public.triggerX = triggerX
		public.triggerY = triggerY
		public.marginX = 20
		public.marginY = 10
		public.toolTipW = public.font:getWidth(public.text) + public.marginX*2
		public.toolTipH = public.font:getHeight(public.text)*(countN(public.text)+1) + public.marginY*2
		public.toolTipX = 0
		public.toolTipY = 0
		public.constX = public.triggerX - public.toolTipW/2 + public.marginX*2
		public.constY = public.triggerY - public.toolTipH + public.marginY*countN(public.text) - public.marginY
		public.x = public.constX
		public.y = public.constY
		public.ringR = 0

		public.triggerColor = white
		public.slideShift = 200

		public.triggerSize = 20
		public.triangleSize = public.triggerSize*0.5
		public.distance = 40
		public.colorAnimationSpeed = 0.1
		public.velocityX = 0.1
		public.velocityY = 0.1
		public.blendColor = black50
		public.blendShift = 10

		public.ringR = 0
		public.maxRingR = 120
		public.ringColor = {}
		public.ringColor[1] = white[1]
		public.ringColor[2] = white[2]
		public.ringColor[3] = white[3]
		public.ringColor[4] = 1
		public.ringState = "waiting"
		public.animationSpeed = 0.5

		public.shineState = "up"
		public.shineColor = {1, 1, 1, 0}
		public.shineMax = 0.25
		public.shineSpeed = 0.005

	function public:update()
		if math.sqrt( (public.triggerX-game.gameScreen.player.x)^2 + (public.triggerY-game.gameScreen.player.y)^2 ) < public.triggerSize+game.gameScreen.player.extendedCollisionRadius then
			if public.state == "hidden" then
				if public.ringState == "waiting" then
					public.ringState = "shown"
					-- game.sfxEngine:play("text")
				end
			end
			public.state = "toshown"
		else
			public.state = "fromshown"
		end

		if public.state == "hidden" then
			-- no code
		
		else
			local cx, cy = game.gameScreen.camera:position()

			if cx < public.constX then
				public.x = public.constX + math.abs(cx - public.constX) * public.velocityX
			else
				public.x = public.constX - math.abs(cx - public.constX) * public.velocityX
			end
			
			if cy < public.constY then
				public.y = public.constY + math.abs(cy - public.constY) * public.velocityY
			else
				public.y = public.constY - math.abs(cy - public.constY) * public.velocityY
			end

			public.toolTipX = public.x - public.marginX
			public.toolTipY = public.y - public.marginY

			if public.state == "shown" then
				--- no code

			elseif public.state == "toshown" then
				if public.color[4] < 1 then
					public.color[4] = public.color[4] + public.colorAnimationSpeed
				else
					public.color[4] = 1
					public.state = "shown"
				end
				public.toolTipColor[4] = public.color[4] * 0.5
				public.toolTipColorHighlight[4] = public.color[4] * 0.5
			elseif public.state == "fromshown" then
				if public.color[4] > 0 then
					public.color[4] = public.color[4] - public.colorAnimationSpeed
				else
					public.color[4] = 0
					public.state = "hidden"
				end
				public.toolTipColor[4] = public.color[4] * 0.5
				public.toolTipColorHighlight[4] = public.color[4] * 0.5
			end

		end

		if public.ringState == "shown" then
			if public.ringColor[4] > 0 then
				public.ringR = public.ringR + public.animationSpeed * 4
				public.ringColor[4] = 1 - public.ringR/public.maxRingR - 0.25
			else
				public.ringR = 0
				public.ringColor[4] = 1
				public.ringState = "hidden"
			end
		end

		if public.ringState ~= "hidden" then
			if public.shineState == "up" then
				if public.shineColor[4] < public.shineMax then
					public.shineColor[4] = public.shineColor[4] + public.shineSpeed
				else
					public.shineColor[4] = public.shineMax
					public.shineState = "down"
				end
			elseif public.shineState == "down" then
				if public.shineColor[4] > 0 then
					public.shineColor[4] = public.shineColor[4] - public.shineSpeed
				else
					public.shineColor[4] = 0
					public.shineState = "up"
				end
			end
		else
			if public.shineColor[4] > 0 then
				public.shineColor[4] = public.shineColor[4] - public.shineSpeed
			else
				public.shineColor[4] = 0
			end
		end

		if public.state ~= "hidden" then
			if public.talkingPause == false then
				if public.displayTextNumber <= utf8.len(public.text) then
					public.displayTextNumber = public.displayTextNumber + 1
					public.displayText = public.displayText..utf8sub(public.text, public.displayTextNumber, public.displayTextNumber)
					game.sfxEngine:play("talking", public.shape)
					local r = math.random()
					if r > 0.5 then
						public.talkingPause = true
					else
						public.talkingPause = false
					end
				end
			else
				public.talkingPause = false
			end
		else
			public.displayText = ''
			public.displayTextNumber = 0
			public.talkingPause = true
		end

	end

	function public:draw(layer)
		if layer == -1 then
			love.graphics.setColor(game.gameScreen.mainColor)
			love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift + 5, public.triggerSize * 1.4, 64)
			love.graphics.setColor(game.background.color)
			love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift + 5, public.triggerSize * 1.2, 64)
			love.graphics.setColor(game.gameScreen.gameShining.color)
			love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift + 5, public.triggerSize * 1.2, 64)


			love.graphics.setColor(black10)
			for i = public.triggerSize - 6, public.triggerSize, 2 do
				love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift, i)
			end

			if public.ringState == "shown" then
				love.graphics.setColor(public.ringColor)
				love.graphics.circle("line", public.triggerX, public.triggerY + public.blendShift, public.ringR, 64)
			end

			for i = 1, 5 do
				love.graphics.setColor(game.gameScreen.mainColor[1] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[2] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[3] * (1+i*0.05 - 0.25), game.gameScreen.mainColor[4])
				love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift * public.color[4] + 5 - i, public.triggerSize, 64)
			end
			love.graphics.setColor(game.gameScreen.lightColor)
			love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift * public.color[4] - 5, public.triggerSize, 64)
			if public.glitch ~= true then
				love.graphics.setColor(public.toolTipColorHighlight[1], public.toolTipColorHighlight[2], public.toolTipColorHighlight[3], 1 - public.toolTipColor[4])
				love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift * public.color[4] - 5, public.triggerSize*0.25, 64)
			end
			love.graphics.setColor(public.shineColor)
			love.graphics.circle("fill", public.triggerX, public.triggerY + public.blendShift * public.color[4] - 5, public.triggerSize, 64)
		elseif layer == 2 then
			love.graphics.setColor(0, 0, 0, 0.05*public.toolTipColor[4])
			for i = 1, 15 do
				love.graphics.rectangle("fill",
					public.toolTipX - i,
					public.toolTipY - (1 - public.color[4]) * public.slideShift - i,
					public.toolTipW + i*2,
					public.toolTipH + i*2,
					30, 30)
			end

			if public.glitch then
				love.graphics.setColor(math.random()/2, math.random()/2, math.random()/2, public.toolTipColor[4])
			else
				love.graphics.setColor(public.toolTipColor)
			end
			love.graphics.rectangle("fill",
				public.toolTipX,
				public.toolTipY - (1 - public.color[4]) * public.slideShift - 5,
				public.toolTipW,
				public.toolTipH + 5,
				30, 30)

			love.graphics.polygon("fill",
				public.triggerX, public.toolTipY + public.toolTipH + 7 - (1 - public.color[4]) * public.slideShift,
				public.triggerX - 5, public.toolTipY + public.toolTipH - (1 - public.color[4]) * public.slideShift,
				public.triggerX + 5, public.toolTipY + public.toolTipH - (1 - public.color[4]) * public.slideShift)

			if public.glitch then
				love.graphics.setColor(math.random(), math.random(), math.random(), public.toolTipColorHighlight[4])
			else
				love.graphics.setColor(public.toolTipColorHighlight)
			end
			love.graphics.rectangle("fill",
				public.toolTipX,
				public.toolTipY - (1 - public.color[4]) * public.slideShift - 5,
				public.toolTipW,
				public.toolTipH,
				30, 30)

			love.graphics.setColor(public.color)
			love.graphics.setFont(public.font)
			love.graphics.print(public.displayText, public.x, public.y - (1 - public.color[4]) * public.slideShift)

			-- shape
			love.graphics.setColor(public.color)

			if public.shape == "circle" then
				love.graphics.setColor(public.toolTipColor)
				love.graphics.circle("fill", public.toolTipX + 40, public.toolTipY + 40 + 5 - (1 - public.color[4]) * public.slideShift, 15)
				love.graphics.setColor(public.color)
				love.graphics.circle("fill", public.toolTipX + 40, public.toolTipY + 40 - (1 - public.color[4]) * public.slideShift, 15)
			elseif public.shape == "triangle" then
				love.graphics.setColor(public.toolTipColor)
				love.graphics.polygon("fill",
					public.toolTipX + 40, public.toolTipY + 25 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 55 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 55 + 5 - (1 - public.color[4]) * public.slideShift)
				love.graphics.setColor(public.color)
				love.graphics.polygon("fill",
					public.toolTipX + 40, public.toolTipY + 25 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 55 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 55 - (1 - public.color[4]) * public.slideShift)
			elseif public.shape == "square" then
				love.graphics.setColor(public.toolTipColor)
				love.graphics.rectangle("fill", public.toolTipX + 30, public.toolTipY + 30 + 5 - (1 - public.color[4]) * public.slideShift, 20, 20)
				love.graphics.setColor(public.color)
				love.graphics.rectangle("fill", public.toolTipX + 30, public.toolTipY + 30 - (1 - public.color[4]) * public.slideShift, 20, 20)
			elseif public.shape == "brilliant" then
				love.graphics.setColor(public.toolTipColor)
				love.graphics.polygon("fill",
					public.toolTipX + 32, public.toolTipY + 30 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 48, public.toolTipY + 30 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 37 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 40, public.toolTipY + 55 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 37 + 5 - (1 - public.color[4]) * public.slideShift)
				love.graphics.setColor(public.color)
				love.graphics.polygon("fill",
					public.toolTipX + 32, public.toolTipY + 30 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 48, public.toolTipY + 30 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 37 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 40, public.toolTipY + 55 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 37 - (1 - public.color[4]) * public.slideShift)
			elseif public.shape == "heart" then
				love.graphics.setColor(public.toolTipColor)
				love.graphics.polygon("fill",
					public.toolTipX + 40, public.toolTipY + 32 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 35 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 40, public.toolTipY + 55 + 5 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 35 + 5 - (1 - public.color[4]) * public.slideShift)
				love.graphics.circle("fill", public.toolTipX + 32.5, public.toolTipY + 32.5 + 5 - (1 - public.color[4]) * public.slideShift, 7.5)
				love.graphics.circle("fill", public.toolTipX + 47.5, public.toolTipY + 32.5 + 5 - (1 - public.color[4]) * public.slideShift, 7.5)
				love.graphics.setColor(public.color)
				love.graphics.polygon("fill",
					public.toolTipX + 40, public.toolTipY + 32 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 55, public.toolTipY + 35 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 40, public.toolTipY + 55 - (1 - public.color[4]) * public.slideShift,
					public.toolTipX + 25, public.toolTipY + 35 - (1 - public.color[4]) * public.slideShift)
				love.graphics.circle("fill", public.toolTipX + 32.5, public.toolTipY + 32.5 - (1 - public.color[4]) * public.slideShift, 7.5)
				love.graphics.circle("fill", public.toolTipX + 47.5, public.toolTipY + 32.5 - (1 - public.color[4]) * public.slideShift, 7.5)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end
