TitleTriangle = {}
function TitleTriangle:new(newX, newY)
	local public = {}
		public.state = "standby"
		public.x = 0
		public.y = 0
		public.animationSpeed = 10
		public.size = 40
		public.pulses = {}
		for i = 1, 6 do
			public.pulses[i] = {}
			public.pulses[i].coef = i
		end
		public.pulsesSpeed = 0.005

	function public:update()
		public.animationSpeed = game.wh/60
		public.size = game.wh/15

		if public.state == "standby" then
			public.x = game.ww/2
			public.y = game.wh/2

			for i = 1, #public.pulses do
				if public.pulses[i].coef > 0 then
					public.pulses[i].coef = public.pulses[i].coef - public.pulsesSpeed
				else
					public.pulses[i].coef = #public.pulses
				end
			end

		elseif public.state == "down" then
			public.x = game.ww/2
			public.y = game.wh

		elseif public.state == "todown" then
			if public.y < game.wh then
				public.y = public.y + public.animationSpeed
			else
				public.state = "down"
				game.state = "levels"
			end
		elseif public.state == "toup" then
			if public.y > game.wh/2 then
				public.y = public.y - public.animationSpeed
			else
				public.state = "standby"
				game.state = "title"
			end
		end
	end

	function public:draw()
		local pulsesOpacity = (1 - (public.y / game.wh)) * 2
		local mainColor, lightColor = getColorsFromLevel(game.levelsScreen.maxOpenedLevel)
		if game.state ~= "game" then
			for i = 1, #public.pulses do
				local coef = public.pulses[i].coef * 0.5
				love.graphics.setColor(lightColor[1], lightColor[2], lightColor[3], (1 - public.pulses[i].coef/#public.pulses) * 0.5 * pulsesOpacity)
				love.graphics.circle("fill", public.x, public.y, public.size * coef)
			end
			
			if game.levelsScreen.maxOpenedLevel < 51 then
				love.graphics.setColor(white)
			else
				love.graphics.setColor(darkColdGrey)
			end
			love.graphics.polygon("fill", public.x, public.y-public.size, public.x-public.size, public.y+public.size, public.x+public.size, public.y+public.size)

			love.graphics.setColor(white[1], white[2], white[3], pulsesOpacity)
			love.graphics.setFont(titleFont)
			love.graphics.print(gameName, game.ww/2 - titleFont:getWidth(gameName)/2, public.y + public.size*2.5 * game.wh/2/public.y)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



MenuButton = {}
function MenuButton:new(newX, newY)
	local public = {}
		public.state = "down"
		public.const = 40
		public.x = public.const
		public.y = public.const
		public.size = 10
		public.animationSpeed = 4
		public.blendColor = black50
		public.blendShift = 5

	function public:update()
		public.animationSpeed = game.wh/150
		public.const = game.wh/15
		public.size = game.wh/60
		public.blendShift = game.wh/120

		if public.state == "down" then
			public.x = public.const
			public.y = public.const

		elseif public.state == "up" then
			public.x = public.const
			public.y = public.const

		elseif public.state == "toupIN" then
			if public.x > -public.size then
				public.x = public.x - public.animationSpeed
			else
				public.state = "toupOUT"
			end

		elseif public.state == "toupOUT" then
			if public.x < public.const then
				public.x = public.x + public.animationSpeed
			else
				public.state = "up"
			end

		elseif public.state == "todownIN" then
			if public.x > -public.size then
				public.x = public.x - public.animationSpeed
			else
				public.state = "todownOUT"
			end

		elseif public.state == "todownOUT" then
			if public.x < public.const then
				public.x = public.x + public.animationSpeed
			else
				public.state = "down"
			end

		elseif public.state == "todropdown" then
			if public.y < public.const+game.wh/6 then
				public.y = public.y + public.animationSpeed
			else
				public.state = "dropdown"
			end

		elseif public.state == "fromdropdown" then
			if public.y > public.const then
				public.y = public.y - public.animationSpeed
			else
				public.state = "down"
			end

		elseif public.state == "hideandup" then
			if public.x > -public.size then
				public.x = public.x - public.animationSpeed
			else
				public.y = public.const
				public.state = "toupOUT"
			end

		elseif public.state == "dropdown" then
			public.x = public.const
			public.y = public.const+game.wh/6
		end
	end

	function public:draw()
		if public.state == "down" or public.state == "toupIN" or public.state == "todownOUT" or public.state == "hideandup" then
			love.graphics.setColor(black25)
			for i = 1, public.blendShift do
				love.graphics.polygon("fill",
					public.x, public.y + public.size*i/public.blendShift + public.blendShift,
					public.x - public.size*i/public.blendShift, public.y - public.size*i/public.blendShift + public.blendShift,
					public.x + public.size*i/public.blendShift, public.y - public.size*i/public.blendShift + public.blendShift)
			end
			love.graphics.setColor(white)
			love.graphics.polygon("fill", public.x, public.y+public.size, public.x-public.size, public.y-public.size, public.x+public.size, public.y-public.size)
		elseif public.state == "up" or public.state == "toupOUT" or public.state == "todownIN" then
			love.graphics.setColor(black25)
			for i = 1, public.blendShift do
				love.graphics.polygon("fill",
					public.x, public.y - public.size*i/public.blendShift + public.blendShift,
					public.x - public.size, public.y + public.size*i/public.blendShift + public.blendShift,
					public.x + public.size, public.y + public.size*i/public.blendShift + public.blendShift)
			end
			love.graphics.setColor(white)
			love.graphics.polygon("fill", public.x, public.y-public.size, public.x-public.size, public.y+public.size, public.x+public.size, public.y+public.size)
		elseif public.state == "dropdown" or public.state == "todropdown" or public.state == "fromdropdown" then
			love.graphics.setColor(black25)
			for i = 1, public.blendShift do
				love.graphics.polygon("fill",
					public.x, public.y + public.size*i/public.blendShift + public.blendShift*2,
					public.x - public.size*i/public.blendShift, public.y - public.size*i/public.blendShift + public.blendShift*2,
					public.x + public.size*i/public.blendShift, public.y - public.size*i/public.blendShift + public.blendShift*2)
			end
			love.graphics.setColor(white)
			love.graphics.polygon("fill", public.x, public.y+public.size+public.blendShift, public.x-public.size, public.y-public.size+public.blendShift, public.x+public.size, public.y-public.size+public.blendShift)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Dropdown = {}
function Dropdown:new(newX, newY)
	local public = {}
		public.state = "hidden"
		public.x = 0
		public.y = 0
		public.w = 0
		public.h = 0
		public.sizeRate = 5
		public.animationSpeed = 4
		public.blendColor = black50
		public.blendShift = 10

	function public:update()
		public.w = game.ww
		public.h = game.wh/6
		public.animationSpeed = game.wh/150

		if public.state == "hidden" then
			public.y = -public.h

		elseif public.state == "toshown" then
			if public.y < 0 then
				public.y = public.y + public.animationSpeed
			else
				public.state = "shown"
			end

		elseif public.state == "fromshown" then
			if public.y + public.h > 0 then
				public.y = public.y - public.animationSpeed
			else
				public.state = "hidden"
			end

		elseif public.state == "shown" then
			-- no code
		end
	end

	function public:draw()
		if public.state == "toshown" or public.state == "fromshown" then
			love.graphics.setColor(black10)
			for i = 1, public.blendShift do
				love.graphics.rectangle("fill", public.x, public.y + i, public.w, public.h)
			end
			love.graphics.setColor(red[1], red[2], red[3], 0.5)
			love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			love.graphics.setColor(lightRed[1], lightRed[2], lightRed[3], 0.5)
			love.graphics.rectangle("fill", public.x, public.y - game.wh/120, public.w, public.h)
		elseif public.state == "shown" then
			love.graphics.setColor(black10)
			for i = 1, public.blendShift do
				love.graphics.rectangle("fill", public.x, public.y + i, public.w, public.h)
			end
			love.graphics.setColor(red[1], red[2], red[3], 0.5)
			love.graphics.rectangle("fill", public.x, public.y, public.w, public.h)
			love.graphics.setColor(lightRed[1], lightRed[2], lightRed[3], 0.5)
			love.graphics.rectangle("fill", public.x, public.y - game.wh/120, public.w, public.h)
			if game.state == "title" then
				love.graphics.setColor(black10)
				for i = 1, public.blendShift do
					love.graphics.circle("fill", game.ww/2, public.y + public.h/2 + i, public.h/public.sizeRate*game.titleScreen.quitingTimer/80, 4)
				end
				love.graphics.setColor(yellow)
				love.graphics.circle("line", game.ww/2, public.y + public.h/2, public.h/public.sizeRate, 4)
				love.graphics.circle("fill", game.ww/2, public.y + public.h/2, public.h/public.sizeRate*game.titleScreen.quitingTimer/80, 4)
				love.graphics.polygon("fill",
					game.ww/2 - (public.h/public.sizeRate*game.titleScreen.quitingTimer/80), public.y + public.h/2,
					game.ww/2 + (public.h/public.sizeRate*game.titleScreen.quitingTimer/80), public.y + public.h/2,
					game.ww/2 + (public.h/public.sizeRate*game.titleScreen.quitingTimer/80), public.y + public.h/2 - 5,
					game.ww/2 - (public.h/public.sizeRate*game.titleScreen.quitingTimer/80), public.y + public.h/2 - 5)
				love.graphics.setColor(lightYellow)
				love.graphics.circle("fill", game.ww/2, public.y + public.h/2 - 5, public.h/public.sizeRate*game.titleScreen.quitingTimer/80, 4)
			elseif game.state == "game" then
				love.graphics.setColor(black10)
				for i = 1, public.blendShift do
					love.graphics.circle("fill", game.ww/2, public.y + public.h/2 + i, public.h/public.sizeRate*game.gameScreen.quitingTimer/80, 4)
				end
				love.graphics.setColor(yellow)
				love.graphics.circle("line", game.ww/2, public.y + public.h/2, public.h/public.sizeRate, 4)
				love.graphics.circle("fill", game.ww/2, public.y + public.h/2, public.h/public.sizeRate*game.gameScreen.quitingTimer/80, 4)
				love.graphics.polygon("fill",
					game.ww/2 - (public.h/public.sizeRate*game.gameScreen.quitingTimer/80), public.y + public.h/2,
					game.ww/2 + (public.h/public.sizeRate*game.gameScreen.quitingTimer/80), public.y + public.h/2,
					game.ww/2 + (public.h/public.sizeRate*game.gameScreen.quitingTimer/80), public.y + public.h/2 - 5,
					game.ww/2 - (public.h/public.sizeRate*game.gameScreen.quitingTimer/80), public.y + public.h/2 - 5)
				love.graphics.setColor(lightYellow)
				love.graphics.circle("fill", game.ww/2, public.y + public.h/2 - 5, public.h/public.sizeRate*game.gameScreen.quitingTimer/80, 4)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Sparks = {}
function Sparks:new(newX, newY)
	local public = {}
		public.const = 40
		public.constSize = 15
		public.r = public.constSize
		public.x = love.graphics.getWidth() - public.const
		public.y = public.const
		public.blendColor = black50
		public.blendShift = 5
		public.progress = 0
		public.progressAcc = 0.05
		public.shining = false
		public.shineDirection = "up"
		public.shineColor = {}
		public.shineColor[1] = white[1]
		public.shineColor[2] = white[2]
		public.shineColor[3] = white[3]
		public.shineColor[4] = 0
		public.shineAnimationSpeed = 0.03

	function public:update()
		public.const = game.wh/15
		public.constSize = game.wh/40
		public.r = public.constSize
		public.x = love.graphics.getWidth() - public.const
		public.y = public.const

		if game.state == "game" then
			public.x = game.ww - public.const
			local sparksCoefficient = game.gameScreen.sparksCount/minimumSparksLevel[game.gameScreen.level]
			if public.progress < sparksCoefficient then
				public.progress = public.progress + public.progressAcc
			else
				public.progress = sparksCoefficient
			end

			if public.progress == 0 then
				public.shineDirection = "up"
				public.shineColor[4] = 0
				public.shining = false
			elseif public.progress == 1 then
				public.shining = true
			end

			if public.shining then
				if public.shineDirection == "up" then
					if public.shineColor[4] < 1 then
						public.shineColor[4] = public.shineColor[4] + public.shineAnimationSpeed
					else
						public.shineDirection = "down"
					end
				else
					if public.shineColor[4] > 0 then
						public.shineColor[4] = public.shineColor[4] - public.shineAnimationSpeed
					else
						public.shineDirection = "up"
					end
				end
			end
		end
	end

	function public:draw()
		if game.state == "game" then
			if public.shining then
				love.graphics.setColor(yellow)
				love.graphics.circle("fill", public.x, public.y, public.r + public.r/2 * (public.shineColor[4]), 4)
				love.graphics.polygon("fill",
					public.x - (public.r + public.r/2 * (public.shineColor[4])), public.y,
					public.x + (public.r + public.r/2 * (public.shineColor[4])), public.y,
					public.x + (public.r + public.r/2 * (public.shineColor[4])), public.y - 5,
					public.x - (public.r + public.r/2 * (public.shineColor[4])), public.y - 5)
				love.graphics.setColor(lightYellow)
				love.graphics.circle("fill", public.x, public.y - 5, public.r + public.r/2 * (public.shineColor[4]), 4)
				love.graphics.setColor(public.shineColor)
				love.graphics.circle("fill", public.x, public.y - 5, public.r + public.r/2 * (public.shineColor[4]), 4)
			else
				love.graphics.setColor(yellow)
				love.graphics.circle("line", public.x, public.y, public.r, 4)
				love.graphics.circle("fill", public.x, public.y, public.r * public.progress, 4)
				love.graphics.polygon("fill",
					public.x - (public.r * public.progress), public.y,
					public.x + (public.r * public.progress), public.y,
					public.x + (public.r * public.progress), public.y - 5*public.progress,
					public.x - (public.r * public.progress), public.y - 5*public.progress)
				love.graphics.setColor(lightYellow)
				love.graphics.circle("fill", public.x, public.y - 5*public.progress, public.r * public.progress, 4)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Arrows = {}
function Arrows:new()
	local public = {}
		public.showAnimation = 0.05
		public.hideAnimation = 0.08
		public.size = 20
		public.const = 100
		public.blendShift = 6

		public.left = {}
		public.left.color = {}
		public.left.color[1] = white[1]
		public.left.color[2] = white[2]
		public.left.color[3] = white[3]
		public.left.color[4] = 0
		public.left.blendColor = {}
		public.left.blendColor[1] = black[1]
		public.left.blendColor[2] = black[2]
		public.left.blendColor[3] = black[3]
		public.left.blendColor[4] = 0
		public.left.state = "hide"
		
		public.right = {}
		public.right.color = {}
		public.right.color[1] = white[1]
		public.right.color[2] = white[2]
		public.right.color[3] = white[3]
		public.right.color[4] = 0
		public.right.blendColor = {}
		public.right.blendColor[1] = black[1]
		public.right.blendColor[2] = black[2]
		public.right.blendColor[3] = black[3]
		public.right.blendColor[4] = 0
		public.right.state = "hide"

	function public:showLeft()
		public.left.state = "show"
	end

	function public:showRight()
		public.right.state = "show"
	end

	function public:hideLeft()
		public.left.state = "hide"
	end

	function public:hideRight()
		public.right.state = "hide"
	end

	function public:update()
		public.size = game.ww/64
		public.blendShift = game.wh/100

		if (game.levelsScreen.selectedLevel > -1 and game.levelsScreen.maxOpenedLevel > 1)
		or (game.levelsScreen.selectedLevel > 0) then
			public:showLeft()
		else
			public:hideLeft()
		end

		if game.levelsScreen.selectedLevel < game.levelsScreen.maxOpenedLevel
		or game.levelsScreen.selectedLevel == 60 then
			public:showRight()
		else
			public:hideRight()
		end

		if public.left.state == "hide" then
			if public.left.color[4] > 0 then
				public.left.color[4] = public.left.color[4] - public.hideAnimation
			else
				public.left.color[4] = 0
			end
		elseif public.left.state == "show" then
			if public.left.color[4] < 1 then
				public.left.color[4] = public.left.color[4] + public.showAnimation
			else
				public.left.color[4] = 1
			end
		end

		if public.right.state == "hide" then
			if public.right.color[4] > 0 then
				public.right.color[4] = public.right.color[4] - public.hideAnimation
			else
				public.right.color[4] = 0
			end
		elseif public.right.state == "show" then
			if public.right.color[4] < 1 then
				public.right.color[4] = public.right.color[4] + public.showAnimation
			else
				public.right.color[4] = 1
			end
		end

		public.left.blendColor[4] = public.left.color[4] / 2
		public.right.blendColor[4] = public.right.color[4] / 2
	end

	function public:draw()		
		love.graphics.setColor(0, 0, 0, public.left.blendColor[4]*0.5)
		for i = 1, public.blendShift, 2 do
			love.graphics.polygon("fill",
				public.const - public.size*i/public.blendShift, game.wh/2 + public.blendShift + (public.size*i/public.blendShift * 3 * (1 - public.left.color[4])),
				public.const + public.size*i/public.blendShift, game.wh/2 + public.blendShift - public.size*i/public.blendShift + (public.size*i/public.blendShift * 3 * (1 - public.left.color[4])),
				public.const + public.size*i/public.blendShift, game.wh/2 + public.blendShift + public.size*i/public.blendShift + (public.size*i/public.blendShift * 3 * (1 - public.left.color[4]))
			)
		end

		love.graphics.setColor(public.left.color)
		love.graphics.polygon("fill",
			public.const - public.size, game.wh/2 + (public.size * 3 * (1 - public.left.color[4])),
			public.const + public.size, game.wh/2 - public.size + (public.size * 3 * (1 - public.left.color[4])),
			public.const + public.size, game.wh/2 + public.size + (public.size * 3 * (1 - public.left.color[4]))
		)

		love.graphics.setColor(0, 0, 0, public.right.blendColor[4]*0.5)
		for i = 1, public.blendShift, 2 do
			love.graphics.polygon("fill",
				game.ww - public.const + public.size*i/public.blendShift, game.wh/2 + public.blendShift + (public.size*i/public.blendShift * (1 - public.right.color[4])),
				game.ww - public.const - public.size*i/public.blendShift, game.wh/2 + public.blendShift - public.size*i/public.blendShift + (public.size*i/public.blendShift * (1 - public.right.color[4])),
				game.ww - public.const - public.size*i/public.blendShift, game.wh/2 + public.blendShift + public.size*i/public.blendShift + (public.size*i/public.blendShift * (1 - public.right.color[4]))
			)
		end

		love.graphics.setColor(public.right.color)
		love.graphics.polygon("fill",
			game.ww - public.const + public.size, game.wh/2 + (public.size * (1 - public.right.color[4])),
			game.ww - public.const - public.size, game.wh/2 - public.size + (public.size * (1 - public.right.color[4])),
			game.ww - public.const - public.size, game.wh/2 + public.size + (public.size * (1 - public.right.color[4]))
		)
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Mask = {}
function Mask:new()
	local private = {}

	local public = {}
		public.state = "standby"
		public.color = {}
		public.color[1] = darkColdGrey[1]
		public.color[2] = darkColdGrey[2]
		public.color[3] = darkColdGrey[3]
		public.color[4] = 1
		public.animationSpeed = 0.01

	function public:update()
		if game.titleScreen.state == "quiting" then
			public.color[4] = 1-game.titleScreen.quitingTimer/100

		elseif game.gameScreen.state == "quiting" then
			public.color[4] = 1.2 - game.gameScreen.quitingTimer/100

		elseif public.state == "fadein" then
			if game.state == "game" and game.gameScreen.chapterPrologue.active == false then
				if public.color[4] > 0 then
					public.color[4] = public.color[4] - public.animationSpeed
				else
					public.state = "standby"
				end
			end

		elseif public.state == "fadeout" then
			if public.color[4] < 1 then
				public.color[4] = public.color[4] + public.animationSpeed*2
			else
				public.state = "standby"
			end

		elseif public.state == "standby" then
			public.color[4] = 0

		elseif public.state == "opening" then
			public.color[4] = game.openingScreen.screenOpacity

		elseif public.state == "fromopening" then
			public.color[4] = game.titleScreen.screenOpacity

		elseif public.state == "fromgame" then
			public.color[4] = game.levelsScreen.screenOpacity
			if public.color[4] > 1 then
				public.color[4] = 1
			elseif public.color[4] < 0 then
				public.color[4] = 0
			end

		end
	end

	function public:draw()
		if not (game.state == "game" and game.gameScreen.chapterPrologue.active) then
			love.graphics.setColor(public.color)
			love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Webs = {}
function Webs:new()
	local public = {}
	public.list = {
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0},
		{-100, -100, 0}
	}
	public.websRandom = 0
	public.maskOpacity = 1
	public.maskState = "standby"
	public.maskAnimarionSpeed = 0.01
	public.color = {}
	public.color[1] = darkColdGrey[1]
	public.color[2] = darkColdGrey[2]
	public.color[3] = darkColdGrey[3]
	public.color[4] = 0.75
	public.flashColor = {}
	public.flashColor[1] = red[1]
	public.flashColor[2] = red[2]
	public.flashColor[3] = red[3]
	public.flashColor[4] = 0
	public.flashColorAnimationSpeed = 0.02
	public.webOpacitySpeed = 0.005


	function public:twoWebs()
		public.websRandom = 3
		public.list[1][1] = math.random(0, game.ww)
		public.list[1][2] = math.random(0, game.ww)
		public.list[2][1] = math.random(0, game.ww)
		public.list[2][2] = math.random(0, game.ww)
	end

	function public:fourWebs()
		public.websRandom = 5
		public.list[3][1] = math.random(0, game.ww)
		public.list[3][2] = math.random(0, game.ww)
		public.list[4][1] = math.random(0, game.ww)
		public.list[4][2] = math.random(0, game.ww)
	end

	function public:eightWebs()
		public.websRandom = 7
		public.list[5][1] = math.random(0, game.ww)
		public.list[4][2] = math.random(0, game.ww)
		public.list[6][1] = math.random(0, game.ww)
		public.list[6][2] = math.random(0, game.ww)
		public.list[7][1] = math.random(0, game.ww)
		public.list[7][2] = math.random(0, game.ww)
		public.list[8][1] = math.random(0, game.ww)
		public.list[8][2] = math.random(0, game.ww)
	end

	function public:sixteenWebs()
		public.websRandom = 13
		public.list[9][1] = math.random(0, game.ww)
		public.list[9][2] = math.random(0, game.ww)
		public.list[10][1] = math.random(0, game.ww)
		public.list[10][2] = math.random(0, game.ww)
		public.list[11][1] = math.random(0, game.ww)
		public.list[11][2] = math.random(0, game.ww)
		public.list[12][1] = math.random(0, game.ww)
		public.list[12][2] = math.random(0, game.ww)
		public.list[13][1] = math.random(0, game.ww)
		public.list[13][2] = math.random(0, game.ww)
		public.list[14][1] = math.random(0, game.ww)
		public.list[14][2] = math.random(0, game.ww)
		public.list[15][1] = math.random(0, game.ww)
		public.list[15][2] = math.random(0, game.ww)
		public.list[16][1] = math.random(0, game.ww)
		public.list[16][2] = math.random(0, game.ww)
	end

	function public:createNext(currentCount)
		if not godMode then --remove
			if game.levelsScreen.selectedLevel < 11 then
				if currentCount == 1 then
					public:fourWebs()
					public.maskState = "to50"
				elseif currentCount == 2 then
					public:eightWebs()
					public.maskState = "to25"
				elseif currentCount == 3 then
					public:sixteenWebs()
					public.maskState = "to12"
				elseif currentCount == 4 then
					public.maskState = "to00"
				end
			elseif game.levelsScreen.selectedLevel < 21 then
				if currentCount == 1 then
					public:fourWebs()
					public.maskState = "to75"
				elseif currentCount == 2 then
					public:eightWebs()
					public.maskState = "to50"
				elseif currentCount == 3 then
					public:sixteenWebs()
					public.maskState = "to25"
				elseif currentCount == 4 then
					public:sixteenWebs()
					public.maskState = "to12"
				elseif currentCount == 5 then
					public.maskState = "to00"
				end
			elseif game.levelsScreen.selectedLevel < 31 then
				if currentCount == 1 then
					public:fourWebs()
					public.maskState = "to87"
				elseif currentCount == 2 then
					public:eightWebs()
					public.maskState = "to75"
				elseif currentCount == 3 then
					public:sixteenWebs()
					public.maskState = "to50"
				elseif currentCount == 4 then
					public:sixteenWebs()
					public.maskState = "to25"
				elseif currentCount == 5 then
					public:sixteenWebs()
					public.maskState = "to12"
				elseif currentCount == 6 then
					public.maskState = "to00"
				end
			elseif game.levelsScreen.selectedLevel < 41 then
				if currentCount == 1 then
					public:fourWebs()
					public.maskState = "to93"
				elseif currentCount == 2 then
					public:eightWebs()
					public.maskState = "to87"
				elseif currentCount == 3 then
					public:sixteenWebs()
					public.maskState = "to75"
				elseif currentCount == 4 then
					public:sixteenWebs()
					public.maskState = "to50"
				elseif currentCount == 5 then
					public:sixteenWebs()
					public.maskState = "to25"
				elseif currentCount == 6 then
					public:sixteenWebs()
					public.maskState = "to12"
				elseif currentCount == 7 then
					public.maskState = "to00"
				end
			elseif game.levelsScreen.selectedLevel < 51 then
				if currentCount == 1 then
					public:twoWebs()
					public.maskState = "to96"
				elseif currentCount == 2 then
					public:fourWebs()
					public.maskState = "to93"
				elseif currentCount == 3 then
					public:eightWebs()
					public.maskState = "to87"
				elseif currentCount == 4 then
					public:sixteenWebs()
					public.maskState = "to75"
				elseif currentCount == 5 then
					public:sixteenWebs()
					public.maskState = "to50"
				elseif currentCount == 6 then
					public:sixteenWebs()
					public.maskState = "to25"
				elseif currentCount == 7 then
					public:sixteenWebs()
					public.maskState = "to12"
				elseif currentCount == 8 then
					public.maskState = "to00"
				end
			else
				-- if currentCount == 1 then
				-- 	public:twoWebs()
				-- elseif currentCount == 2 then
				-- 	public:fourWebs()
				-- 	public.maskState = "to50"
				-- elseif currentCount == 3 then
				-- 	public:eightWebs()
				-- 	public.maskState = "to25"
				-- elseif currentCount == 4 then
				-- 	public:sixteenWebs()
				-- 	public.maskState = "to12"
				-- elseif currentCount == 5 then
				-- 	public.maskState = "to00"
				-- end
			end

			game.sfxEngine:play("damage")

			local x = math.random(-30, 30)
			local y = math.random(-30, 30)
			game.gameScreen.camera.x = game.gameScreen.camera.x + x
			game.gameScreen.camera.y = game.gameScreen.camera.y + y
			game.musicEngine.currentTrack:setPitch(0.2)
			game.musicEngine.deafLevel = 0.5
			public.flashColor[4] = 1
		else
			game.sfxEngine:play("damage")
		end
	end

	function public:toDefault()
		for i = 1, #public.list do
			public.list[i][1] = -100
			public.list[i][2] = -100
			public.list[i][3] = 0
		end
		public.websRandom = 0
		public.maskOpacity = 1
		public.maskState = "standby"
	end

	function public.update()
		if public.maskState == "standby" then
			-- no code

		elseif public.maskState == "to96" then
			if public.maskOpacity > 0.96 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end

		elseif public.maskState == "to93" then
			if public.maskOpacity > 0.93 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end
		
		elseif public.maskState == "to87" then
			if public.maskOpacity > 0.87 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end
		
		elseif public.maskState == "to75" then
			if public.maskOpacity > 0.75 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end

		elseif public.maskState == "to50" then
			if public.maskOpacity > 0.50 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end

		elseif public.maskState == "to25" then
			if public.maskOpacity > 0.25 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end

		elseif public.maskState == "to12" then
			if public.maskOpacity > 0.12 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskState = "standby"
			end

		elseif public.maskState == "to00" then
			if public.maskOpacity > 0 then
				public.maskOpacity = public.maskOpacity - public.maskAnimarionSpeed
			else
				public.maskOpacity = 0
				public.maskState = "standby"
			end

		end

		if public.flashColor[4] > 0 then
			public.flashColor[4] = public.flashColor[4] - public.flashColorAnimationSpeed
		else
			public.flashColor[4] = 0
		end
	end

	function public:draw()
		public.color[4] = 0.75
		for i = 1, #public.list do
			local shift = math.random(0, public.websRandom)
			if (math.random() > 0.99) then
				love.graphics.setColor(math.random(), math.random(), math.random(), public.color[4] * public.list[i][3])
			else
				love.graphics.setColor(public.color[1], public.color[2], public.color[3], public.color[4] * public.list[i][3])
			end
			love.graphics.line(public.list[i][1] + shift, 0, public.list[i][2] - shift, game.wh)
			love.graphics.line(public.list[i][1], 0, public.list[i][2], game.wh)
			love.graphics.line(public.list[i][1] - shift, 0, public.list[i][2] + shift, game.wh)

			if public.list[i][1] > 0 and public.list[i][3] < 1 then
				public.list[i][3] = public.list[i][3] + public.webOpacitySpeed
			end
		end
		public.color[4] = 1 - public.maskOpacity
		love.graphics.setColor(public.color)
		love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
		love.graphics.setColor(math.random(), math.random(), math.random(), public.flashColor[4])
		love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
	end

	setmetatable(public, self)
	self.__index = self; return public
end



PressAnyKey = {}
function PressAnyKey:new()
	local public = {}
		if OS == 'Android' or OS == 'iOS' then
			public.text = ui.touchScreen
		else
			public.text = ui.pressAnyKey
		end
		public.const = levelTitleFontSize
		public.x = 0
		public.y = 0
		public.state = "hidden"
		public.timer = 0
		public.shift = 0
		public.shiftState = "up"
		public.maxShift = 8
		public.animationSpeed = 0.3
		public.forCancel = false
		public.forCancelText = ui.forCancel
		public.opacity = 0
		public.opacitySpeed = 0.1

	function public:update()
		public.const = levelTitleFontSize
		if public.state == "toshown" then
			if public.timer < 300 then
				public.timer = public.timer + 1
			else
				public.state = "shown"
				public.shift = 0
				public.shiftState = "up"
			end
		elseif public.state == "shown" then
			if public.shiftState == "up" then
				if public.shift < public.maxShift then
					public.shift = public.shift + public.animationSpeed
				else
					public.shiftState = "down"
				end
			elseif public.shiftState == "down" then
				if public.shift > 0 then
					public.shift = public.shift - public.animationSpeed
				else
					public.shiftState = "up"
				end
			end
		end

		if public.state == "shown" then
			if public.opacity < 1 then
				public.opacity = public.opacity + public.opacitySpeed
			end
		else
			if public.opacity > 0 then
				public.opacity = public.opacity - public.opacitySpeed
			end
		end

		public.x = public.const
		public.y = game.wh - public.const - public.shift
	end

	function public:draw()
		if public.opacity > 0 and game.hud.webs.maskOpacity > 0 then
			love.graphics.setColor(white[1], white[2], white[3], public.opacity)
			love.graphics.setFont(tipFont)
			if public.forCancel then
				love.graphics.print(public.text..' '..public.forCancelText, public.x, public.y)
			else
				love.graphics.print(public.text, public.x/4, public.y)
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



PressEscape = {}
function PressEscape:new()
	local public = {}
		if OS == 'Android' or OS == 'iOS' then
			public.text = ui.touchTriangle
		else
			public.text = ui.pressEscape
		end
		public.const = levelTitleFontSize
		public.x = 0
		public.y = 0
		public.state = "hidden"
		public.shift = 0
		public.shiftState = "up"
		public.maxShift = 8
		public.animationSpeed = 0.3
		public.opacity = 0
		public.opacitySpeed = 0.1

	function public:update()
		public.const = levelTitleFontSize
		if game.hud.webs.maskOpacity == 0 then
			public.state = "shown"
		else
			public.state = "hidden"
		end

		if public.state == "shown" then
			if public.opacity < 1 then
				public.opacity = public.opacity + public.opacitySpeed
			end
		else
			public.opacity = 0
		end

		if public.shiftState == "up" then
			if public.shift < public.maxShift then
				public.shift = public.shift + public.animationSpeed
			else
				public.shiftState = "down"
			end
		elseif public.shiftState == "down" then
			if public.shift > 0 then
				public.shift = public.shift - public.animationSpeed
			else
				public.shiftState = "up"
			end
		end

		public.x = public.const
		public.y = game.wh - public.const - public.shift
	end

	function public:draw()
		if public.state == "shown" then
			love.graphics.setColor(white[1], white[2], white[3], public.opacity)
			love.graphics.setFont(tipFont)
			love.graphics.print(public.text, public.x/4, public.y)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



QuitingText = {}
function QuitingText:new()
	local public = {}
		public.text = ui.quitingIncrement
		public.const = levelTitleFontSize
		public.x = 0
		public.y = 0
		public.state = "hidden"
		public.timer = 0
		public.shift = 0
		public.shiftState = "up"
		public.maxShift = 8
		public.animationSpeed = 0.3
		public.opacity = 0
		public.opacitySpeed = 0.1

	function public:update()
		public.const = levelTitleFontSize
		if public.state == "shown" then
			if public.opacity < 1 then
				public.opacity = public.opacity + public.opacitySpeed
			end
		else
			if public.opacity > 0 then
				public.opacity = public.opacity - public.opacitySpeed
			end
		end


		if public.shiftState == "up" then
			if public.shift < public.maxShift then
				public.shift = public.shift + public.animationSpeed
			else
				public.shiftState = "down"
			end
		elseif public.shiftState == "down" then
			if public.shift > 0 then
				public.shift = public.shift - public.animationSpeed
			else
				public.shiftState = "up"
			end
		end

		public.x = public.const
		public.y = game.wh - public.const - public.shift
	end

	function public:draw()
		if public.opacity > 0 then
			love.graphics.setColor(white[1], white[2], white[3], public.opacity)
			love.graphics.setFont(tipFont)
			love.graphics.print(public.text, public.x/4, public.y)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Background = {}
function Background:new()
	public = {}
		public.color = {}
		public.color[1] = darkColdGrey[1]
		public.color[2] = darkColdGrey[2]
		public.color[3] = darkColdGrey[3]
		public.state = "standby"
		public.tweenColor = {}
		public.animationSpeed = 0.05

	function public:update()
		if public.state == "standby" then
			-- no code

		elseif public.state == "tween" then
			if (public.color[1] ~= public.tweenColor[1]) or (public.color[2] ~= public.tweenColor[2]) or (public.color[3] ~= public.tweenColor[3]) then
				if math.abs(public.color[1] - public.tweenColor[1]) > public.animationSpeed then
					if public.color[1] < public.tweenColor[1] then
						public.color[1] = public.color[1] + public.animationSpeed
					elseif public.color[1] > public.tweenColor[1] then
						public.color[1] = public.color[1] - public.animationSpeed
					end
				else
					public.color[1] = public.tweenColor[1]
				end
				if math.abs(public.color[2] - public.tweenColor[2]) > public.animationSpeed then
					if public.color[2] < public.tweenColor[2] then
						public.color[2] = public.color[2] + public.animationSpeed
					elseif public.color[2] > public.tweenColor[2] then
						public.color[2] = public.color[2] - public.animationSpeed
					end
				else
					public.color[2] = public.tweenColor[2]
				end
				if math.abs(public.color[3] - public.tweenColor[3]) > public.animationSpeed then
					if public.color[3] < public.tweenColor[3] then
						public.color[3] = public.color[3] + public.animationSpeed
					elseif public.color[3] > public.tweenColor[3] then
						public.color[3] = public.color[3] - public.animationSpeed
					end
				else
					public.color[3] = public.tweenColor[3]
				end
			else
				public.state = "standby"
			end

		end
	end

	function public:setColor(newColor)
		public.color[1] = newColor[1]
		public.color[2] = newColor[2]
		public.color[3] = newColor[3]
	end

	function public:setColorTween(newColor)
		public.state = "tween"
		public.tweenColor = newColor
	end

	function public:setColorByTome(selectedLevel)
		if selectedLevel > 0 and selectedLevel < 11 then
			public:setColorTween(darkBlue)
		elseif selectedLevel > 10 and selectedLevel < 21 then
			public:setColorTween(darkOrange)
		elseif selectedLevel > 20 and selectedLevel < 31 then
			public:setColorTween(darkGreen)
		elseif selectedLevel > 30 and selectedLevel < 41 then
			public:setColorTween(darkPurple)
		elseif selectedLevel > 40 and selectedLevel < 51 then
			public:setColorTween(darkRed)
		elseif selectedLevel > 50 and selectedLevel < 61 then
			public:setColorTween(darkGrey)
		end
	end

	function public:draw()
		if game.state ~= "game" then
			love.graphics.setColor(game.levelsScreen.finalLevelColor)
			love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
		end
		love.graphics.setBackgroundColor(public.color)
	end

	setmetatable(public, self)
	self.__index = self; return public
end


GameShining = {}
function GameShining:new()
	local public = {}
		public.color = {blue[1], blue[2], blue[3], 0}
		public.opacity = 0
		public.growSpeed = 0.0025
		public.maxOpacity = 0.1
		public.grow = true

	function public:update()
		if public.grow then
			if public.opacity < public.maxOpacity then
				public.opacity = public.opacity + public.growSpeed * game.levelsScreen.selectedLevel/60
			else
				public.opacity = public.maxOpacity
				public.grow = false
			end
		else
			if public.opacity > 0 then
				public.opacity = public.opacity - public.growSpeed * game.levelsScreen.selectedLevel/60
			else
				-- public.color[4] = 0
				public.color = {
					game.gameScreen.lightColor[1] + math.random()/30,
					game.gameScreen.lightColor[2] + math.random()/30,
					game.gameScreen.lightColor[3] + math.random()/30,
					0
				}
				public.opacity = 0
				public.grow = true
			end
		end
		local coef = (game.levelsScreen.selectedLevel%10)/10
		if coef == 0 then coef = 1 end
		public.color[4] = public.opacity * coef
	end

	function public:draw()
		love.graphics.setColor(public.color)
		love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
	end

	setmetatable(public, self)
	self.__index = self; return public
end


Ashes = {}
function Ashes:new()
	local public = {}
		public.opacity = 0.5
		public.layers = {}
		public.layers[1] = {}
		public.layers[2] = {}
		public.layers[3] = {}
		public.layers[4] = {}
		public.layers[5] = {}
		public.layers[6] = {}

		public.layers[1].color = {}
		public.layers[1].color[1] = lightBlue[1]
		public.layers[1].color[2] = lightBlue[2]
		public.layers[1].color[3] = lightBlue[3]
		public.layers[1].color[4] = public.opacity
		public.layers[2].color = {}
		public.layers[2].color[1] = lightOrange[1]
		public.layers[2].color[2] = lightOrange[2]
		public.layers[2].color[3] = lightOrange[3]
		public.layers[2].color[4] = public.opacity
		public.layers[3].color = {}
		public.layers[3].color[1] = lightGreen[1]
		public.layers[3].color[2] = lightGreen[2]
		public.layers[3].color[3] = lightGreen[3]
		public.layers[3].color[4] = public.opacity
		public.layers[4].color = {}
		public.layers[4].color[1] = lightPurple[1]
		public.layers[4].color[2] = lightPurple[2]
		public.layers[4].color[3] = lightPurple[3]
		public.layers[4].color[4] = public.opacity
		public.layers[5].color = {}
		public.layers[5].color[1] = lightRed[1]
		public.layers[5].color[2] = lightRed[2]
		public.layers[5].color[3] = lightRed[3]
		public.layers[5].color[4] = public.opacity
		public.layers[6].color = {}
		public.layers[6].color[1] = white[1]
		public.layers[6].color[2] = white[2]
		public.layers[6].color[3] = white[3]
		public.layers[6].color[4] = public.opacity

		local width = love.graphics.getWidth()
		local height = love.graphics.getHeight()
		for i = 1, 40 do
			public.layers[1][i] = {}
			public.layers[1][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[1][i].y = screenHeight + 40
			public.layers[1][i].r = 7
			public.layers[1][i].speed = math.random() * 6 + 1
			public.layers[1][i].maxShift = public.layers[1][i].r
			public.layers[1][i].shiftDirection = 1
			public.layers[1][i].shift = 0
			public.layers[1][i].shiftAnimationSpeed = math.random()/2
		end

		for i = 1, 80 do
			public.layers[2][i] = {}
			public.layers[2][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[2][i].y = screenHeight + 40
			public.layers[2][i].r = 6
			public.layers[2][i].speed = math.random() * 5 + 1
			public.layers[2][i].maxShift = public.layers[2][i].r
			public.layers[2][i].shiftDirection = 1
			public.layers[2][i].shift = 0
			public.layers[2][i].shiftAnimationSpeed = math.random()/2
		end

		for i = 1, 120 do
			public.layers[3][i] = {}
			public.layers[3][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[3][i].y = screenHeight + 40
			public.layers[3][i].r = 5
			public.layers[3][i].speed = math.random() * 4 + 1
			public.layers[3][i].maxShift = public.layers[3][i].r
			public.layers[3][i].shiftDirection = 1
			public.layers[3][i].shift = 0
			public.layers[3][i].shiftAnimationSpeed = math.random()/2
		end

		for i = 1, 160 do
			public.layers[4][i] = {}
			public.layers[4][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[4][i].y = screenHeight + 40
			public.layers[4][i].r = 4
			public.layers[4][i].speed = math.random() * 3 + 1
			public.layers[4][i].maxShift = public.layers[4][i].r
			public.layers[4][i].shiftDirection = 1
			public.layers[4][i].shift = 0
			public.layers[4][i].shiftAnimationSpeed = math.random()/2
		end

		for i = 1, 200 do
			public.layers[5][i] = {}
			public.layers[5][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[5][i].y = screenHeight + 40
			public.layers[5][i].r = 3
			public.layers[5][i].speed = math.random() * 2 + 1
			public.layers[5][i].maxShift = public.layers[5][i].r
			public.layers[5][i].shiftDirection = 1
			public.layers[5][i].shift = 0
			public.layers[5][i].shiftAnimationSpeed = math.random()/2
		end

		for i = 1, 240 do
			public.layers[6][i] = {}
			public.layers[6][i].x = math.random(0, screenWidth*2) - screenWidth/2
			public.layers[6][i].y = screenHeight + 40
			public.layers[6][i].r = 2
			public.layers[6][i].speed = math.random() + 1
			public.layers[6][i].maxShift = public.layers[6][i].r
			public.layers[6][i].shiftDirection = 1
			public.layers[6][i].shift = 0
			public.layers[6][i].shiftAnimationSpeed = math.random()/2
		end



	function public:update()
		for layer = 1, #public.layers do
			for i = 1, #public.layers[layer] do
				if public.layers[layer][i].y > - 40 then
					public.layers[layer][i].y = public.layers[layer][i].y - public.layers[layer][i].speed
					if public.layers[layer][i].shiftDirection == 1 then
						if public.layers[layer][i].shift < public.layers[layer][i].maxShift then
							public.layers[layer][i].shift = public.layers[layer][i].shift + public.layers[layer][i].shiftAnimationSpeed
						else
							public.layers[layer][i].shiftDirection = -1
						end
					else
						if public.layers[layer][i].shift > -public.layers[layer][i].maxShift then
							public.layers[layer][i].shift = public.layers[layer][i].shift - public.layers[layer][i].shiftAnimationSpeed
						else
							public.layers[layer][i].shiftDirection = 1
						end
					end
				else
					public.layers[layer][i].x = math.random(0, screenWidth)
					public.layers[layer][i].y = screenHeight + 40
				end
			end
		end
	end

	function public:draw()
		for layer = 1, #public.layers do
			if ((layer == 1) and (game.levelsScreen.maxOpenedLevel > 10))
			or ((layer == 2) and (game.levelsScreen.maxOpenedLevel > 20))
			or ((layer == 3) and (game.levelsScreen.maxOpenedLevel > 30))
			or ((layer == 4) and (game.levelsScreen.maxOpenedLevel > 40))
			or ((layer == 5) and (game.levelsScreen.maxOpenedLevel > 50))
			or ((layer == 6) and (game.levelsScreen.maxOpenedLevel > 59)) then
				love.graphics.setColor(public.layers[layer].color)
				for i = 1, #public.layers[layer] do
					love.graphics.polygon("fill", 
						public.layers[layer][i].x + public.layers[layer][i].shift,
						public.layers[layer][i].y - public.layers[layer][i].r,
						public.layers[layer][i].x + public.layers[layer][i].r + public.layers[layer][i].shift,
						public.layers[layer][i].y + public.layers[layer][i].r,
						public.layers[layer][i].x - public.layers[layer][i].r + public.layers[layer][i].shift,
						public.layers[layer][i].y + public.layers[layer][i].r
					)
				end
			end
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end

ClearProgressSplash = {}
function ClearProgressSplash:new()
	local public = {}
		public.state = "hidden"
		public.text = ui.progressDeleting
		public.const = 40
		public.color = {}
		public.color[1] = lightRed[1]
		public.color[2] = lightRed[2]
		public.color[3] = lightRed[3]
		public.color[4] = 0
		public.colorAnimationSpeed = 0.1
		public.quitingProgress = 1
		public.quitingProgressSpeed = 0.003
		public.size = 0
		public.progressColor = {}
		public.progressColorHighlight = {}
		public.progressColor[1] = yellow[1]
		public.progressColor[2] = yellow[2]
		public.progressColor[3] = yellow[3]
		public.progressColor[4] = 1
		public.progressColorHighlight[1] = lightYellow[1]
		public.progressColorHighlight[2] = lightYellow[2]
		public.progressColorHighlight[3] = lightYellow[3]
		public.progressColorHighlight[4] = 1
		public.blendColor = {}
		public.blendColor[1] = black[1]
		public.blendColor[2] = black[2]
		public.blendColor[3] = black[3]
		public.blendColor[4] = 0
		public.blendShift = 10
		public.textColor = {}
		public.textColor[1] = white[1]
		public.textColor[2] = white[2]
		public.textColor[3] = white[3]
		public.textColor[4] = 0

	function public:update()
		public.size = game.ww/16
		public.quitingProgressSpeed = 0.003

		if public.state == "hidden" then

		elseif public.state == "toshown" then
			if public.color[4] < 1 then
				public.color[4] = public.color[4] + public.colorAnimationSpeed
			else
				public.state = "shown"
				public.color[4] = 1
				game.hud.pressAnyKey.state = "shown"
				game.hud.pressAnyKey.forCancel = true
			end

			public.quitingProgress = 1
			public.blendColor[4] = public.color[4]/2

		elseif public.state == "shown" then
			if public.quitingProgress > 0 then
				public.quitingProgress = public.quitingProgress - public.quitingProgressSpeed
			else
				game.levelsScreen.loadingSaving:clearProgress()
				game.levelsScreen.maxOpenedLevel = 1
				public.state = "tohidden"
				game.levelsScreen.state = "totitle"
				game.titleScreen.state = "waiting"
				game.levelsScreen.nextWorldName = ""
				game.levelsScreen.worldNameState = "out"
				game.sfxEngine:play("cancel")
			end
			
		elseif public.state == "tohidden" then
			if public.color[4] > 0 then
				public.color[4] = public.color[4] - public.colorAnimationSpeed
			else
				public.state = "hidden"
				public.color[4] = 0
			end

			game.hud.pressAnyKey.state = "hidden"
			game.hud.pressAnyKey.forCancel = false
			public.blendColor[4] = public.color[4]/2

		end
	end

	function public:draw()
		if public.state ~= "hidden" then
			love.graphics.setColor(public.color[1], public.color[2], public.color[3], public.color[4])
			love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
			love.graphics.setColor(black10)
			for i = 1, public.blendShift, 2 do
				love.graphics.circle("fill", game.ww/2, game.wh/2 + i, public.size * public.quitingProgress, 4)
			end
			love.graphics.setColor(public.progressColor)
			love.graphics.circle("line", game.ww/2, game.wh/2, public.size, 4)
			love.graphics.circle("fill", game.ww/2, game.wh/2, public.size * public.quitingProgress, 4)
			love.graphics.polygon("fill",
				game.ww/2 - public.size * public.quitingProgress, game.wh/2,
				game.ww/2 + public.size * public.quitingProgress, game.wh/2,
				game.ww/2 + public.size * public.quitingProgress, game.wh/2 - 5,
				game.ww/2 - public.size * public.quitingProgress, game.wh/2 - 5)
			love.graphics.setColor(public.progressColorHighlight)
			love.graphics.circle("fill", game.ww/2, game.wh/2 - 5, public.size * public.quitingProgress, 4)
			love.graphics.setColor(white)
			love.graphics.setFont(titleFont)
			love.graphics.print(public.text, game.ww/2 - titleFont:getWidth(public.text)/2, public.const)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



ChapterPrologue = {}
function ChapterPrologue:new()
	local public = {}
		public.active = false
		public.state = "in"
		public.text = {"NO_TEXT", "NO_TEXT"}
		public.timer = 0

		public.backgroundColor = {}
		public.backgroundColor[1] = darkColdGrey[1]
		public.backgroundColor[2] = darkColdGrey[2]
		public.backgroundColor[3] = darkColdGrey[3]
		public.backgroundColor[4] = 1

		public.textColor = {}
		public.textColor[1] = white[1]
		public.textColor[2] = white[2]
		public.textColor[3] = white[3]
		public.textColor[4] = 0
		public.textColorAnimationSpeed = 0.03

		public.canNotPassText = ui.canNotSkip
		public.canNotPassTextColor = {
			white[1],
			white[2],
			white[3],
			0
		}
		public.canNotPassTextShow = false
		public.canNotPassTextAnimationSpeed = 0.1
		public.canNotPassTextConst = levelTitleFontSize

	function public:setChapter(chapterNumber)
		public.active = true
		public.chapterNumber = chapterNumber
		public.textCounter = 1
		public.text = {chapterPrologues[public.chapterNumber][public.textCounter][1], chapterPrologues[public.chapterNumber][public.textCounter][2]}
		public.state = "in"
		game.musicEngine:setVolume(0)
	end

	function public:update()
		public.canNotPassTextConst = levelTitleFontSize
		if public.state == "in" then
			if public.textColor[4] < 1 then
				public.textColor[4] = public.textColor[4] + public.textColorAnimationSpeed
			else
				public.textColor[4] = 1
				public.timer = 0
				public.state = "showing"
			end

		elseif public.state == "showing" then
			if public.timer < chapterPrologues[public.chapterNumber][public.textCounter][3] then
				public.timer = public.timer + 1
			else
				public.state = "out"
			end

		elseif public.state == "out" then
			if public.textColor[4] > 0 then
				public.textColor[4] = public.textColor[4] - public.textColorAnimationSpeed
			else
				public.textColor[4] = 0
				if public.textCounter >= #chapterPrologues[public.chapterNumber] then
					public.active = false
					game.musicEngine:nextTrack(game.levelsScreen.selectedLevel, true)
				else
					public.textCounter = public.textCounter + 1
					public.text = {chapterPrologues[public.chapterNumber][public.textCounter][1], chapterPrologues[public.chapterNumber][public.textCounter][2]}
					public.state = "in"
					game.sfxEngine:play("damage")
					public.timer = 0
				end
			end

		end

		if public.canNotPassTextShow then
			if public.canNotPassTextColor[4] < 1.5 then
				public.canNotPassTextColor[4] = public.canNotPassTextColor[4] + public.canNotPassTextAnimationSpeed
			else
				public.canNotPassTextColor[4] = 1.5
				public.canNotPassTextShow = false
			end
		else
			if public.canNotPassTextColor[4] > 0 then
				public.canNotPassTextColor[4] = public.canNotPassTextColor[4] - public.canNotPassTextAnimationSpeed
			else
				public.canNotPassTextColor[4] = 0
			end
		end
	end

	function public:draw()
		love.graphics.setColor(public.backgroundColor)
		love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
		love.graphics.setFont(titleFont)
		
		local direction = math.random(0, 2) - 1
		love.graphics.setColor(math.random(), math.random(), math.random(), public.textColor[4])
		love.graphics.print(public.text[1], game.ww/2 - titleFont:getWidth(public.text[1])/2 - direction * (1 - public.textColor[4])*25, game.wh/3)
		direction = math.random(0, 2) - 1
		love.graphics.setColor(math.random(), math.random(), math.random(), public.textColor[4])
		love.graphics.print(public.text[2], game.ww/2 - titleFont:getWidth(public.text[2])/2 - direction * (1 - public.textColor[4])*25, game.wh/3  + titleFont:getHeight(public.text[1]) )

		if public.chapterNumber == 4 and public.text[1] ~= "" then
			love.graphics.setColor(math.random(), math.random(), math.random(), public.textColor[4] * math.random())

		elseif public.chapterNumber == 5 and public.text[1] ~= "" then
			love.graphics.setColor(red[1], red[2], red[3], public.textColor[4])
			love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
			love.graphics.setColor(darkColdGrey[1], darkColdGrey[2], darkColdGrey[3], public.textColor[4])

		elseif public.chapterNumber == 6 and public.text[1] ~= "" then
			love.graphics.setColor(math.random()/4 + 0.9, math.random()/4 + 0.9, math.random()/4 + 0.9, public.textColor[4])
			love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
			love.graphics.setColor(darkColdGrey[1], darkColdGrey[2], darkColdGrey[3], public.textColor[4])
		else
			love.graphics.setColor(public.textColor)
		end

		love.graphics.print(public.text[1], game.ww/2 - titleFont:getWidth(public.text[1])/2 + math.random()*titleFontSize/16-titleFontSize/32, game.wh/3 + math.random()*titleFontSize/16-titleFontSize/32)
		love.graphics.print(public.text[2], game.ww/2 - titleFont:getWidth(public.text[2])/2 + math.random()*titleFontSize/16-titleFontSize/32, game.wh/3  + titleFont:getHeight(public.text[1]) + math.random()*titleFontSize/16-titleFontSize/32)

		if public.chapterNumber == 1 then
			local circle = {}
			for i = 1, 12 do
				local cosAngle = math.cos(math.rad((i - 1) * 30))
				local sinAngle = math.sin(math.rad((i - 1) * 30))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/12*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/12*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end

			love.graphics.setColor(math.random(), math.random(), math.random(), math.random())
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)

		elseif public.chapterNumber == 2 then
			local circle = {}
			for i = 1, 24 do
				local cosAngle = math.cos(math.rad((i - 1) * 15))
				local sinAngle = math.sin(math.rad((i - 1) * 15))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/10*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/10*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end
			
			love.graphics.setColor(math.random(), math.random(), math.random(), math.random())
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)

		elseif public.chapterNumber == 3 then
			local circle = {}
			for i = 1, 48 do
				local cosAngle = math.cos(math.rad((i - 1) * 7.5))
				local sinAngle = math.sin(math.rad((i - 1) * 7.5))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/8*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/8*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end
			
			love.graphics.setColor(math.random(), math.random(), math.random(), math.random())
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)

		elseif public.chapterNumber == 4 then
			local circle = {}
			for i = 1, 96 do
				local cosAngle = math.cos(math.rad((i - 1) * 3.75))
				local sinAngle = math.sin(math.rad((i - 1) * 3.75))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/7*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/7*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end
			
			love.graphics.setColor(math.random(), math.random(), math.random(), math.random())
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)			

		elseif public.chapterNumber == 5 then
			local circle = {}
			for i = 1, 192 do
				local cosAngle = math.cos(math.rad((i - 1) * 1.875))
				local sinAngle = math.sin(math.rad((i - 1) * 1.875))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/6*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/6*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end
			
			love.graphics.setColor(darkColdGrey)
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)

		elseif public.chapterNumber == 6 then
			local circle = {}
			for i = 1, 192 do
				local cosAngle = math.cos(math.rad((i - 1) * 1.875))
				local sinAngle = math.sin(math.rad((i - 1) * 1.875))
				local move = math.random()/10*3 + 0.6
				local dotX = game.ww/2 + (game.wh/5*move)*cosAngle
				local dotY = game.wh/3*2 - (game.wh/5*move)*sinAngle
				table.insert(circle, dotX)
				table.insert(circle, dotY)
			end
			
			love.graphics.setColor(darkColdGrey)
			circle = shuffleArray(circle)
			love.graphics.polygon('line', circle)

		end

		love.graphics.setColor(public.canNotPassTextColor)
		love.graphics.setFont(tipFont)
		love.graphics.print(public.canNotPassText, public.canNotPassTextConst/4, game.wh - public.canNotPassTextConst)
	end

	setmetatable(public, self)
	self.__index = self; return public
end
