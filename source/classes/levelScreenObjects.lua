LevelIcon = {}
function LevelIcon:new(newLevel)
	local private = {}

	local public = {}
		public.state = "standby"
		public.level = newLevel
		public.rSize = 12
		public.distance = 4
		public.animationSpeed = 25
		public.x = love.graphics.getWidth()/2
		public.y = love.graphics.getHeight()/2
		public.r = love.graphics.getWidth()/public.rSize
		public.blendColor = black50
		public.blendShift = 16
		public.blendShiftSmall = 5
		public.sparkSize = 0.25
		public.shineColor = {white[1], white[2], white[3], 0}
		public.maxShine = 0.25
		public.shineColorAnimationSpeed = 0.005
		public.shineState = "standby"
		public.maxDotsR = public.r
		public.dotsR = 0
		public.dotsSpeed = 2
		public.dotsColor = {}
		public.dotsColor[1] = white[1]
		public.dotsColor[2] = white[2]
		public.dotsColor[3] = white[3]
		public.dotsColor[4] = 1
		public.angle = 0

	function public:update()
		public.animationSpeed = game.wh/24

		if public.state == "standby" then
			-- no code
		elseif public.state == "toright" then
			if public.x < game.ww/2 + game.ww/public.distance then
				public.x = public.x + public.animationSpeed
			else
				public.state = "standby"
				public.x = game.ww/2 + game.ww/public.distance
			end

		elseif public.state == "toleft" then
			if (public.x > game.ww/2 - game.ww/public.distance) then
				public.x = public.x - public.animationSpeed
			else
				public.state = "standby"
				public.x = game.ww/2 - game.ww/public.distance
			end

		elseif public.state == "tocenter" then
			if (public.x > game.ww/2) and math.abs(public.x-game.ww/2) > public.animationSpeed then
				public.x = public.x - public.animationSpeed
			elseif (public.x < game.ww/2) and math.abs(public.x-game.ww/2) > public.animationSpeed then
				public.x = public.x + public.animationSpeed
			else
				public.x = game.ww/2
				public.state = "standby"
			end

		elseif public.state == "outscreenright" then
			if public.x < game.ww + public.r then
				public.x = public.x + public.animationSpeed
			else
				public.state = "standby"
				public.image = nil
			end

		elseif public.state == "outscreenleft" then
			if public.x > -public.r then
				public.x = public.x - public.animationSpeed
			else
				public.state = "standby"
				public.image = nil
			end

		elseif public.state == "fromscreenleft" then
			if (public.x < game.ww/2 - game.ww/public.distance) and math.abs(game.ww/2 - game.ww/public.distance - public.x) > public.animationSpeed then
				public.x = public.x + public.animationSpeed
			else
				public.state = "standby"
			end

		elseif public.state == "fromscreenright" then
			if (public.x > game.ww/2 + game.ww/public.distance) and math.abs(public.x - game.ww/2 - game.ww/public.distance) > public.animationSpeed then
				public.x = public.x - public.animationSpeed
			else
				public.state = "standby"
			end

		end

	end

	function public:draw()
		local bgColor = {0, 0, 0}
		local arrowColor = {0, 0, 0}
		local sideColor = {0, 0, 0}
		if public.level > 0 and public.level < 11 then
			bgColor[1] = lightBlue[1]
			bgColor[2] = lightBlue[2]
			bgColor[3] = lightBlue[3]
			arrowColor[1] = darkBlue[1]
			arrowColor[2] = darkBlue[2]
			arrowColor[3] = darkBlue[3]
			sideColor[1] = blue[1]
			sideColor[2] = blue[2]
			sideColor[3] = blue[3]
		elseif public.level > 10 and public.level < 21 then
			bgColor[1] = lightOrange[1]
			bgColor[2] = lightOrange[2]
			bgColor[3] = lightOrange[3]
			arrowColor[1] = darkOrange[1]
			arrowColor[2] = darkOrange[2]
			arrowColor[3] = darkOrange[3]
			sideColor[1] = orange[1]
			sideColor[2] = orange[2]
			sideColor[3] = orange[3]
		elseif public.level > 20 and public.level < 31 then
			bgColor[1] = lightGreen[1]
			bgColor[2] = lightGreen[2]
			bgColor[3] = lightGreen[3]
			arrowColor[1] = darkGreen[1]
			arrowColor[2] = darkGreen[2]
			arrowColor[3] = darkGreen[3]
			sideColor[1] = green[1]
			sideColor[2] = green[2]
			sideColor[3] = green[3]
		elseif public.level > 30 and public.level < 41 then
			bgColor[1] = lightPurple[1]
			bgColor[2] = lightPurple[2]
			bgColor[3] = lightPurple[3]
			arrowColor[1] = darkPurple[1]
			arrowColor[2] = darkPurple[2]
			arrowColor[3] = darkPurple[3]
			sideColor[1] = purple[1]
			sideColor[2] = purple[2]
			sideColor[3] = purple[3]
		elseif public.level > 40 and public.level < 51 then
			bgColor[1] = lightRed[1]
			bgColor[2] = lightRed[2]
			bgColor[3] = lightRed[3]
			arrowColor[1] = darkRed[1]
			arrowColor[2] = darkRed[2]
			arrowColor[3] = darkRed[3]
			sideColor[1] = red[1]
			sideColor[2] = red[2]
			sideColor[3] = red[3]
		elseif public.level > 50 and public.level < 61 then
			bgColor[1] = lightGrey[1]
			bgColor[2] = lightGrey[2]
			bgColor[3] = lightGrey[3]
			arrowColor[1] = darkGrey[1]
			arrowColor[2] = darkGrey[2]
			arrowColor[3] = darkGrey[3]
			sideColor[1] = grey[1]
			sideColor[2] = grey[2]
			sideColor[3] = grey[3]
		end

		if public.shineState == "up" then
			if public.shineColor[4] < public.maxShine then
				public.shineColor[4] = public.shineColor[4] + public.shineColorAnimationSpeed
			else
				public.shineColor[4] = public.maxShine
				public.shineState = "down"
			end
		elseif public.shineState == "down" then
			if public.shineColor[4] > 0 then
				public.shineColor[4] = public.shineColor[4] - public.shineColorAnimationSpeed
			else
				public.shineColor[4] = 0
				public.shineState = "standby"
			end
		elseif public.shineState == "standby" then
			if public.shineColor[4] > -1 then
				public.shineColor[4] = public.shineColor[4] - public.shineColorAnimationSpeed
			else
				public.shineColor[4] = 0
				public.shineState = "up"
			end
		end

		if public.level == game.levelsScreen.selectedLevel
		and public.level > 0 and public.level < 61 then
			public.angle = public.angle + game.levelsScreen.rotateDirection
			if public.angle == 360 then public.angle = 0 end
			public.maxDotsR = public.r * 1.2

			if public.state == "toright" or public.state == "toleft" then
				if public.dotsR > 0 then
					public.dotsR = public.dotsR - public.dotsSpeed * 2
				else
					public.dotsR = 0
				end
			elseif public.state == "standby" or public.state == "tocenter" then
				if public.dotsR < public.maxDotsR then
					public.dotsR = public.dotsR + public.dotsSpeed * 2
				else
					public.dotsR = public.maxDotsR
				end
			end

			public.dotsColor = {
				bgColor[1],
				bgColor[2],
				bgColor[3],
				(public.dotsR/public.maxDotsR)
			}

			for i = 1, 24 do
				local cosAngle = math.cos(math.rad((i - 1) * 15  + public.angle/3))
				local sinAngle = math.sin(math.rad((i - 1) * 15  + public.angle/3))

				local dotX1 = public.x + (public.dotsR)*cosAngle
				local dotY1 = public.y + 5 - (public.dotsR)*sinAngle
				local dotX2 = public.x + (public.dotsR - public.r/20 - 10*(1-public.dotsR/public.maxDotsR))*cosAngle
				local dotY2 = public.y + 5 - (public.dotsR - public.r/20 - 10*(1-public.dotsR/public.maxDotsR))*sinAngle
				love.graphics.setColor(public.dotsColor)
				love.graphics.line(dotX1, dotY1, dotX2, dotY2)
			end
		end

		if (public.level == game.levelsScreen.maxOpenedLevel + 1) and (public.level < 61) then
			local progress
			if game.levelsScreen.temporarySparks > 0 then
				progress = game.levelsScreen.temporarySparks/minimumSparksLevel[game.levelsScreen.maxOpenedLevel]
			else
				progress = 0
			end

			if progress > 0.05 then
				love.graphics.setColor(black10)
				for i = 1, public.blendShiftSmall do
					love.graphics.circle("fill", public.x, public.y + i, public.r * public.sparkSize * progress, 4)
				end
			end
			love.graphics.setColor(yellow)
			love.graphics.circle("line", public.x, public.y, public.r * public.sparkSize, 4)
			
			if progress > 0.05 then
				love.graphics.setColor(yellow)
				love.graphics.circle("fill", public.x, public.y, public.r * public.sparkSize * progress, 4)
				love.graphics.polygon("fill",
					public.x - public.r * public.sparkSize * progress, public.y,
					public.x + public.r * public.sparkSize * progress, public.y,
					public.x + public.r * public.sparkSize * progress, public.y - 5,
					public.x - public.r * public.sparkSize * progress, public.y - 5)
				love.graphics.setColor(lightYellow)
				love.graphics.circle("fill", public.x, public.y - 5, public.r * public.sparkSize * progress, 4)
			end
		end

		if public.level > 0 and public.level < 61 then
			if public.level <= game.levelsScreen.maxOpenedLevel then
				love.graphics.setColor(black10)
				for i = 1, public.blendShift, 4 do
					love.graphics.circle("fill", public.x, public.y + i, public.r, 64)
				end
				
				for i = 1, 10 do
					love.graphics.setColor(sideColor[1] * (1 + i*0.05 - 0.5), sideColor[2] * (1 + i*0.05 - 0.5), sideColor[3] * (1 + i*0.05 - 0.5), sideColor[4])
					love.graphics.circle("fill", public.x, public.y + game.wh/60 - i*game.wh/600, public.r, 64)
				end

				love.graphics.setColor(bgColor)
				love.graphics.circle("fill", public.x, public.y, public.r, 64)
				if public.level == game.levelsScreen.maxOpenedLevel then
					love.graphics.setColor(public.shineColor)
					love.graphics.circle("fill", public.x, public.y, public.r, 64)
				end
				
				local smallArrowSize = public.r*0.1
				local smallArrowSwift = public.r*0.25
				local bigArrowSize = public.r*0.3
				love.graphics.setColor(arrowColor)

				if public.level == 1 or public.level == 11 or public.level == 21
				or public.level == 31 or public.level == 41 or public.level == 51 then
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize, public.x-smallArrowSize, public.y+smallArrowSize, public.x+smallArrowSize, public.y+smallArrowSize)

				elseif public.level == 2 or public.level == 12 or public.level == 22
				or public.level == 32 or public.level == 42 or public.level == 52 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)

				elseif public.level == 3 or public.level == 13 or public.level == 23
				or public.level == 33 or public.level == 43 or public.level == 53 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize, public.x-smallArrowSize, public.y+smallArrowSize, public.x+smallArrowSize, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)

				elseif public.level == 4 or public.level == 14 or public.level == 24
				or public.level == 34 or public.level == 44 or public.level == 54 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)

				elseif public.level == 5 or public.level == 15 or public.level == 25
				or public.level == 35 or public.level == 45 or public.level == 55 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize, public.x-smallArrowSize, public.y+smallArrowSize, public.x+smallArrowSize, public.y+smallArrowSize)

				elseif public.level == 6 or public.level == 16 or public.level == 26
				or public.level == 36 or public.level == 46 or public.level == 56 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)

				elseif public.level == 7 or public.level == 17 or public.level == 27
				or public.level == 37 or public.level == 47 or public.level == 57 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize, public.x-smallArrowSize, public.y+smallArrowSize, public.x+smallArrowSize, public.y+smallArrowSize)

				elseif public.level == 8 or public.level == 18 or public.level == 28
				or public.level == 38 or public.level == 48 or public.level == 58 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize, public.y+smallArrowSize+smallArrowSwift)


				elseif public.level == 9 or public.level == 19 or public.level == 29
				or public.level == 39 or public.level == 49 or public.level == 59 then
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize)
					love.graphics.polygon("fill", public.x-smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize-smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x+smallArrowSwift, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize+smallArrowSwift, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize-smallArrowSwift, public.x-smallArrowSize, public.y+smallArrowSize-smallArrowSwift, public.x+smallArrowSize, public.y+smallArrowSize-smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize+smallArrowSwift, public.x-smallArrowSize, public.y+smallArrowSize+smallArrowSwift, public.x+smallArrowSize, public.y+smallArrowSize+smallArrowSwift)
					love.graphics.polygon("fill", public.x, public.y-smallArrowSize, public.x-smallArrowSize, public.y+smallArrowSize, public.x+smallArrowSize, public.y+smallArrowSize)

				elseif public.level == 10 or public.level == 20 or public.level == 30
				or public.level == 40 or public.level == 50 or public.level == 60 then
					love.graphics.polygon("fill", public.x, public.y-bigArrowSize, public.x-bigArrowSize, public.y+bigArrowSize, public.x+bigArrowSize, public.y+bigArrowSize)
				end
			end

		elseif public.level == 0 then
			if game.levelsScreen.selectedLevel == 0 then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(game.levelsScreen.messagesColor)
				for i = 1, #game.levelsScreen.messages do
					love.graphics.print(game.levelsScreen.messages[i], public.x - (titleFont:getWidth(game.levelsScreen.messages[i]))/2, game.levelsScreen.messagesY + i*game.levelsScreen.messagesMargin)
				end
			end

		elseif public.level == 61 then
			if game.levelsScreen.selectedLevel == 61 then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(game.levelsScreen.creditsColor)
				for i = 1, #game.levelsScreen.credits do
					love.graphics.print(game.levelsScreen.credits[i], public.x - (titleFont:getWidth(game.levelsScreen.credits[i]))/2, game.levelsScreen.creditsY + i*game.levelsScreen.creditsMargin)
				end
			end

		end

		if public.level == 0 then
			love.graphics.setFont(tipFont)
			love.graphics.setColor(game.levelsScreen.messagesColor[1], game.levelsScreen.messagesColor[2], game.levelsScreen.messagesColor[3], 1 - game.levelsScreen.messagesColor[4])
			love.graphics.print(messagesButton, public.x - (tipFont:getWidth(messagesButton)/2), public.y - (tipFont:getHeight(messagesButton)/2))
		end

		if public.level == -1 and game.levelsScreen.maxOpenedLevel > 1 then
			love.graphics.setFont(tipFont)
			love.graphics.setColor(lightRed[1], lightRed[2], lightRed[3], game.levelsScreen.messagesColor[4]/2)
			love.graphics.print(clearProgressButton, public.x - (tipFont:getWidth(clearProgressButton)/2), public.y - (tipFont:getHeight(clearProgressButton)))
		end

	end

	setmetatable(public, self)
	self.__index = self; return public
end