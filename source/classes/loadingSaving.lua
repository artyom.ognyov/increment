serialize = require('source/libraries/ser')

LoadingSaving = {}
function LoadingSaving:new(newFilePath)
	local public = {}
		public.filePath = newFilePath

	function public:load(path)
		--remove
		if forceLevel then
			return forceLevel
		end

		local maxOpenedLevel
		if not love.filesystem.getInfo(path) then
	        maxOpenedLevel = 1
	        public:save(maxOpenedLevel, path)
	    end
	    local data = love.filesystem.load(path)()
	    maxOpenedLevel = data.maxOpenedLevel
	    return maxOpenedLevel
	end

	function public:save(level, path)
		local data = { }
    	data.maxOpenedLevel = level
    	love.filesystem.write(path, serialize(data))
	end

	function public:loadProgress()
		-- if OS == 'Android' or OS == 'iOS' then
		-- 	return {1}
		-- else
			return public:load(public.filePath)
		-- end
	end

	function public:saveProgress()
		-- if OS == 'Android' or OS == 'iOS' then
		-- 	-- no saving for mobile :(
		-- else
			local level = game.levelsScreen.maxOpenedLevel
			public:save(level, public.filePath)
		-- end
	end

	function public:clearProgress()
		-- if OS == 'Android' or OS == 'iOS' then
		-- 	-- no saving for mobile :(
		-- else
			public:save(1, public.filePath)
		-- end
	end

	setmetatable(public, self)
	self.__index = self; return public
end