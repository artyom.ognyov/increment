require 'source/classes/gameObjects'
require 'source/classes/hudObjects'
require 'source/classes/levelScreenObjects'
require 'source/classes/audioClasses'
require 'source/classes/loadingSaving'
require 'source/levels/levelsProperties'
require 'source/utils/text'

local Camera = require 'source/libraries/hump.camera'

Game = {}
function Game:new()
	local public = {}
		public.ww = love.graphics.getWidth()
		public.wh = love.graphics.getHeight()
		public.state = "title" -- "opening" for release
		public.ashes = Ashes:new()
		public.hud = Hud:new()
		public.openingScreen = OpeningScreen:new()
		public.titleScreen = TitleScreen:new()
		public.background = Background:new()
		public.background:setColor(public.titleScreen.backgroundColor)
		public.levelsScreen = LevelsScreen:new()
		public.gameScreen = GameScreen:new()
		public.musicEngine = MusicEngine:new()
		public.sfxEngine = SFXEngine:new()
		public.noise = love.graphics.newImage('source/images/noise.png')

	function public:update()
		if (firstLaunch) then
			firstLaunch = false
			love.window.setMode(1000, 600, {resizable=true, vsync=true, fullscreen=true, minwidth=800, minheight=600, borderless=true, msaa=antialiasing})
			public.ww = love.graphics.getWidth()
			public.wh = love.graphics.getHeight()
			updateFonts()
		end

		public.touches = love.touch.getTouches()

		if public.state ~= "game" then
			public.ashes:update()
		end

		local tempww = love.graphics.getWidth()
		local tempwh = love.graphics.getHeight()
		if (public.ww ~= tempww) or (public.wh ~= tempwh) then
			public.ww = tempww
			public.wh = tempwh
		end
		public.background:update()
		if public.state == "opening" then
			public.openingScreen:update()
		elseif public.state == "title" then
			public.titleScreen:update()
		elseif public.state == "levels" then
			public.levelsScreen:update()
		elseif public.state == "game" then
			public.gameScreen:update()
		end
		public.hud:update()
		public.musicEngine:update()
	end

	function public:keypressed(key)
		if public.state == "opening" then
			public.openingScreen:keypressed(key)
		elseif public.state == "title" then
			public.titleScreen:keypressed(key)
		elseif public.state == "levels" then
			public.levelsScreen:keypressed(key)
		elseif public.state == "game" then
			public.gameScreen:keypressed(key)
		end
		public.hud:keypressed(key)
		if keypressedF11(key) then
			if love.window.getFullscreen() then
				love.window.setMode(1000, 600,
					{resizable=true, vsync=true, minwidth=800, minheight=600, borderless=true, msaa=antialiasing})
				public.ww = love.graphics.getWidth()
				public.wh = love.graphics.getHeight()
				updateFonts()
			else
				love.window.setMode(1000, 600,
					{resizable=true, vsync=true, fullscreen=true, minwidth=800, minheight=600, borderless=true, msaa=antialiasing})
				public.ww = love.graphics.getWidth()
				public.wh = love.graphics.getHeight()
				updateFonts()
			end
		elseif keypressedF12(key) then
			if antialiasing == 0 then
				antialiasing = 1
			else
				antialiasing = 0
			end

			if love.window.getFullscreen() then
				love.window.setMode(1000, 600,
					{resizable=true, vsync=true, fullscreen=true, minwidth=800, minheight=600, borderless=true, msaa=antialiasing})
				public.ww = love.graphics.getWidth()
				public.wh = love.graphics.getHeight()
				updateFonts()
			else
				love.window.setMode(1000, 600,
					{resizable=true, vsync=true, minwidth=800, minheight=600, borderless=true, msaa=antialiasing})
				public.ww = love.graphics.getWidth()
				public.wh = love.graphics.getHeight()
				updateFonts()
			end
		end
	end

	function public:mousepressed(x, y)
		if public.state == "title" then
			public.titleScreen:mousepressed(x, y)
		elseif public.state == "levels" then
			public.levelsScreen:mousepressed(x, y)
		elseif public.state == "game" then
			public.gameScreen:mousepressed(x, y)
		end
	end

	function public:draw()
		public.background:draw()

		if public.state ~= "game" and public.state ~= "opening" then
			public.ashes:draw()
		end

		if public.state == "opening" then
			public.openingScreen:draw()
		elseif public.state == "title" then
			public.titleScreen:draw()
		elseif public.state == "levels" then
			public.levelsScreen:draw()
		elseif public.state == "game" then
			public.gameScreen:draw()
		end

		public.hud:draw()

		love.graphics.setColor(0.75, 0.75, 0.75, 0.25)
		local xShift = math.random()*100
		local yShift = math.random()*100
		love.graphics.draw(public.noise, -xShift, -yShift)
	end

	setmetatable(public, self)
	self.__index = self; return public
end



OpeningScreen = {}
function OpeningScreen:new()
	local public = {}
		public.state = "standby"
		public.animationStep = "in"
		public.transactionSigh = 1
		public.screenOpacity = 1
		public.animationSpeed = 0.05
		public.standbyTimer = 0
		public.standbyLimit = 1
		public.sparkLoading = 0
		public.sparkLoadingMax = 4
		public.sparkLoadingWithFade = public.sparkLoading
		public.sparkAnimationSpeed = 0.05
		public.camera = Camera(0, 0)

		public.ppsLogo = love.graphics.newImage('source/images/pps_1.png')

	function public:update()
		game.hud.mask.state = "opening"
		if public.animationStep == "in" then
			if public.screenOpacity > 0 then
				public.screenOpacity = public.screenOpacity - public.animationSpeed
			else
				public.screenOpacity = 0
				public.standbyTimer = public.standbyLimit
				public.animationStep = "standby"
			end

		elseif public.animationStep == "standby" then
			if public.standbyTimer > 0 then
				public.standbyTimer = public.standbyTimer - public.animationSpeed
			else
				public.standbyTimer = 0
				public.animationStep = "out"
			end

		elseif public.animationStep == "out" then
			if public.screenOpacity < 1 then
				public.screenOpacity = public.screenOpacity + public.animationSpeed

			else
				public.screenOpacity = 1
				public.animationStep = "in"
				if public.state == "standby" then
					public.state = "hello"
					public.standbyLimit = 5
				elseif public.state == "hello" then
					public.state = "pocato"
					public.standbyLimit = 5
				elseif public.state == "pocato" then
					public.state = "controls"
					public.standbyLimit = 10
				elseif public.state == "controls" then
					public.state = "headphones"
					public.standbyLimit = 7
				elseif public.state == "headphones" then
					public.state = "warning"
					public.standbyLimit = 15
				elseif public.state == "warning" then
					public.state = "disclaimer"
					public.standbyLimit = 15
				elseif public.state == "disclaimer" then
					public.state = "quit"
					game.state = "title"
					game.hud.mask.state = "fromopening"
					game.levelsScreen.worldName = getWorldName(game.levelsScreen.maxOpenedLevel)
					game.titleScreen.justFromOpening = true
				end

				public.sparkLoading = public.sparkLoading + 1
			end

		end

		if public.sparkLoadingWithFade < public.sparkLoading then
			public.sparkLoadingWithFade = public.sparkLoadingWithFade + public.sparkAnimationSpeed
		else
			public.sparkLoadingWithFade = public.sparkLoading
		end

		if public.animationStep == "in" then
			public.transactionSigh = -1
		elseif public.animationStep == "out" then
			public.transactionSigh = 1
		else
			public.transactionSigh = 1
		end

		public.camera:lookAt(game.ww/2, game.wh/2)
		if game.ww <= 1300 then
			public.camera.scale = game.ww/1300
		else
			public.camera.scale = 1
		end
	end

	function public:keypressed(key)
		if key then
			if public.state == "standby" then

				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "hello"
				public.standbyLimit = 5
				public.sparkLoading = public.sparkLoading + 1
			elseif public.state == "hello" then
				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "pocato"
				public.standbyLimit = 5
				public.sparkLoading = public.sparkLoading + 1
			elseif public.state == "pocato" then
				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "controls"
				public.standbyLimit = 20
				public.sparkLoading = public.sparkLoading + 1
			elseif public.state == "controls" then
				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "headphones"
				public.standbyLimit = 15
				public.sparkLoading = public.sparkLoading + 1
			elseif public.state == "headphones" then
				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "warning"
				public.standbyLimit = 15
				public.sparkLoading = public.sparkLoading + 1
			elseif public.state == "disclaimer" then
				public.standbyTimer = 0
				public.animationStep = "in"
				public.screenOpacity = 1
				public.state = "quit"
				game.state = "title"
				game.hud.mask.state = "fromopening"
				game.levelsScreen.worldName = getWorldName(game.levelsScreen.maxOpenedLevel)
				game.titleScreen.justFromOpening = true
				public.sparkLoading = public.sparkLoading + 1
			end
		end
	end

	function public:draw()
		public.camera:attach()
			if public.state == "hello" then
				love.graphics.setFont(titleFont)
				local timenow = os.date('*t')
				if timenow.hour >= 0 and timenow.hour < 6 then
					love.graphics.setColor(lightBlue)
					love.graphics.print(ui.goodNight, game.ww/10, game.wh/2 + public.screenOpacity*20*public.transactionSigh)
				elseif timenow.hour >= 6 and timenow.hour < 12 then
					love.graphics.setColor(lightGreen)
					love.graphics.print(ui.goodMorning, game.ww/10, game.wh/2 + public.screenOpacity*20*public.transactionSigh)
				elseif timenow.hour >= 12 and timenow.hour < 18 then
					love.graphics.setColor(lightOrange)
					love.graphics.print(ui.goodDay, game.ww/10, game.wh/2 + public.screenOpacity*20*public.transactionSigh)
				elseif timenow.hour >= 18 and timenow.hour < 24 then
					love.graphics.setColor(lightRed)
					love.graphics.print(ui.goodEvening, game.ww/10, game.wh/2 + public.screenOpacity*20*public.transactionSigh)
				else
					love.graphics.setColor(white)
					love.graphics.print(ui.hello, game.ww/10, game.wh/2 + public.screenOpacity*20*public.transactionSigh)
				end

			elseif public.state == "pocato" then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(white)
				love.graphics.draw(public.ppsLogo, game.ww/10, game.wh/2 - 59 + public.screenOpacity*10*public.transactionSigh)
				love.graphics.print(ui.presents, game.ww/10, game.wh/2 + 60 + public.screenOpacity*20*public.transactionSigh)

			elseif public.state == "controls" then
				love.graphics.setFont(titleFont)
				local buttonSize = titleFontSize
				love.graphics.setColor(white[1], white[2], white[3], 0.05)
				love.graphics.rectangle("line", game.ww/2 - titleFontSize*3.5, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.rectangle("line", game.ww/2 - titleFontSize*3.5, game.wh/2 - titleFontSize + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)

				love.graphics.setColor(white)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*2, game.wh/2 + titleFontSize*0.6 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.setColor(darkColdGrey)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*2, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.setColor(white)
				love.graphics.rectangle("line", game.ww/2 - titleFontSize*2, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.print(">", game.ww/2 - titleFontSize*1.5 - titleFont:getWidth(">")/2, game.wh/2 + titleFontSize*0.25 + public.screenOpacity*20*public.transactionSigh)

				love.graphics.setColor(white)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*5, game.wh/2 + titleFontSize*0.6 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.setColor(darkColdGrey)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*5, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.setColor(white)
				love.graphics.rectangle("line", game.ww/2 - titleFontSize*5, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*20*public.transactionSigh, buttonSize, buttonSize, game.wh/60)
				love.graphics.print("<", game.ww/2 - titleFontSize*4.5 - titleFont:getWidth("<")/2, game.wh/2 + titleFontSize*0.25 + public.screenOpacity*20*public.transactionSigh)

				love.graphics.setColor(white)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*6, game.wh/2 - titleFontSize*2.4 + public.screenOpacity*10*public.transactionSigh, buttonSize * 2, buttonSize, game.wh/60)
				love.graphics.setColor(darkColdGrey)
				love.graphics.rectangle("fill", game.ww/2 - titleFontSize*6, game.wh/2 - titleFontSize*2.5 + public.screenOpacity*10*public.transactionSigh, buttonSize * 2, buttonSize, game.wh/60)
				love.graphics.setColor(white)
				love.graphics.rectangle("line", game.ww/2 - titleFontSize*6, game.wh/2 - titleFontSize*2.5 + public.screenOpacity*10*public.transactionSigh, buttonSize * 2, buttonSize, game.wh/60)
				love.graphics.print("ESC", game.ww/2 - titleFontSize*5 - titleFont:getWidth("ESC")/2, game.wh/2 - titleFontSize*2.75 + public.screenOpacity*20*public.transactionSigh)

				love.graphics.setFont(titleFont)
				love.graphics.setColor(white)
				love.graphics.print('+', game.ww/2, game.wh/2 + public.screenOpacity*15*public.transactionSigh)
				love.graphics.print(ui.anyComfortable, game.ww/2 + 100, game.wh/2 - titleFontSize/2 + public.screenOpacity*10*public.transactionSigh)
				love.graphics.print(ui.key, game.ww/2 + 100, game.wh/2 + titleFontSize/2 + public.screenOpacity*20*public.transactionSigh)

			elseif public.state == "headphones" then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(white)
				love.graphics.print(ui.useYourHeadphones, game.ww/10, game.wh/2 - titleFontSize/2 + public.screenOpacity*10*public.transactionSigh)
				love.graphics.print(ui.ifYouWant, game.ww/10, game.wh/2 + titleFontSize/2 + public.screenOpacity*20*public.transactionSigh)

			elseif public.state == "warning" then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(lightRed)

				love.graphics.print(ui.warning[1], game.ww/10, game.wh/2 - titleFontSize*2.5 + public.screenOpacity*0.625*public.transactionSigh)
				love.graphics.print(ui.warning[2], game.ww/10, game.wh/2 - titleFontSize*1.5 + public.screenOpacity*1.25*public.transactionSigh)
				love.graphics.print(ui.warning[3], game.ww/10, game.wh/2 - titleFontSize*0.5 + public.screenOpacity*2.5*public.transactionSigh)
				love.graphics.print(ui.warning[4], game.ww/10, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*5*public.transactionSigh)
				love.graphics.print(ui.warning[5], game.ww/10, game.wh/2 + titleFontSize*1.5 + public.screenOpacity*10*public.transactionSigh)
				love.graphics.print(ui.warning[6], game.ww/10, game.wh/2 + titleFontSize*2.5 + public.screenOpacity*20*public.transactionSigh)

			elseif public.state == "disclaimer" then
				love.graphics.setFont(titleFont)
				love.graphics.setColor(white)
				love.graphics.print(ui.disclaimer[1], game.ww/10, game.wh/2 - titleFontSize*2.5 + public.screenOpacity*0.625*public.transactionSigh)
				love.graphics.print(ui.disclaimer[2], game.ww/10, game.wh/2 - titleFontSize*1.5 + public.screenOpacity*1.25*public.transactionSigh)
				love.graphics.print(ui.disclaimer[3], game.ww/10, game.wh/2 - titleFontSize*0.5 + public.screenOpacity*2.5*public.transactionSigh)
				love.graphics.print(ui.disclaimer[4], game.ww/10, game.wh/2 + titleFontSize*0.5 + public.screenOpacity*5*public.transactionSigh)
				love.graphics.print(ui.disclaimer[5], game.ww/10, game.wh/2 + titleFontSize*1.5 + public.screenOpacity*10*public.transactionSigh)
				love.graphics.print(ui.disclaimer[6], game.ww/10, game.wh/2 + titleFontSize*2.5 + public.screenOpacity*20*public.transactionSigh)

			end

		public.camera:detach()

		if public.state ~= "standby" then
			love.graphics.setColor(yellow)
			love.graphics.circle('line', game.ww - game.wh/15, game.wh/15, game.wh/40, 4)
			love.graphics.circle('fill', game.ww - game.wh/15, game.wh/15, game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, 4)
			love.graphics.polygon('fill',
				game.ww - game.wh/15 - game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, game.wh/15,
				game.ww - game.wh/15 + game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, game.wh/15,
				game.ww - game.wh/15 + game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, game.wh/16,
				game.ww - game.wh/15 - game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, game.wh/16)
			love.graphics.setColor(lightYellow)
			love.graphics.circle('fill', game.ww - game.wh/15, game.wh/16, game.wh/40 * public.sparkLoadingWithFade/public.sparkLoadingMax, 4)
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



TitleScreen = {}
function TitleScreen:new()
	local public = {}
		public.state = "fromopening"
		public.quitingTimer = 0
		public.quitingSpeed = 0.5
		public.screenOpacity = 1
		public.animationSpeed = 0.025
		public.justFromOpening = true
		public.backgroundColor = {}
		public.backgroundColor[1] = darkColdGrey[1]
		public.backgroundColor[2] = darkColdGrey[2]
		public.backgroundColor[3] = darkColdGrey[3]

	function public:update()
		if public.state == "waiting" then
			-- no code

		elseif public.state == "quiting" then
			if public.quitingTimer > 0 then
				public.quitingTimer = public.quitingTimer - public.quitingSpeed
			else
				love.event.quit()
			end

		elseif public.state == "tolevels" then
			-- no code

		elseif public.state == "fromlevels" then
			public.state = "waiting"
			game.hud.titleTriangle.state = "toup"
			game.hud.menuButton.state = "todownIN"
			game.background:setColorTween(public.backgroundColor)

		elseif public.state == "fromopening" then
			game.hud.mask.state = "fromopening"
			if public.screenOpacity > 0 then
				public.screenOpacity = public.screenOpacity - public.animationSpeed
			else
				public.screenOpacity = 0
				public.state = "waiting"
				game.hud.mask.state = "standby"
			end
		end

	end

	function public:keypressed(key)
		if public.state == "waiting" then
			if keypressedEscape(key) and (game.hud.titleTriangle.state == "standby") then
				public.state = "quiting"
				game.hud.quitingText.state = "shown"
				public.quitingTimer = 100
				game.hud.menuButton.state = "todropdown"
				game.hud.dropdown.state = "toshown"
				game.sfxEngine:play("quitIN")
			elseif keypressedAnyNotEscape(key)
			and (game.hud.titleTriangle.state == "standby")
			and (game.hud.menuButton.state == "down") then
				public.state = "tolevels"
				game.hud.titleTriangle.state = "todown"
				game.hud.menuButton.state = "toupIN"
				game.levelsScreen.state = "justfromtitle"
				game.levelsScreen.nextWorldName = getWorldName(game.levelsScreen.maxOpenedLevel)
				game.levelsScreen.worldNameState = "out"
				if public.justFromOpening then
					public.justFromOpening = false
					game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
				else
					game.musicEngine:updateTrack(game.levelsScreen.maxOpenedLevel)
				end
				game.sfxEngine:play("switchLevel")
			end
		elseif public.state == "quiting" then
			if (key) then
				public.state = "waiting"
				game.hud.quitingText.state = "hidden"
				public.quitingTimer = 0
				game.hud.menuButton.state = "fromdropdown"
				game.hud.dropdown.state = "fromshown"
				game.sfxEngine:play("cancel")
			end
		end
	end

	function public:mousepressed(x, y)
		if public.state == "waiting" then
			if isTouched(x, y, 0, 0, 75, 75)
			and (game.hud.titleTriangle.state == "standby")
			and (game.hud.menuButton.state == "down") then
				public.state = "quiting"
				public.quitingTimer = 100
				game.hud.menuButton.state = "todropdown"
				game.hud.dropdown.state = "toshown"
				game.sfxEngine:play("quitIN")
			elseif isTouched(x, y, 0, 0, game.ww, game.wh) then
				public.state = "tolevels"
				game.hud.titleTriangle.state = "todown"
				game.hud.menuButton.state = "toupIN"
				game.levelsScreen.state = "justfromtitle"
				game.levelsScreen.nextWorldName = getWorldName(game.levelsScreen.maxOpenedLevel)
				game.levelsScreen.worldNameState = "out"
				if public.justFromOpening then
					public.justFromOpening = false
					game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
				else
					game.musicEngine:updateTrack(game.levelsScreen.maxOpenedLevel)
				end
			end
		elseif public.state == "quiting" then
			if isTouched(x, y, 0, 0, game.ww, game.wh) then
				public.state = "waiting"
				public.quitingTimer = 0
				game.hud.menuButton.state = "fromdropdown"
				game.hud.dropdown.state = "fromshown"
				game.sfxEngine:play("cancel")
			end
		end
	end

	function public:draw() end

	setmetatable(public, self)
	self.__index = self; return public
end



LevelsScreen = {}
function LevelsScreen:new()
	local public = {}
		public.loadingSaving = LoadingSaving:new('save.ini')
		local loading = public.loadingSaving:loadProgress()
		public.maxOpenedLevel = tonumber(loading)
		public.titleTriangle = TitleTriangle:new()
		public.titleTriangle.state = "down"
		public.state = "fromtitle"
		public.selectedLevel = public.maxOpenedLevel
		public.prevSelectedLevel = 0
		public.numberOfLevels = 1
		public.temporarySparks = 0
		public.sparksBarAnimationSpeed = 0.1
		public.reload = false
		public.screenOpacity = 0
		public.leftIcon   = LevelIcon:new(1)
		public.centerIcon = LevelIcon:new(2)
		public.rightIcon  = LevelIcon:new(3)
		public.temporaryIcon = LevelIcon:new(0)
		public.rotateDirection = -1
		public.worldName = ""
		public.nextWorldName = ""
		public.worldNameState = "standby"
		public.worldNameColor = {}
		public.worldNameColor[1] = white[1]
		public.worldNameColor[2] = white[2]
		public.worldNameColor[3] = white[3]
		public.worldNameColor[4] = 0
		public.worldNameAnimationSpeed = 0.1
		public.finalLevelColor = {}
		public.finalLevelColor[1] = darkColdGrey[1]
		public.finalLevelColor[2] = darkColdGrey[2]
		public.finalLevelColor[3] = darkColdGrey[3]
		public.finalLevelColor[4] = 0
		public.finalLevelColorAnimationSpeed = 0.05
		public.messages = messages
		public.messagesState = "hidden"
		public.messagesY = 0
		public.messagesMargin = 75
		public.messagesColor = {}
		public.messagesColor[1] = white[1]
		public.messagesColor[2] = white[2]
		public.messagesColor[3] = white[3]
		public.messagesColor[4] = 0
		public.messagesColorAnimationSpeed = 0.1
		public.messagesScrollAnimationSpeed = 1
		public.credits = credits
		public.creditsState = "hidden"
		public.creditsY = 0
		public.creditsMargin = 75
		public.creditsColor = {}
		public.creditsColor[1] = darkColdGrey[1]
		public.creditsColor[2] = darkColdGrey[2]
		public.creditsColor[3] = darkColdGrey[3]
		public.creditsColor[4] = 0
		public.creditsColorAnimationSpeed = 0.1
		public.creditsScrollAnimationSpeed = 1

	function public:getMaxOpenedLevel()
		for i = 1, public.numberOfLevels do
			if (i < 60) and (maximumSparksLevel[i] >= minimumSparksLevel[i]) then
				public.maxOpenedLevel = i+1
			end
		end
	end

	function public:update()
		public.leftIcon:update()
		public.centerIcon:update()
		public.rightIcon:update()
		public.temporaryIcon:update()

		public.messagesScrollAnimationSpeed = game.wh/600
		public.creditsScrollAnimationSpeed = game.wh/600
		public.messagesMargin = tipFontSize
		public.creditsMargin = tipFontSize

		if public.state == "waiting" then
			public.leftIcon.x = game.ww/2 - game.ww/public.leftIcon.distance
			public.leftIcon.y = game.wh/2
			public.leftIcon.r = love.graphics.getWidth()/public.leftIcon.rSize

			public.centerIcon.x = game.ww/2
			public.centerIcon.y = game.wh/2
			public.centerIcon.r = love.graphics.getWidth()/public.centerIcon.rSize

			public.rightIcon.x = game.ww/2 + game.ww/public.rightIcon.distance
			public.rightIcon.y = game.wh/2
			public.rightIcon.r = love.graphics.getWidth()/public.rightIcon.rSize

			public.temporaryIcon.x = game.ww/2 + game.ww/public.temporaryIcon.distance
			public.temporaryIcon.y = game.wh/2
			public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize


		elseif public.state == "totitle" then
			if public.centerIcon.y+public.centerIcon.r > 0 then
				public.leftIcon.y = public.leftIcon.y - public.leftIcon.animationSpeed
				public.centerIcon.y = public.centerIcon.y - public.centerIcon.animationSpeed
				public.rightIcon.y = public.rightIcon.y - public.rightIcon.animationSpeed
			else
				game.state = "title"
				game.titleScreen.state = "fromlevels"
				game.musicEngine:setVolume(0.2)
				public.worldName = ""
				public.worldNameState = "out"
				public.messagesState = "tohide"
			end

		elseif public.state == "nextselection" then
			if public.temporaryIcon.state ~= "standby"
			or public.leftIcon.state ~= "standby"
			or public.centerIcon.state ~= "standby"
			or public.rightIcon.state ~= "standby" then
				public.leftIcon:update()
				public.centerIcon:update()
				public.rightIcon:update()
				public.temporaryIcon:update()
			else
				public.state = "waiting"
				public.selectedLevel = public.selectedLevel - 1
				public.leftIcon   = nil
				public.centerIcon = nil
				public.rightIcon  = nil
				public.leftIcon   = LevelIcon:new(public.selectedLevel-1)
				public.centerIcon = LevelIcon:new(public.selectedLevel)
				public.rightIcon  = LevelIcon:new(public.selectedLevel+1)
				public.leftIcon.x = game.ww/2 - game.ww/public.leftIcon.distance
				public.centerIcon.x = game.ww/2
				public.rightIcon.x = game.ww/2 + game.ww/public.rightIcon.distance
				if public.selectedLevel == 10
				or public.selectedLevel == 20
				or public.selectedLevel == 30
				or public.selectedLevel == 40
				or public.selectedLevel == 50
				then
					public.nextWorldName = getWorldName(public.selectedLevel)
					public.worldNameState = "out"
				end
			end


		elseif public.state == "prevselection" then
			if public.temporaryIcon.state ~= "standby"
			or public.leftIcon.state ~= "standby"
			or public.centerIcon.state ~= "standby"
			or public.rightIcon.state ~= "standby" then
				public.leftIcon:update()
				public.centerIcon:update()
				public.rightIcon:update()
				public.temporaryIcon:update()
			else
				public.state = "waiting"
				public.selectedLevel = public.selectedLevel + 1
				public.leftIcon   = nil
				public.centerIcon = nil
				public.rightIcon  = nil
				public.leftIcon   = LevelIcon:new(public.selectedLevel-1)
				public.centerIcon = LevelIcon:new(public.selectedLevel)
				public.rightIcon  = LevelIcon:new(public.selectedLevel+1)
				public.leftIcon.x = game.ww/2 - game.ww/public.leftIcon.distance
				public.centerIcon.x = game.ww/2
				public.rightIcon.x = game.ww/2 + game.ww/public.rightIcon.distance
				if public.selectedLevel == 11
				or public.selectedLevel == 21
				or public.selectedLevel == 31
				or public.selectedLevel == 41
				or public.selectedLevel == 51
				then
					public.nextWorldName = getWorldName(public.selectedLevel)
					public.worldNameState = "out"
				end
			end

		elseif public.state == "justfromtitle" then
			public.selectedLevel = public.maxOpenedLevel
			if public.selectedLevel > 60 then public.selectedLevel = 60 end
			game.background:setColorByTome(public.selectedLevel)
			public.leftIcon   = LevelIcon:new(public.selectedLevel-1)
			public.centerIcon = LevelIcon:new(public.selectedLevel)
			public.rightIcon  = LevelIcon:new(public.selectedLevel+1)
			public.leftIcon.x = game.ww/2 - game.ww/public.leftIcon.distance
			public.centerIcon.x = game.ww/2
			public.rightIcon.x = game.ww/2 + game.ww/public.rightIcon.distance
			public.leftIcon.y = -public.leftIcon.r
			public.centerIcon.y = -public.centerIcon.r
			public.rightIcon.y = -public.rightIcon.r
			public.state = "fromtitle"

		elseif public.state == "fromtitle" then
			if public.centerIcon.y < game.wh/2 then
				public.leftIcon.y = public.leftIcon.y + public.leftIcon.animationSpeed
				public.centerIcon.y = public.centerIcon.y + public.centerIcon.animationSpeed
				public.rightIcon.y = public.rightIcon.y + public.rightIcon.animationSpeed
			else
				public.state = "waiting"
			end

		elseif public.state == "togame" then
			if public.centerIcon.y - public.centerIcon.r < game.wh then
				public.leftIcon.y = public.leftIcon.y + public.leftIcon.animationSpeed
				public.centerIcon.y = public.centerIcon.y + public.centerIcon.animationSpeed
				public.rightIcon.y = public.rightIcon.y + public.rightIcon.animationSpeed
			else
				game.state = "game"
				game.hud.mask.state = "fadein"
				game.hud.mask.color[4] = 1
				if public.selectedLevel == 1
				or public.selectedLevel == 11
				or public.selectedLevel == 21
				or public.selectedLevel == 31
				or public.selectedLevel == 41
				or public.selectedLevel == 51 then
					if public.selectedLevel == public.maxOpenedLevel and maximumSparksLevel[public.selectedLevel] <= 0 then
						local chapter
						if public.selectedLevel == 1 then chapter = 1
						elseif public.selectedLevel == 11 then chapter = 2
						elseif public.selectedLevel == 21 then chapter = 3
						elseif public.selectedLevel == 31 then chapter = 4
						elseif public.selectedLevel == 41 then chapter = 5
						elseif public.selectedLevel == 51 then chapter = 6
						end
						game.gameScreen.chapter = chapter
						game.gameScreen.chapterPrologue:setChapter(chapter)
					end
				end

				if public.selectedLevel == public.maxOpenedLevel then
					public.temporarySparks = maximumSparksLevel[public.selectedLevel]
				end
				game.hud.webs:toDefault()
				game.gameScreen.collisionCount = 0
				game.gameScreen.cameraAngle = angleLevel[public.selectedLevel]
				local playerX
				local playerY
				game.gameScreen.level = public.selectedLevel
				game.gameScreen.world, playerX, playerY = arrayToLevel(levels[game.gameScreen.level])
				game.gameScreen.player = Player:new(playerX, playerY)
				game.gameScreen.cameraMovementZoom = 2
				game.gameScreen.camera = Camera(playerX, playerY)
				game.gameScreen.sparksCount = 0
				game.gameScreen.mainColor, game.gameScreen.lightColor, game.gameScreen.darkColor = getColorsFromLevel(public.selectedLevel)
				game.gameScreen.player.dotsColor[1] = game.gameScreen.lightColor[1]
				game.gameScreen.player.dotsColor[2] = game.gameScreen.lightColor[2]
				game.gameScreen.player.dotsColor[3] = game.gameScreen.lightColor[3]
			end

		elseif public.state == "justfromgame" then
			public.prevSelectedLevel = public.selectedLevel
			public.selectedLevel = public.maxOpenedLevel
			game.background:setColorByTome(public.selectedLevel)
			public.leftIcon   = LevelIcon:new(public.selectedLevel-1)
			public.centerIcon = LevelIcon:new(public.selectedLevel)
			public.rightIcon  = LevelIcon:new(public.selectedLevel+1)
			public.leftIcon.x = game.ww/2 - game.ww/public.leftIcon.distance
			public.centerIcon.x = game.ww/2
			public.rightIcon.x = game.ww/2 + game.ww/public.rightIcon.distance
			public.leftIcon.y = game.wh+public.leftIcon.r
			public.centerIcon.y = game.wh+public.centerIcon.r
			public.rightIcon.y = game.wh+public.rightIcon.r
			public.state = "fromgame"
			game.hud.pressAnyKey.state = "hidden"
			game.hud.pressAnyKey.timer = 0

		elseif public.state == "fromgame" then
			game.hud.mask.state = "fromgame"
			if public.centerIcon.y > game.wh/2 then
				public.leftIcon.y = public.leftIcon.y - public.leftIcon.animationSpeed
				public.centerIcon.y = public.centerIcon.y - public.centerIcon.animationSpeed
				public.rightIcon.y = public.rightIcon.y - public.rightIcon.animationSpeed
				public.screenOpacity = (public.centerIcon.y - game.wh/2)/(game.wh/2)
			else
				game.hud.mask.state = "standby"
				if (public.temporarySparks < maximumSparksLevel[public.selectedLevel])
				and (public.prevSelectedLevel == public.maxOpenedLevel) then
					public.state = "sparksupdate"
					public.leftIcon.y = game.wh/2
					public.centerIcon.y = game.wh/2
					public.rightIcon.y = game.wh/2
				else
					public.state = "waiting"
				end
			end

		elseif public.state == "sparksupdate" then
			if (public.temporarySparks < maximumSparksLevel[public.selectedLevel])
			and (public.temporarySparks < minimumSparksLevel[public.selectedLevel]) then
				sparksBarAnimationSpeed = 0.1 + minimumSparksLevel[public.selectedLevel] * (minimumSparksLevel[public.selectedLevel] - public.temporarySparks)
				public.temporarySparks = public.temporarySparks + public.sparksBarAnimationSpeed
			else
				if (maximumSparksLevel[public.selectedLevel] >= minimumSparksLevel[public.selectedLevel]) then
					public.temporarySparks = 0
					public.state = "prevselection"
					game.background:setColorByTome(public.selectedLevel + 1)
					public.temporaryIcon = LevelIcon:new(public.selectedLevel+2)
					public.temporaryIcon.x = game.ww + public.temporaryIcon.r
					public.temporaryIcon.y = game.wh/2
					public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
					public.temporaryIcon.state = "fromscreenright"
					public.leftIcon.state = "outscreenleft"
					public.centerIcon.state = "toleft"
					public.rightIcon.state = "tocenter"
					public.maxOpenedLevel = public.selectedLevel + 1
					if public.maxOpenedLevel == 11
						or public.maxOpenedLevel == 21
						or public.maxOpenedLevel == 31
						or public.maxOpenedLevel == 41
						or public.maxOpenedLevel == 51
						or public.maxOpenedLevel == 61
					then
						game.musicEngine:nextTrack(public.maxOpenedLevel)
						public.nextWorldName = getWorldName(public.maxOpenedLevel)
						public.worldNameState = "out"
						public.creditsY = game.wh - public.creditsMargin
						public.creditsState = "scroll"
					else
						game.musicEngine:updateTrack(public.maxOpenedLevel)
					end
					game.sfxEngine:play("levelUnlock")
					public.loadingSaving:saveProgress()
				else
					public.state = "waiting"
				end
			end

		end

		if public.worldNameState == "out" then
			if public.worldNameColor[4] > 0 then
				public.worldNameColor[4] = public.worldNameColor[4] - public.worldNameAnimationSpeed
			else
				public.worldNameColor[4] = 0
				public.worldNameState = "in"
				public.worldName = public.nextWorldName
			end

		elseif public.worldNameState == "in" then
			if public.selectedLevel > 50 then
				public.worldNameColor[1] = darkColdGrey[1]
				public.worldNameColor[2] = darkColdGrey[2]
				public.worldNameColor[3] = darkColdGrey[3]
			else
				public.worldNameColor[1] = white[1]
				public.worldNameColor[2] = white[2]
				public.worldNameColor[3] = white[3]
			end

			if public.worldNameColor[4] < 1 then
				public.worldNameColor[4] = public.worldNameColor[4] + public.worldNameAnimationSpeed
			else
				public.worldNameColor[4] = 1
				public.worldNameState = "standby"
			end

		end

		if public.messagesState == "hidden" then
			--no code
		elseif public.messagesState == "scroll" then
			if public.messagesColor[4] < 1 then
				public.messagesColor[4] = public.messagesColor[4] + public.messagesColorAnimationSpeed
			end
			love.graphics.setColor(black)
			if public.messagesY > -(#game.levelsScreen.messages + 1)*game.levelsScreen.messagesMargin then
				public.messagesY = public.messagesY - public.messagesScrollAnimationSpeed
			end
		elseif public.messagesState == "tohide" then
			if public.messagesColor[4] > 0 then
				public.messagesColor[4] = public.messagesColor[4] - public.messagesColorAnimationSpeed
			else
				public.messagesColor[4] = 0
				public.messagesState = "hidden"
			end
		end

		if public.creditsState == "hidden" then
			--no code
		elseif public.creditsState == "scroll" then
			if public.creditsColor[4] < 1 then
				public.creditsColor[4] = public.creditsColor[4] + public.creditsColorAnimationSpeed
			end
			if public.creditsY > -(#game.levelsScreen.credits + 1)*game.levelsScreen.creditsMargin then
				public.creditsY = public.creditsY - public.creditsScrollAnimationSpeed
			end
		elseif public.creditsState == "tohide" then
			if public.creditsColor[4] > 0 then
				public.creditsColor[4] = public.creditsColor[4] - public.creditsColorAnimationSpeed
			else
				public.creditsColor[4] = 0
				public.creditsState = "hidden"
			end
		end

		if (public.selectedLevel == 10
		or public.selectedLevel == 20
		or public.selectedLevel == 30
		or public.selectedLevel == 40
		or public.selectedLevel == 50)
		and public.selectedLevel == public.maxOpenedLevel then
			if public.finalLevelColor[4] < 0.95 then
				public.finalLevelColor[4] = public.finalLevelColor[4] + public.finalLevelColorAnimationSpeed
			else
				public.finalLevelColor[4] = 0.95
			end
		else
			if public.finalLevelColor[4] > 0 then
				public.finalLevelColor[4] = public.finalLevelColor[4] - public.finalLevelColorAnimationSpeed
			else
				public.finalLevelColor[4] = 0
			end
		end

	end

	function public:keypressed(key)
		if public.state == "waiting" then
			-- escape clearProgressSplash
			if (key) and (game.hud.clearProgressSplash.state == "shown") then
				public.state = "prevselection"
				game.background:setColorByTome(public.selectedLevel + 1)
				public.temporaryIcon = LevelIcon:new(public.selectedLevel+2)
				public.temporaryIcon.x = game.ww + public.temporaryIcon.r
				public.temporaryIcon.y = game.wh/2
				public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
				public.temporaryIcon.state = "fromscreenright"
				public.leftIcon.state = "outscreenleft"
				public.centerIcon.state = "toleft"
				public.rightIcon.state = "tocenter"
				game.hud.clearProgressSplash.state = "tohidden"
				public.messagesY = game.wh - public.messagesMargin
				public.messagesState = "scroll"
				public.nextWorldName = ""
				public.worldNameState = "out"
				game.sfxEngine:play("switchLevel")
			end
			-- escape to title
			if (keypressedEscape(key))
			and (game.hud.titleTriangle.state == "down") then
				public.state = "totitle"
				game.titleScreen.state = "waiting"
				public.nextWorldName = ""
				public.worldNameState = "out"
				game.sfxEngine:play("cancel")
			end
			-- scroll left
			if (keypressedLeft(key))
			and ((public.selectedLevel > 0)
			or ((public.selectedLevel > -1)
			and (public.maxOpenedLevel > 1))) then
				public.state = "nextselection"
				game.background:setColorByTome(public.selectedLevel - 1)
				public.temporaryIcon = LevelIcon:new(public.selectedLevel-2)
				public.temporaryIcon.x = -public.temporaryIcon.r
				public.temporaryIcon.y = game.wh/2
				public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
				public.temporaryIcon.state = "fromscreenleft"
				public.leftIcon.state = "tocenter"
				public.centerIcon.state = "toright"
				public.rightIcon.state = "outscreenright"
				local levelChangedTo = public.selectedLevel - 1
				if levelChangedTo == 10
				or levelChangedTo == 20
				or levelChangedTo == 30
				or levelChangedTo == 40
				or levelChangedTo == 50
				or levelChangedTo == 60
				or levelChangedTo == 70
				or levelChangedTo == 80
				or levelChangedTo == 90 then
					game.musicEngine:nextTrack(levelChangedTo)
				end
				if public.selectedLevel == 61 then
					public.creditsState = "tohide"
					public.nextWorldName = worldNames[6]
					public.worldNameState = "out"
				end
				if public.selectedLevel == 1 then
					public.messagesY = game.wh - public.messagesMargin
					public.messagesState = "scroll"
					public.nextWorldName = ""
					public.worldNameState = "out"
				end
				if public.selectedLevel == 0 then
					game.hud.clearProgressSplash.state = "toshown"
					public.messagesState = "tohide"
				end
				game.sfxEngine:play("switchLevel")
				public.rotateDirection = -1
			end
			-- scroll right
			if (keypressedRight(key)) and ((public.selectedLevel < public.maxOpenedLevel)
				or (public.selectedLevel == 60)) then
				public.state = "prevselection"
				game.background:setColorByTome(public.selectedLevel + 1)
				public.temporaryIcon = LevelIcon:new(public.selectedLevel+2)
				public.temporaryIcon.x = game.ww + public.temporaryIcon.r
				public.temporaryIcon.y = game.wh/2
				public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
				public.temporaryIcon.state = "fromscreenright"
				public.leftIcon.state = "outscreenleft"
				public.centerIcon.state = "toleft"
				public.rightIcon.state = "tocenter"
				local levelChangedTo = public.selectedLevel + 1
				if levelChangedTo == 11
				or levelChangedTo == 21
				or levelChangedTo == 31
				or levelChangedTo == 41
				or levelChangedTo == 51
				or levelChangedTo == 61
				or levelChangedTo == 71
				or levelChangedTo == 81
				or levelChangedTo == 91 then
					if game.levelsScreen.maxOpenedLevel - levelChangedTo > 9 then
						game.musicEngine:nextTrack(levelChangedTo + 9)
					else
						game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
					end
				end
				if public.selectedLevel == 0 then
					public.messagesState = "tohide"
					public.nextWorldName = worldNames[1]
					public.worldNameState = "out"
				end
				if public.selectedLevel == 60 then
					public.creditsY = game.wh - public.creditsMargin
					public.creditsState = "scroll"
					public.nextWorldName = ""
					public.worldNameState = "out"
				end
				game.sfxEngine:play("switchLevel")
				public.rotateDirection = 1
			end
			-- enter to game
			if (keypressedAnyNotLeftRightEscape(key)) and public.selectedLevel > 0 and public.selectedLevel < 61 then
				game.sfxEngine:play("start")
				public.state = "togame"
				game.hud.menuButton.state = "todownIN"
				public.nextWorldName = ""
				public.worldNameState = "out"
			end
		end
	end

	function public:mousepressed(x, y)
		if public.state == "waiting" then
			-- escape to title
			if isTouched(x, y, 0, 0, 75, 75) and (game.hud.titleTriangle.state == "down") then
				public.state = "totitle"
				game.titleScreen.state = "waiting"
				public.nextWorldName = ""
				public.worldNameState = "out"
				game.sfxEngine:play("cancel")

			-- scroll left
			elseif isTouched(x, y,
			public.leftIcon.x - public.leftIcon.r, public.leftIcon.y - public.leftIcon.r,
			public.leftIcon.r*2, public.leftIcon.r*2)
			and (public.selectedLevel > 1) then
				if (public.selectedLevel > 0) then
					public.state = "nextselection"
					game.background:setColorByTome(public.selectedLevel - 1)
					public.temporaryIcon = LevelIcon:new(public.selectedLevel-2)
					public.temporaryIcon.x = -public.temporaryIcon.r
					public.temporaryIcon.y = game.wh/2
					public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
					public.temporaryIcon.state = "fromscreenleft"
					public.leftIcon.state = "tocenter"
					public.centerIcon.state = "toright"
					public.rightIcon.state = "outscreenright"
					local levelChangedTo = public.selectedLevel - 1
					if levelChangedTo == 10
					or levelChangedTo == 20
					or levelChangedTo == 30
					or levelChangedTo == 40
					or levelChangedTo == 50
					or levelChangedTo == 60
					or levelChangedTo == 70
					or levelChangedTo == 80
					or levelChangedTo == 90 then
						game.musicEngine:nextTrack(levelChangedTo)
					end
					game.sfxEngine:play("switchLevel")
					public.rotateDirection = -1
				end

			-- scroll right
			elseif isTouched(x, y,
			public.rightIcon.x - public.rightIcon.r, public.rightIcon.y - public.rightIcon.r,
			public.rightIcon.r*2, public.rightIcon.r*2)
			and (public.selectedLevel < public.maxOpenedLevel) then
				if (public.selectedLevel < public.maxOpenedLevel) then
					public.state = "prevselection"
					game.background:setColorByTome(public.selectedLevel + 1)
					public.temporaryIcon = LevelIcon:new(public.selectedLevel+2)
					public.temporaryIcon.x = game.ww + public.temporaryIcon.r
					public.temporaryIcon.y = game.wh/2
					public.temporaryIcon.r = love.graphics.getWidth()/public.temporaryIcon.rSize
					public.temporaryIcon.state = "fromscreenright"
					public.leftIcon.state = "outscreenleft"
					public.centerIcon.state = "toleft"
					public.rightIcon.state = "tocenter"
					local levelChangedTo = public.selectedLevel + 1
					if levelChangedTo == 11
					or levelChangedTo == 21
					or levelChangedTo == 31
					or levelChangedTo == 41
					or levelChangedTo == 51
					or levelChangedTo == 61
					or levelChangedTo == 71
					or levelChangedTo == 81
					or levelChangedTo == 91 then
						if game.levelsScreen.maxOpenedLevel - levelChangedTo > 9 then
							game.musicEngine:nextTrack(levelChangedTo + 9)
						else
							game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
						end
					end
					game.sfxEngine:play("switchLevel")
					public.rotateDirection = 1
				end

			-- enter the game
			elseif public.selectedLevel > 0 then
				game.sfxEngine:play("start")
				public.state = "togame"
				game.hud.menuButton.state = "todownIN"

			end
		end
	end

	function public:draw()
		love.graphics.setColor(black[1], black[2], black[3], game.levelsScreen.messagesColor[4]*0.75)
		love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
		if public.state == "waiting"
		or public.state == "totitle"
		or public.state == "fromtitle"
		or public.state == "togame"
		or public.state == "fromgame"
		or public.state == "sparksupdate" then
			public.leftIcon:draw()
			public.rightIcon:draw()
			public.centerIcon:draw()
		elseif public.state == "nextselection" then
			public.centerIcon:draw()
			public.temporaryIcon.draw()
			public.rightIcon:draw()
			public.leftIcon:draw()
		elseif public.state == "prevselection" then
			public.centerIcon:draw()
			public.temporaryIcon.draw()
			public.leftIcon:draw()
			public.rightIcon:draw()
		elseif public.state == "totitle" or public.state == "fromtitle" then
			-- no code
		end

		love.graphics.setFont(titleFont)
		love.graphics.setColor(public.worldNameColor)
		love.graphics.print(public.worldName,
			game.ww/2-titleFont:getWidth(public.worldName)/2,
			game.hud.menuButton.const * public.centerIcon.y/game.wh*2)
	end

	setmetatable(public, self)
	self.__index = self; return public
end



GameScreen = {}
function GameScreen:new()
	local public = {}
		public.chapterPrologue = ChapterPrologue:new()
		public.gameShining = GameShining:new()
		public.state = "waiting"
		public.quitingTimer = 0
		public.quitingSpeed = 0.7
		public.level = 1
		public.sparksCount = 0
		public.collisionCount = 0
		public.chapter = 1
		-- game objects
		public.player = Player:new(0, 0)
		public.camera = Camera(0, 0)
		public.dx = 0
		public.dy = 0
		public.cameraAngle = 0
		public.cameraMovementZoom = 2
		public.cameraMovementZoomMax = 1.2
		public.cameraMovementZoomSpeed = 0.001
		public.world = arrayToLevel(levels[1])
		-- colors
		public.mainColor = {}
		public.lightColor = {}
		public.darkColor = {}

	function public:rubber(dx,dy)
		local dt = love.timer.getDelta()*4
	    return dx*dt, dy*dt
	end

	function public:update()
		if public.chapterPrologue.active then
			if not prologueMusic[public.chapter]:isPlaying() then
				prologueMusic[public.chapter]:play()
			end
			if prologueMusic[public.chapter]:getVolume() < 1 then
				prologueMusic[public.chapter]:setVolume(prologueMusic[public.chapter]:getVolume() + 0.05)
			end
			public.chapterPrologue:update()
			public.camera.scale = (game.ww/1300 + math.random()/4 * math.sin(game.hud.webs.flashColor[4])) * public.cameraMovementZoom
		else
			if prologueMusic[public.chapter]:isPlaying() then
				prologueMusic[public.chapter]:setVolume(prologueMusic[public.chapter]:getVolume() - 0.01)
				if prologueMusic[public.chapter]:getVolume() <= 0 then
					prologueMusic[public.chapter]:stop()
				end
			end

			public.gameShining:update()

			public.camera:rotateTo(math.rad(public.cameraAngle))

			if public.state == "waiting" then
				if public.cameraMovementZoom > public.cameraMovementZoomMax then
					public.cameraMovementZoom = public.cameraMovementZoom - 0.01
				else
					if public.player.state == "pause" then
						if public.cameraMovementZoom < public.cameraMovementZoomMax and public.cameraMovementZoomMax - public.cameraMovementZoom > public.cameraMovementZoomSpeed then
							public.cameraMovementZoom = public.cameraMovementZoom + public.cameraMovementZoomSpeed
						else
							public.cameraMovementZoom = public.cameraMovementZoomMax
						end
					elseif public.player.state == "stop" then
						if public.cameraMovementZoom > 1 and public.cameraMovementZoom - 1 > public.cameraMovementZoomSpeed then
							public.cameraMovementZoom = public.cameraMovementZoom - public.cameraMovementZoomSpeed
						else
							public.cameraMovementZoom = 1
						end
					end
				end
				local dx, dy = public.player.x - public.camera.x, public.player.y - public.camera.y
				dx, dy = public:rubber(dx, dy)
				public.camera:move(dx/2, dy/2)
				public.camera.scale = (game.ww/1300 + math.random()/4 * math.sin(game.hud.webs.flashColor[4])) * public.cameraMovementZoom
				for i = 1, #public.world do
					public.world[i]:update()
				end
				public.player:update()

			elseif public.state == "quiting" then
				if game.hud.webs.maskOpacity > 0 and public.quitingTimer > 0 then
					public.quitingTimer = public.quitingTimer - public.quitingSpeed
				else
					if game.hud.webs.maskOpacity > 0 then
						public.state = "waiting"
						game.state = "levels"
						game.levelsScreen.state = "justfromgame"
						game.hud.dropdown.state = "fromshown"
						game.hud.menuButton.state = "hideandup"
						game.hud.mask.state = "fromgame"
						game.hud.mask.color[4] = 1
						game.levelsScreen.screenOpacity = 1
						game.hud.webs:toDefault()
						public.collisionCount = 0
						if game.levelsScreen.selectedLevel ~= game.levelsScreen.maxOpenedLevel
						and math.fmod(game.levelsScreen.maxOpenedLevel, 10) <= (game.levelsScreen.maxOpenedLevel - game.levelsScreen.selectedLevel) then
							game.musicEngine:nextTrack(game.levelsScreen.maxOpenedLevel)
						else
							game.musicEngine:setVolume(1)
						end
						game.sfxEngine:play("cancel")
						game.levelsScreen.nextWorldName = getWorldName(game.levelsScreen.maxOpenedLevel)
						game.levelsScreen.worldNameState = "out"
					else
						game.state = "game"
						game.hud.mask.state = "fadein"
						game.hud.mask.color[4] = 1
						if game.levelsScreen.selectedLevel == game.levelsScreen.maxOpenedLevel then
							game.levelsScreen.temporarySparks = maximumSparksLevel[game.levelsScreen.selectedLevel]
						end
						game.hud.webs:toDefault()
						game.gameScreen.collisionCount = 0
						game.gameScreen.cameraAngle = angleLevel[game.levelsScreen.selectedLevel]
						local playerX
						local playerY
						game.gameScreen.level = game.levelsScreen.selectedLevel
						game.gameScreen.world, playerX, playerY = arrayToLevel(levels[game.gameScreen.level])
						game.gameScreen.player = Player:new(playerX, playerY)
						game.gameScreen.cameraMovementZoom = 2
						game.gameScreen.camera = Camera(playerX, playerY)
						game.gameScreen.sparksCount = 0
						game.gameScreen.mainColor, game.gameScreen.lightColor, game.gameScreen.darkColor = getColorsFromLevel(game.levelsScreen.selectedLevel)
						game.gameScreen.player.dotsColor[1] = game.gameScreen.lightColor[1]
						game.gameScreen.player.dotsColor[2] = game.gameScreen.lightColor[2]
						game.gameScreen.player.dotsColor[3] = game.gameScreen.lightColor[3]
						game.gameScreen.state = "waiting"
						game.gameScreen.quitingTimer = 0
						game.hud.dropdown.state = "fromshown"
						game.hud.menuButton.state = "fromdropdown"
						game.musicEngine:setVolume(1)
					end
				end

			end
		end

	end

	function public:keypressed(key)
		if public.state == "waiting" then
			if (keypressedAnyNotEscape(key)) then
				if public.player.state == "stop" then
					public.player.state = "pause"
					public.player.reverse = not public.player.reverse
				elseif public.player.state == "pause" then
					public.player.state = "stop"
				end
			elseif keypressedEscape(key) and (game.hud.mask.state == "standby") then
				public.state = "quiting"
				public.quitingTimer = 100
				game.hud.menuButton.state = "todropdown"
				game.hud.dropdown.state = "toshown"
				game.musicEngine:setVolume(0.2)
				game.sfxEngine:play("quitIN")
			end

		elseif public.state == "quiting" then
			if (key) then
				public.state = "waiting"
				public.quitingTimer = 0
				game.hud.dropdown.state = "fromshown"
				game.hud.menuButton.state = "fromdropdown"
				game.musicEngine:setVolume(1)
				game.sfxEngine:play("cancel")
			end

		end

		if public.chapterPrologue.active then
			if key then
				public.chapterPrologue.canNotPassTextShow = true
			end
		end
	end

	function public:mousepressed(x, y)
		if public.state == "waiting" then
			if isTouched(x, y, 0, 0, 75, 75) and (game.hud.mask.state == "standby") then
				public.state = "quiting"
				public.quitingTimer = 100
				game.hud.menuButton.state = "todropdown"
				game.hud.dropdown.state = "toshown"
				game.musicEngine:setVolume(0.2)
				game.sfxEngine:play("quitIN")
			elseif isTouched(x, y, 0, 0, game.ww, game.wh) then
				if public.player.state == "stop" then
					public.player.state = "pause"
					public.player.reverse = not public.player.reverse
				elseif public.player.state == "pause" then
					public.player.state = "stop"
				end
			end

		elseif public.state == "quiting" then
			if isTouched(x, y, 0, 0, game.ww, game.wh) then
				public.state = "waiting"
				public.quitingTimer = 0
				game.hud.dropdown.state = "fromshown"
				game.hud.menuButton.state = "fromdropdown"
				game.musicEngine:setVolume(1)
				game.sfxEngine:play("cancel")
			end

		end
	end

	function public:draw()
		if public.chapterPrologue.active then
			public.chapterPrologue:draw()
		else
			if game.levelsScreen.selectedLevel > 50 then
				love.graphics.setColor(math.random()/10 + 0.5, math.random()/10 + 0.5, math.random()/10 + 0.5, 0.25)
				love.graphics.rectangle("fill", 0, 0, game.ww, game.wh)
			end
			public.gameShining:draw()
			public.camera:attach()
			public.player:draw(-1)
			for i = 1, #public.world do public.world[i]:draw(-3) end
			for i = 1, #public.world do public.world[i]:draw(-2) end
			for i = 1, #public.world do public.world[i]:draw(-1) end
			public.player:draw(0)
			for i = 1, #public.world do public.world[i]:draw(1) end
			for i = 1, #public.world do public.world[i]:draw(2) end
			public.camera:detach()
		end
	end

	setmetatable(public, self)
	self.__index = self; return public
end



Hud = {}
function Hud:new()

	local public = {}
		public.menuButton = MenuButton:new()
		public.dropdown = Dropdown:new()
		public.titleTriangle = TitleTriangle:new()
		public.sparks = Sparks:new()
		public.arrows = Arrows:new()
		public.mask = Mask:new()
		public.webs = Webs:new()
		public.clearProgressSplash = ClearProgressSplash:new()
		public.pressAnyKey = PressAnyKey:new()
		public.pressEscape = PressEscape:new()
		public.quitingText = QuitingText:new()

	function public:update()
		if game.state == "levels" then
			public.arrows:update()
		end
		public.menuButton:update()
		public.dropdown:update()
		public.titleTriangle: update()
		public.sparks:update()
		public.mask:update()
		public.webs:update()
		public.clearProgressSplash:update()
		public.pressAnyKey:update()
		public.pressEscape:update()
		public.quitingText:update()
	end

	function public:keypressed(key) end

	function public:draw()
		if game.state == "opening" then
			-- no code
		elseif game.state == "levels" then
			public.arrows:draw()
			public.dropdown:draw()
			public.menuButton:draw()
		elseif not (game.state == "game" and game.gameScreen.chapterPrologue.active) then
			public.titleTriangle:draw()
			public.webs:draw()
			public.sparks:draw()
			public.dropdown:draw()
			public.menuButton:draw()
		end
		public.mask:draw()
		public.clearProgressSplash:draw()
		public.pressAnyKey:draw()
		public.pressEscape:draw()
		public.quitingText:draw()
	end

	setmetatable(public, self)
	self.__index = self; return public
end
