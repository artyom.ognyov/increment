local Camera = require 'source/libraries/hump.camera'
io.stdout:setvbuf'no' 

require 'source/classes/gameObjects'
require 'source/classes/screens'
require 'source/utils/colors'
require 'source/utils/globalVariables'
require 'source/utils/utilFunctions'
require 'source/utils/text'

function engineLoad()
	math.randomseed(os.time())
	game = Game:new()
end

function engineUpdate(dt)
	game:update()
end

function engineKeyreleased()
	if keyIsReadyToBePressed == false then
		keyIsReadyToBePressed = true
	end
end

function engineKeypressed(key)
	game:keypressed(key)
end

function engineMousepressed(x, y, button, istouch)
	if button == 1 then
		game:mousepressed(x, y)
	end
end

function engineDraw()
	game:draw()
end
