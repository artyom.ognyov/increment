chapterPrologues = {}

chapterPrologues[1] = {
	{'', '', 200},
	{'В этом мире нет', 'ничего постоянного.', 150},
	{'Всё когда-нибудь', 'заканчивается.', 150},
	{'Когда я пытаюсь вспомнить', 'свои дни в Инкременте,', 150},
	{'Это пробуждает во мне два', 'самых сильных чувства:', 150},
	{'Любовь', 'и боль...', 150},
	{'Я аккуратно соберу', 'все воспоминания...', 150},
	{'', 'Кусочек за кусочком', 300},
}

chapterPrologues[2] = {
	{'', '', 100},
	{'Это к чему всё было?', 'В голове спутались мысли.', 150},
	{'Что это за место', 'и что за люди со мной говорили?', 150},
	{'Странное чувство вины', 'никак не проходит.', 150},
	{'', 'Я должна помнить...', 150},
	{'', 'Надо вернуться в начало', 360},
}

chapterPrologues[3] = {
	{'', '', 100},
	{'Не помню что это', 'за занятия', 150},
	{'Всё что осталось это', 'пара триггеров.', 150},
	{'Щелчки сменяющихся диафильмов', 'и запах пыли на лампе.', 150},
	{'Шелест бумаги и попытки', 'уловить что-то через почерк.', 150},
	{'Привкус металла', 'и какой-то блеск на полу', 150},
	{'', '...', 150},
	{'', 'Попытаюсь вспомнить', 360},
}

chapterPrologues[4] = {
	{'', '', 100},
	{'Мне сказали', '...', 200},
	{'Что придётся начинать', 'всё сначала', 200},
	{'Но на этот раз', 'будет больнее', 360},
}

chapterPrologues[5] = {
	{'', '', 100},
	{'Я аккуратно соберу', 'все воспоминания...', 200},
	{'Кусочек за кусочком', '', 360},
}

chapterPrologues[6] = {
	{'', '', 100},
	{'Только в кромешной тьме', '', 200},
	{'Можно увидедеть призраков прошлого', '', 200},
	{'Занятия не прошли напрасно', '', 200},
	{'Я', '', 50},
	{'Всё', '', 50},
	{'Вспомнила', '', 360},
}