require 'source/classes/gameObjects'
require 'source/utils/colors'

-- remove
godMode = false
forceLevel = nil
levels = {}
require 'source/levels/world1'
require 'source/levels/world2'
require 'source/levels/world3'
require 'source/levels/world4'
require 'source/levels/world5'
require 'source/levels/world6'

-- [n] - maximum sparks got on level n
maximumSparksLevel = {}
for i = 1, 100 do
	maximumSparksLevel[i] = -1
end

-- [n] - how many sparks on the level n
minimumSparksLevel = {
	2, 2, 2, 3, 3, 5, 8, 7, 5, 25, -- 1-10
	9, 18, 11, 12, 16, 12, 13, 12, 16, 31, -- 11-20
	4, 14, 9, 10, 13, 16, 12, 19, 12, 37, -- 21-30
	4, 12, 30, 21, 17, 16, 31, 35, 12, 52, -- 31-40
	9, 18, 24, 18, 6, 41, 0, 0, 0, 66, -- 41-50
	10, 5, 4, 3, 2, 1, 0, 0, 0, 0, -- 51-60
}

angleLevel = {
	0, 0, 0, 0, 3, -3, 5, -5, 7, 0, -- 1-10
	5, -5, 3, 7, 0, -5, 0, -3, 5, -7, -- 11-20
	0, 7, -5, 5, -5, 3, -3, 7, -7, -3, -- 21-30
	0, 5, -3, 7, 3, -5, 3, 5, -3, 0, -- 31-40
	0, 5, -5, 7, 3, -7, 0, 0, 0, 0, -- 41-50
	0, 0, -7, 5, -3, 0, 0, 0, 0, 0, -- 51-60
}
