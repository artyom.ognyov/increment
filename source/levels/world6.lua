require 'source/utils/text'

-- WORLD 6: WHITE

levels[51] = {
	{ name = "spark", arguments = {4770, 3276} },
	{ name = "spark", arguments = {4735, 2996} },
	{ name = "spark", arguments = {4485, 2794} },
	{ name = "spark", arguments = {3210, 2844} },
	{ name = "spark", arguments = {3245, 3795} },
	{ name = "spark", arguments = {3870, 3120} },
	{ name = "spark", arguments = {3920, 2300} },
	{ name = "spark", arguments = {5390, 2265} },
	{ name = "spark", arguments = {5970, 1635} },
	{ name = "spark", arguments = {4500, 1600} },

	{ name = "text", arguments = {text[51][1][1], 4770, 3031, text[51][1][2]} },
	{ name = "text", arguments = {text[51][2][1], 4535, 2794, text[51][2][2]} },
	{ name = "text", arguments = {text[51][3][1], 3210, 2794, text[51][3][2]} },
	{ name = "text", arguments = {text[51][4][1], 3210, 3830, text[51][4][2]} },
	{ name = "text", arguments = {text[51][5][1], 3870, 3170, text[51][5][2]} },
	{ name = "text", arguments = {text[51][6][1], 3870, 2300, text[51][6][2]} },
	{ name = "text", arguments = {text[51][7][1], 5355, 2300, text[51][7][2]} },
	{ name = "text", arguments = {text[51][8][1], 6020, 1635, text[51][8][2]} },
	{ name = "text", arguments = {text[51][9][1], 4535, 1635, text[51][9][2]} },
	
	{ name = "finish", arguments = {3950, 1050} },
	{ name = "player", arguments = {4770, 3520} }
}

levels[52] = {
	{ name = "circle", arguments = {821, 953, 100} },
	{ name = "circle", arguments = {939, 1011, 100} },
	{ name = "circle", arguments = {1046, 1153, 100} },
	{ name = "circle", arguments = {1046, 1313, 100} },
	{ name = "circle", arguments = {972, 1478, 100} },
	{ name = "circle", arguments = {820, 1555, 100} },
	{ name = "circle", arguments = {666, 1478, 100} },
	{ name = "circle", arguments = {594, 1333, 100} },
	{ name = "circle", arguments = {594, 1168, 100} },
	{ name = "circle", arguments = {694, 1011, 100} },

	{ name = "spark", arguments = {821, 745} },
	{ name = "spark", arguments = {821, 685} },
	{ name = "spark", arguments = {794, 655} },
	{ name = "spark", arguments = {847, 655} },
	{ name = "spark", arguments = {821, 625} },

	{ name = "text", arguments = {text[52][1][1], 821, 550, text[52][1][2]} },

	{ name = "finish", arguments = {820, 250} },
	{ name = "player", arguments = {820, 1378} }
}

levels[53] = {
	{ name = "circle", arguments = {1220, 655, 100} },
	{ name = "circle", arguments = {1628, 655, 100} },

	{ name = "spark", arguments = {794, 655} },
	{ name = "spark", arguments = {821, 625} },
	{ name = "spark", arguments = {821, 685} },
	{ name = "spark", arguments = {847, 655} },

	{ name = "text", arguments = {text[53][1][1], 1020, 655, text[53][1][2]} },
	{ name = "text", arguments = {text[53][2][1], 1220, 655, text[53][2][2]} },
	{ name = "text", arguments = {text[53][3][1], 1420, 655, text[53][3][2]} },

	{ name = "finish", arguments = {1628, 655} },
	{ name = "player", arguments = {820, 940} }
}

levels[54] = {
	{ name = "wall", arguments = {2000, 0, 1200, 200} },
	{ name = "wall", arguments = {2000, 200, 200, 800} },
	{ name = "wall", arguments = {1600, 1000, 200, 1000} },
	{ name = "wall", arguments = {2200, 200, 200, 200} },
	
    { name = "door", arguments = {1800, 900, 200, 200, 4}},

	{ name = "sizecircle", arguments = {1700, 800, 100, 150, 100} },
	{ name = "sizecircle", arguments = {1900, 600, 100, 150, 150} },

	{ name = "spark", arguments = {2350, 800} },
	{ name = "spark", arguments = {1600, 2120} },
	{ name = "spark", arguments = {1700, 950} },

	{ name = "text", arguments = {text[54][1][1], 1700, 2120, text[54][1][2]} },
	{ name = "text", arguments = {text[54][2][1], 3320, 100, text[54][2][2]} },

	{ name = "finish", arguments = {1900, 800} },
	{ name = "player", arguments = {2350, 1112} }
}

levels[55] = {
	{ name = "wall", arguments = {700, 500, 1200, 200} },
	{ name = "wall", arguments = {500, 700, 200, 1400} },
	{ name = "wall", arguments = {1900, 700, 200, 1400} },
	{ name = "wall", arguments = {1000, 1900, 600, 200} },
	{ name = "wall", arguments = {1000, 1000, 200, 200} },
	{ name = "wall", arguments = {1400, 1000, 200, 200} },
	{ name = "wall", arguments = {1000, 1400, 200, 200} },
	{ name = "wall", arguments = {1400, 1400, 200, 200} },

	{ name = "movecircle", arguments = { {
		{1000, 1000},
		{1600, 1000},
		{1600, 1600},
		{1000, 1600}
	}, 100, 100} },

	{ name = "movecircle", arguments = { {
		{1600, 1000},
		{1600, 1600},
		{1000, 1600},
		{1000, 1000}
	}, 100, 100} },

	{ name = "movecircle", arguments = { {
		{1600, 1600},
		{1000, 1600},
		{1000, 1000},
		{1600, 1000}
	}, 100, 100} },

	{ name = "movecircle", arguments = { {
		{1000, 1600},
		{1000, 1000},
		{1600, 1000},
		{1600, 1600}
	}, 100, 100} },

	{ name = "movecircle", arguments = { {
		{1200, 1200},
		{1400, 1200},
		{1400, 1400},
		{1200, 1400}
	}, 200, 40} },

	{ name = "movecircle", arguments = { {
		{1400, 1200},
		{1400, 1400},
		{1200, 1400},
		{1200, 1200},
	}, 200, 40} },

	{ name = "movecircle", arguments = { {
		{1400, 1400},
		{1200, 1400},
		{1200, 1200},
		{1400, 1200}
	}, 200, 40} },

	{ name = "movecircle", arguments = { {
		{1200, 1400},
		{1200, 1200},
		{1400, 1200},
		{1400, 1400}
	}, 200, 40} },

	{ name = "spark", arguments = {1000, 1300} },
	{ name = "spark", arguments = {1600, 1300} },

	{ name = "text", arguments = {text[55][1][1], 1300, 1800, text[55][1][2]} },

	{ name = "finish", arguments = {1300, 1300} },
	{ name = "player", arguments = {1300, 2488} }
}

levels[56] = {
	{ name = "wall", arguments = {850, 1500, 150, 200} },
	{ name = "wall", arguments = {1600, 1500, 150, 200} },
	{ name = "wall", arguments = {1000, 1000, 150, 200} },
	{ name = "wall", arguments = {1450, 1000, 150, 200} },
	{ name = "wall", arguments = {1000, 1200, 200, 100} },
	{ name = "wall", arguments = {1400, 1200, 200, 100} },
	{ name = "wall", arguments = {1000, 1900, 200, 100} },
	{ name = "wall", arguments = {1400, 1900, 200, 100} },
	{ name = "wall", arguments = {1000, 2000, 150, 200} },
	{ name = "wall", arguments = {1450, 2000, 150, 200} },

	{ name = "spark", arguments = {1300, 1250} },
	{ name = "sizecircle", arguments = {1300, 1600, 100, 150, 150} },
	
	{ name = "text", arguments = {text[56][1][1], 1300, 1830, text[56][1][2]} },
	{ name = "text", arguments = {text[56][2][1], 1300, 1425, text[56][2][2]} },

	{ name = "finish", arguments = {1300, 1100} },
	{ name = "player", arguments = {1300, 2100} }
}

levels[60] = {
	{ name = "wall", arguments = {0, -2500, 2500, 1000} },
	{ name = "wall", arguments = {0, -1500, 1000, 8200} },
	{ name = "wall", arguments = {1500, -1500, 1000, 8200} },
	{ name = "wall", arguments = {0, 6700, 2500, 1000} },
	{ name = "wall", arguments = {1000, 1500, 150, 200} },
	{ name = "wall", arguments = {1350, 1500, 150, 200} },
	{ name = "wall", arguments = {1000, 2200, 150, 200} },
	{ name = "wall", arguments = {1350, 2200, 150, 200} },
	{ name = "wall", arguments = {1000, 2900, 150, 200} },
	{ name = "wall", arguments = {1350, 2900, 150, 200} },
	{ name = "wall", arguments = {1000, 3600, 150, 200} },
	{ name = "wall", arguments = {1350, 3600, 150, 200} },
	{ name = "wall", arguments = {1000, 4300, 150, 200} },
	{ name = "wall", arguments = {1350, 4300, 150, 200} },
	{ name = "wall", arguments = {1000, 5000, 150, 200} },
	{ name = "wall", arguments = {1350, 5000, 150, 200} },
	{ name = "wall", arguments = {1000, 5700, 150, 1000} },
	{ name = "wall", arguments = {1350, 5700, 150, 1000} },
	{ name = "wall", arguments = {1150, 4650, 200, 200} },
	{ name = "spark", arguments = {1250, 6198} },
	{ name = "spark", arguments = {1250, 6032} },
	{ name = "spark", arguments = {1250, 5949} },
	{ name = "spark", arguments = {1250, 5882} },
	{ name = "spark", arguments = {1250, 5700} },
	{ name = "spark", arguments = {1250, 5618} },
	{ name = "spark", arguments = {1250, 5233} },
	{ name = "spark", arguments = {1250, 5100} },
	{ name = "spark", arguments = {1250, 4533} },
	{ name = "spark", arguments = {1250, 4400} },
	{ name = "spark", arguments = {1075, 4150} },
	{ name = "spark", arguments = {1075, 4050} },
	{ name = "spark", arguments = {1075, 3950} },
	{ name = "spark", arguments = {1250, 3808} },
	{ name = "spark", arguments = {1250, 3700} },
	{ name = "spark", arguments = {1350, 3500} },
	{ name = "spark", arguments = {1150, 3350} },
	{ name = "spark", arguments = {1150, 3200} },
	{ name = "spark", arguments = {1250, 3000} },
	{ name = "spark", arguments = {1425, 2750} },
	{ name = "spark", arguments = {1425, 2650} },
	{ name = "spark", arguments = {1425, 2550} },
	{ name = "spark", arguments = {1250, 2300} },
	{ name = "spark", arguments = {1250, 1720} },
	{ name = "spark", arguments = {1250, 1600} },

	{ name = "circle", arguments = {1250, 5450, 300} },
	{ name = "circle", arguments = {1300, 4050, 500} },
	{ name = "circle", arguments = {1350, 3200, 300} },
	{ name = "circle", arguments = {1150, 3500, 300} },
	{ name = "circle", arguments = {1200, 2650, 500} },
	{ name = "sizecircle", arguments = {1250, 1950, 325, 335, 325} },
	{ name = "circle", arguments = {1500, 5000, 200} },
	{ name = "circle", arguments = {1000, 5000, 200} },
	{ name = "circle", arguments = {1000, 4500, 200} },
	{ name = "circle", arguments = {1500, 4500, 200} },
	{ name = "circle", arguments = {1425, 3873, 200} },
	{ name = "circle", arguments = {1500, 3600, 200} },
	{ name = "circle", arguments = {1068, 3165, 200} },
	{ name = "circle", arguments = {1350, 2400, 200} },
	{ name = "sizecircle", arguments = {1000, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1500, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1150, 1700, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1350, 1700, 400, 500, 400} },

	{ name = "circle", arguments = {1250, 5450, 300} },
	{ name = "circle", arguments = {1300, 4050, 500} },
	{ name = "circle", arguments = {1350, 3200, 300} },
	{ name = "circle", arguments = {1150, 3500, 300} },
	{ name = "circle", arguments = {1200, 2650, 500} },
	{ name = "sizecircle", arguments = {1250, 1950, 325, 335, 325} },
	{ name = "circle", arguments = {1500, 5000, 200} },
	{ name = "circle", arguments = {1000, 5000, 200} },
	{ name = "circle", arguments = {1000, 4500, 200} },
	{ name = "circle", arguments = {1500, 4500, 200} },
	{ name = "circle", arguments = {1425, 3873, 200} },
	{ name = "circle", arguments = {1500, 3600, 200} },
	{ name = "circle", arguments = {1068, 3165, 200} },
	{ name = "circle", arguments = {1350, 2400, 200} },
	{ name = "sizecircle", arguments = {1000, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1500, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1150, 1700, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1350, 1700, 400, 500, 400} },

	{ name = "circle", arguments = {1250, 5450, 300} },
	{ name = "circle", arguments = {1300, 4050, 500} },
	{ name = "circle", arguments = {1350, 3200, 300} },
	{ name = "circle", arguments = {1150, 3500, 300} },
	{ name = "circle", arguments = {1200, 2650, 500} },
	{ name = "sizecircle", arguments = {1250, 1950, 325, 335, 325} },
	{ name = "circle", arguments = {1500, 5000, 200} },
	{ name = "circle", arguments = {1000, 5000, 200} },
	{ name = "circle", arguments = {1000, 4500, 200} },
	{ name = "circle", arguments = {1500, 4500, 200} },
	{ name = "circle", arguments = {1425, 3873, 200} },
	{ name = "circle", arguments = {1500, 3600, 200} },
	{ name = "circle", arguments = {1068, 3165, 200} },
	{ name = "circle", arguments = {1350, 2400, 200} },
	{ name = "sizecircle", arguments = {1000, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1500, 1950, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1150, 1700, 400, 500, 400} },
	{ name = "sizecircle", arguments = {1350, 1700, 400, 500, 400} },
	
	{ name = "text", arguments = {text[60][1][1], 1250, 5020, text[60][1][2]} },
	{ name = "text", arguments = {text[60][2][1], 1250, 4320, text[60][2][2]} },
	{ name = "text", arguments = {text[60][3][1], 1350, 3420, text[60][3][2]} },
	{ name = "text", arguments = {text[60][4][1], 1250, 2920, text[60][4][2]} },
	{ name = "text", arguments = {text[60][5][1], 1250, 2220, text[60][5][2]} },
	{ name = "text", arguments = {text[60][6][1], 1200, 2120, text[60][6][2]} },
	{ name = "text", arguments = {text[60][7][1], 1300, 2120, text[60][7][2]} },
	{ name = "text", arguments = {text[60][8][1], 1250, 1480, text[60][8][2]} },
	{ name = "text", arguments = {text[60][9][1], 1250, -880, text[60][9][2]} },
	{ name = "finish", arguments = {1250, -1200, true} },
	{ name = "player", arguments = {1250, 6500} },
}