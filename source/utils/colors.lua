function sixteenToNumber(char)
	if char == '0' then
		return 0
	elseif char == '1' then
		return 1
	elseif char == '2' then
		return 2
	elseif char == '3' then
		return 3
	elseif char == '4' then
		return 4
	elseif char == '5' then
		return 5
	elseif char == '6' then
		return 6
	elseif char == '7' then
		return 7
	elseif char == '8' then
		return 8
	elseif char == '9' then
		return 9
	elseif (char == 'A') or (char == 'a') then
		return 10
	elseif (char == 'B') or (char == 'b') then
		return 11
	elseif (char == 'C') or (char == 'c') then
		return 12
	elseif (char == 'D') or (char == 'd') then
		return 13
	elseif (char == 'E') or (char == 'e') then
		return 14
	elseif (char == 'F') or (char == 'f') then
		return 15
	else
		return 0
	end
end

function hexToRgb(hexText)
	local rgbArray = {0, 0, 0}
	rgbArray[1] = (sixteenToNumber(string.sub(hexText, 1, 1)) * 16 + sixteenToNumber(string.sub(hexText, 2, 2))) / 255
	rgbArray[2] = (sixteenToNumber(string.sub(hexText, 3, 3)) * 16 + sixteenToNumber(string.sub(hexText, 4, 4))) / 255
	rgbArray[3] = (sixteenToNumber(string.sub(hexText, 5, 5)) * 16 + sixteenToNumber(string.sub(hexText, 6, 6))) / 255
	return rgbArray
end

white = {1, 1, 1, 1}
white75 = {1, 1, 1, 0.75}
white50 = {1, 1, 1, 0.5}
white25 = {1, 1, 1, 0.25}
white10 = {1, 1, 1, 0.1}
white05 = {1, 1, 1, 0.05}
white005 = {1, 1, 1, 0.005}

black = {0, 0, 0, 1}
black75 = {0, 0, 0, 0.75}
black50 = {0, 0, 0, 0.5}
black25 = {0, 0, 0, 0.25}
black10 = {0, 0, 0, 0.1}
black05 = {0, 0, 0, 0.05}

warmGrey		= hexToRgb('212121')
lightWarmGrey	= hexToRgb('484848')
darkWarmGrey	= hexToRgb('000000')

coldGrey		= hexToRgb('263238')
lightColdGrey	= hexToRgb('4f5b62')
darkColdGrey	= hexToRgb('000a12')

yellow			= hexToRgb('ffc107')
lightYellow		= hexToRgb('fff350')
darkYellow		= hexToRgb('c79100')

-- LEVEL COLORS
blue			= hexToRgb('01579b')
lightBlue		= hexToRgb('4f83cc')
darkBlue		= hexToRgb('002f6c')

orange			= hexToRgb('e65100')
lightOrange		= hexToRgb('ff833a')
darkOrange		= hexToRgb('ac1900')

green			= hexToRgb('004d40')
lightGreen		= hexToRgb('39796b')
darkGreen		= hexToRgb('00251a')

purple			= hexToRgb('4a148c')
lightPurple		= hexToRgb('7c43bd')
darkPurple		= hexToRgb('12005e')

red				= hexToRgb('b71c1c')
lightRed		= hexToRgb('f05545')
darkRed			= hexToRgb('7f0000')

grey			= hexToRgb('eceff1')
lightGrey		= hexToRgb('ffffff')
darkGrey		= hexToRgb('babdbe')