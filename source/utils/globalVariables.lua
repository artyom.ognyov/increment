keyIsReadyToBePressed = true

antialiasing = 0

FPS_TIME = 1/60
love.keyboard.setKeyRepeat(false)
love.window.setMode(0, 0)
screenWidth = love.graphics.getWidth()
screenHeight = love.graphics.getHeight()
firstLaunch = true
love.mouse.setVisible(false)
love.graphics.setLineStyle("smooth")

titleFontSize = 54
buttonFontSize = 54
tipFontSize = 48
gameFontSize = 48
levelTitleFontSize = 64
levelTitleNumberFontSize = 128

OS = love.system.getOS()
if OS == 'Android' or OS == 'iOS' then
	fontSource = 'source/fonts/Monly-Free-Bold.otf'
else
	fontSource = 'source/fonts/Monly-Free-Light.otf'
end
boldFontSource = 'source/fonts/Monly-Free-Bold.otf'

titleFont = love.graphics.newFont(fontSource, titleFontSize)
buttonFont = love.graphics.newFont(fontSource, buttonFontSize)
tipFont = love.graphics.newFont(fontSource, tipFontSize)
gameFont = love.graphics.newFont(fontSource, gameFontSize)
gameFontBold = love.graphics.newFont(boldFontSource, gameFontSize)
levelTitleNumberFont = love.graphics.newFont(fontSource, levelTitleNumberFontSize)
