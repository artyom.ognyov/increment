gameName = "INCREMENT"

ui = {}
	ui.touchScreen = "TOUCH THE SCREEN"
	ui.pressAnyKey = "PRESS ANY KEY"
	ui.touchTriangle = "TOUCH THE TRIANGLE"
	ui.quitingIncrement = "QUITING INCREMENT . . ."
	ui.pressEscape = "PRESS ESCAPE"
	ui.forCancel = 'FOR CANCEL'
	ui.progressDeleting = "DELITING PROGRESS"
	ui.canNotSkip = 'CAN NOT SKIP'

	ui.goodNight = 'GOOD NIGHT'
	ui.goodMorging = 'GOOD MORNING'
	ui.goodDay = 'GOOD AFTERNOON'
	ui.goodEvening = 'GOOD EVENING'
	ui.hello = 'HELLO'
	ui.presents = 'PRESENTS'
	ui.anyComfortable = 'ANY COMFORTABLE'
	ui.key = 'KEY'
	ui.useYourHeadphones = 'USE YOUR HEADPHONES'
	ui.ifYouWant = '(IF YOU WANT)'
	ui.warning = {
		'WARNING!',
		'THIS GAME CONTAINS FAST FLASHING',
		'IMAGES. IT MAY CAUSE DISCOMFORT AND',
		'TRIGGER SEIZURES FOR PEOPLE WITH',
		'PHOTOSENSITIVE EPILEPSY.',
		'SAFETY FIRST!'
	}

	ui.disclaimer = {
		'All the characters',
		'and events depicted',
		'are fictitious.',
		'Any resemblance to',
		'a person living or dead',
		'is purely coincidental.'
	}

worldNames = {
	"THE MOST BLUE NIGHT",
	"THIS IS NOT YOUR SUN",
	"WHAT A CHANCE",
	"IN ANOTHER CASTLE",
	"YOUR HANDS",
	"WHERE IS YOUR FREEDOM?"
}

messagesButton = "ABOUT"
messages = {
	"ПОЛЬЗОВАТЕЛЬСКОЕ",
	"СОГЛАШЕНИЕ:",
	"",
	"1. Принимая условия",
	"настоящего Соглашения,",
	"пользователь даёт",
	"своё согласие",
	"социальной сети",
	"'INCREMENT'",
	"(далее - Компания)",
	"на сбор, хранение",
	"и обработку своих",
	"персональных данных,",
	"указанных путём",
	"заполнения веб-форм",
	"на сайте и его поддоменах",
	"(далее - Сайт).",
	"Под персональными",
	"данными понимается",
	"любая информация,",
	"относящаяся к прямо",
	"или косвенно",
	"определённому или",
	"определяемому",
	"физическому лицу.",
	"",
	"2. Настоящее Соглашение",
	"применяется в отношении",
	"обработки следующих",
	"персональных данных:",
	"фамилия, имя, отчество;",
	"место пребывания;",
	"номера телефонов;",
	"адрес электронной почты;",
	"воспоминания.",
	"",
	"3. В ходе обработки",
	"с персональными",
	"данными будут",
	"совершены следующие",
	"операции:",
	"сбор, хранение,",
	"уточнение, передача,",
	"блокирование,",
	"удаление,",
	"уничтожение.",
	"",
	"4. Компания обязуется",
	"не передавать полученную",
	"от Пользователя",
	"информацию третьим лицам.",
	"",
	"5. Персональные данные",
	"хранятся и",
	"обрабатываются до",
	"завершения всех",
	"необходимых процедур",
	"либо до ликвидации",
	"Компании.",
	"",
	"6. Пользователь принимает",
	"политику использования",
	"файлов cookies,",
	"используемую на Сайте,",
	"и даёт согласие на",
	"получение информации",
	"об IP-адресе и иных",
	"сведений о его",
	"активности на Сайте.",
	"",
	"7. Компания оставляет",
	"за собой право удаления",
	"и уничтожения всех",
	"персональных данных",
	"пользователя без",
	"дополнительного",
	"соглашения."
}

clearProgressButton = "DELETING\nPROGRESS"

credits = {
	"СПАСИБО ЗА ИГРУ",
	"",
	"Game is made by:",
	"POKATO POCO SHOW",
	"",
	"Code, graphics,",
	"level design,",
	"story",
	"made by:",
	"Artyom Ognyov",
	"",
	"Music by:",
	"Kai Engel",
}
