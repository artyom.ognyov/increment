utf8=require("utf8")

function extended (child, parent)
	setmetatable(child,{__index = parent}) 
end

function keypressedEscape(key)
	local s = false
	if key == "escape" and key ~= "f11" and key ~= "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedAnyNotEscape(key)
	local s = false
	if key ~= "escape" and key ~= "f11" and key ~= "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedDown(key)
	local s = false
	if key == "down" and key ~= "f11" and key ~= "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedLeft(key)
	local s = false
	if (key == "left" or key == "a") and key ~= "f11" and key ~= "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedRight(key)
	local s = false
	if (key == "right" or key == "d") and key ~= "f11" and key ~= "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedAnyNotLeftRightEscape(key)
	local s = false
	if key ~= "right"
	and key ~= "d"
	and key ~= "left"
	and key ~= "a"
	and key ~= "escape"
	and key ~= "f11"
	and key ~= "f12"
	and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedF11(key)
	local s = false
	if key == "f11" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function keypressedF12(key)
	local s = false
	if key == "f12" and keyIsReadyToBePressed == true then
		keyIsReadyToBePressed = false
		s = true
	end
	return s
end

function isTouched(x, y, x1, y1, w, h)
	if x >= x1 and x <= x1+w and y >= y1 and y <= y1+h then
		return true
	else
		return false
	end
end

function getColorsFromLevel(level)
	local main = {}
	local light = {}
	local dark = {}

	if level > 0 and level < 11 then
		main[1] = blue[1]
		main[2] = blue[2]
		main[3] = blue[3]
		light[1] = lightBlue[1]
		light[2] = lightBlue[2]
		light[3] = lightBlue[3]
		dark[1] = darkBlue[1]
		dark[2] = darkBlue[2]
		dark[3] = darkBlue[3]
	elseif level > 10 and level < 21 then
		main[1] = orange[1]
		main[2] = orange[2]
		main[3] = orange[3]
		light[1] = lightOrange[1]
		light[2] = lightOrange[2]
		light[3] = lightOrange[3]
		dark[1] = darkOrange[1]
		dark[2] = darkOrange[2]
		dark[3] = darkOrange[3]
	elseif level > 20 and level < 31 then
		main[1] = green[1]
		main[2] = green[2]
		main[3] = green[3]
		light[1] = lightGreen[1]
		light[2] = lightGreen[2]
		light[3] = lightGreen[3]
		dark[1] = darkGreen[1]
		dark[2] = darkGreen[2]
		dark[3] = darkGreen[3]
	elseif level > 30 and level < 41 then
		main[1] = purple[1]
		main[2] = purple[2]
		main[3] = purple[3]
		light[1] = lightPurple[1]
		light[2] = lightPurple[2]
		light[3] = lightPurple[3]
		dark[1] = darkPurple[1]
		dark[2] = darkPurple[2]
		dark[3] = darkPurple[3]
	elseif level > 40 and level < 51 then
		main[1] = red[1]
		main[2] = red[2]
		main[3] = red[3]
		light[1] = lightRed[1]
		light[2] = lightRed[2]
		light[3] = lightRed[3]
		dark[1] = darkRed[1]
		dark[2] = darkRed[2]
		dark[3] = darkRed[3]
	elseif level > 50 then --and level < 61 then
		main[1] = grey[1]
		main[2] = grey[2]
		main[3] = grey[3]
		light[1] = lightGrey[1]
		light[2] = lightGrey[2]
		light[3] = lightGrey[3]
		dark[1] = darkGrey[1]
		dark[2] = darkGrey[2]
		dark[3] = darkGrey[3]
	end

	return main, light, dark
end

-- For level loading/saving
function arrayToLevel(array)
	local world = {}
	local playerX
	local playerY
	for i = 1, #array do
		if array[i].name == "player" then
			playerX = array[i].arguments[1]
			playerY = array[i].arguments[2]
		elseif array[i].name == "wall" then
			if godMode then
				world[i] = DissappearingWall:new(
					array[i].arguments[1],
					array[i].arguments[2],
					array[i].arguments[3],
					array[i].arguments[4])
			else
				world[i] = Wall:new(
					array[i].arguments[1],
					array[i].arguments[2],
					array[i].arguments[3],
					array[i].arguments[4])
			end
		elseif array[i].name == "disswall" then
			world[i] = DissappearingWall:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4])
		elseif array[i].name == "door" then
				world[i] = Door:new(
					array[i].arguments[1],
					array[i].arguments[2],
					array[i].arguments[3],
					array[i].arguments[4],
					array[i].arguments[5])
		elseif array[i].name == "spark" then
			world[i] = Spark:new(
				array[i].arguments[1],
				array[i].arguments[2])
		elseif array[i].name == "circle" then
			world[i] = Circle:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3])
		elseif array[i].name == "sizecircle" then
			world[i] = SizeCircle:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4],
				array[i].arguments[5])
		elseif array[i].name == "movecircle" then
			world[i] = MoveCircle:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4])
		elseif array[i].name == "movesizecircle" then
			world[i] = MoveSizeCircle:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4])
		elseif array[i].name == "sizecircledependent" then
			world[i] = SizeCircleDependent:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4],
				array[i].arguments[5],
				array[i].arguments[6])
		elseif array[i].name == "movecircledependent" then
			world[i] = MoveCircleDependent:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4],
				array[i].arguments[5])
		elseif array[i].name == "text" then
			world[i] = Text:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3],
				array[i].arguments[4])
		elseif array[i].name == "finish" then
			world[i] = Finish:new(
				array[i].arguments[1],
				array[i].arguments[2],
				array[i].arguments[3])
		end
	end
	return world, playerX, playerY
end

function utf8sub(str, x, y)
	local x2,y2
	x2=utf8.offset(str,x)
	if y then
		y2=utf8.offset(str,y+1)
		if y2 then
			y2=y2-1
		end
	end
	return string.sub(str,x2,y2)
end

function countN(text)
	local n = 0
	for i = 1, #text do
		if string.sub(text, i, i) == "\n" then
			n = n + 1
		end
	end
	return n
end

function checkNotCyrillicSymbol(s)
	local x = false
	for y = 1, #notCyrillic do
		if s == string.sub(notCyrillic,y,y) then
			x = true
		end
	end
	return x
end

function getWorldName(level)
	local name = ""
	if level < 11 then
		name = worldNames[1]
	elseif level < 21 then
		name = worldNames[2]
	elseif level < 31 then
		name = worldNames[3]
	elseif level < 41 then
		name = worldNames[4]
	elseif level < 51 then
		name = worldNames[5]
	else
		name = worldNames[6]
	end
	return name
end

function shuffleArray(array)
	local defaultArray = {}
	for i = 1, #array do defaultArray[i] = array[i] end
	local shuffledArray = {}
	while #defaultArray > 0 do
		i = math.random(1, #defaultArray/2)
		table.insert(shuffledArray, defaultArray[(i-1)*2 + 1])
		table.insert(shuffledArray, defaultArray[(i-1)*2 + 2])
		table.remove(defaultArray, (i-1)*2 + 1)
		table.remove(defaultArray, (i-1)*2 + 1)
	end
	return shuffledArray
end

function updateFonts()
	titleFontSize = game.wh/11
	tipFontSize = game.wh/12
	levelTitleFontSize = game.wh/9
	levelTitleNumberFontSize = game.wh/4.5
	titleFont = love.graphics.newFont(fontSource, titleFontSize)
	tipFont = love.graphics.newFont(fontSource, tipFontSize)
	gameFontBold = love.graphics.newFont(boldFontSource, gameFontSize)
	levelTitleNumberFont = love.graphics.newFont(fontSource, levelTitleNumberFontSize)
end

function loadTrack(filePath)
	track = audio
		:newSource(filePath, "stream")
		
		:parse()
		
		-- :setIntensity(20)
		
		-- :setBPM(120)
		
		:setOffset(100)
		
		:setPitch(0.8)
		:setVolume(0)
		:setTargetPitch(1)
		:setTargetVolume(1)
		
		:setLooping(true)

	return track
end

function loadMusicFiles()
	local musicFilesData = {}

	musicFilesData.world1_1 = loadTrack("source/music/01 Quiet Music for Tiny Robots - Lullaby for a Broken Circuit.mp3", "stream")
	musicFilesData.world1_2 = loadTrack("source/music/05 Quiet Music for Tiny Robots - You Won’t Believe What Happens Next.mp3", "stream")
	musicFilesData.world2_1 = loadTrack("source/music/03 Quiet Music for Tiny Robots - Polar Vortex.mp3", "stream")
	musicFilesData.world2_2 = loadTrack("source/music/09 Quiet Music for Tiny Robots - You Were My Robot Lover.mp3", "stream")
	musicFilesData.world3_1 = loadTrack("source/music/01 Quiet Music for Tiny Robots - Buffering.mp3", "stream")
	musicFilesData.world3_2 = loadTrack("source/music/03 Quiet Music for Tiny Robots - More Than Machine.mp3", "stream")
	musicFilesData.world4_1 = loadTrack("source/music/07 Quiet Music for Tiny Robots - No Tears, No Rust.mp3", "stream")
	musicFilesData.world4_2 = loadTrack("source/music/10 Quiet Music for Tiny Robots - Quiet.mp3", "stream")
	musicFilesData.world5_1 = loadTrack("source/music/05 Quiet Music for Tiny Robots - You Won’t Believe What Happens Next.mp3", "stream")
	-- musicFilesData.world5_2 = loadTrack("source/music/10 Quiet Music for Tiny Robots - Quiet.mp3", "stream")
	musicFilesData.world5_2 = loadTrack("source/music/05 Quiet Music for Tiny Robots - You Won’t Believe What Happens Next.mp3", "stream")
	musicFilesData.world6_1 = loadTrack("source/music/06 Quiet Music for Tiny Robots - Closing Ceremony.mp3", "stream")
	musicFilesData.world6_2 = loadTrack("source/music/05 Quiet Music for Tiny Robots - Tiny Robot Armies.mp3", "stream")
	return musicFilesData
end

prologueMusic = {}
prologueMusic[1] = loadTrack("source/music/04 Quiet Music for Tiny Robots - Interlude 2.mp3", "stream")
prologueMusic[2] = loadTrack("source/music/02 Quiet Music for Tiny Robots - Interlude 1.mp3", "stream")
prologueMusic[3] = loadTrack("source/music/06 Quiet Music for Tiny Robots - Interlude 3.mp3", "stream")
prologueMusic[4] = loadTrack("source/music/08 Quiet Music for Tiny Robots - Interlude 4.mp3", "stream")
prologueMusic[5] = loadTrack("source/music/04 Quiet Music for Tiny Robots - Interlude 2.mp3", "stream")
prologueMusic[6] = loadTrack("source/music/02 Quiet Music for Tiny Robots - Interlude 1.mp3", "stream")